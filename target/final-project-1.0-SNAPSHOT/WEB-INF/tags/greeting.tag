<%@ attribute name="firstname" required="true" type="java.lang.String" description="Firstname of User" %>
<%@ attribute name="lastname" required="true" type="java.lang.String" description="Lastname of User" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<p>Hello, ${firstname} ${lastname}</p>
