<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="dt" uri="myTags" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tags" %>
<fmt:setLocale value="${lang}" scope="session"/>
<fmt:setBundle basename="text"/>
<html>
<head>
    <title>Internet Service Provider</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <style>
        <%@include file="../../../style/profile.css" %>
    </style>
</head>
<body>
<header>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="/application"><fmt:message key="ISP"/></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarNav" style="justify-content: right">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="/application"><fmt:message key="Home"/></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/application/about"><fmt:message key="About"/></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/application/contacts" style="margin-right: 15px">
                            <fmt:message key="Contacts"/>
                        </a>
                    </li>
                    <c:if test="${role!=null}">
                        <c:if test="${role.equalsTo('CUSTOMER')}">
                            <li class="nav-item">
                                <a class="button-link" href="topUpAccount">
                                    <button type="button" class="btn btn-outline-warning"><fmt:message
                                            key="Balance"/>: &nbsp
                                        <dt:numberTag format="${lang}" number="${user.balance}"/>
                                        &nbsp$
                                    </button>
                                </a>
                            </li>
                        </c:if>
                        <li class="nav-item">
                            <a class="button-link" href="logout">
                                <button type="button" class="btn btn-outline-primary"
                                        style="width: 80px; margin-right: 15px; margin-left: 15px">
                                    <fmt:message key="Logout"/>
                                </button>
                            </a>
                        </li>
                    </c:if>
                    <li>
                        <select id="lang" onchange="setLanguage()">
                            <c:if test="${lang == 'en'}">
                                <option>en</option>
                                <option>uk</option>
                            </c:if>
                            <c:if test="${lang == 'uk'}">
                                <option>uk</option>
                                <option>en</option>
                            </c:if>
                        </select>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>
<jsp:include page="../message.jsp"></jsp:include>

<section class="middle-container">

    <div class="main">
        <div class="links-container"><a class="button-link" href="profile">
            <button type="button" disabled class="input-button-disabled">
                <fmt:message key="Profile"/>
            </button>
        </a>
            <a class="button-link" href="tariffs?page=1">
                <button type="button" class="input-button-links">
                    <fmt:message key="Tariffs"/>
                </button>
            </a>
            <c:if test="${role.equalsTo('CUSTOMER')}">
                <a class="button-link" href="topUpAccount">
                    <button type="button" class="input-button-links">
                        <fmt:message key="FILL_UP_BALANCE"/>
                    </button>
                </a>

                <a class="button-link" href="transactionsHistory?page=1">
                    <button type="button" class="input-button-links">
                        <fmt:message key="TRANSACTIONS_HISTORY"/>
                    </button>
                </a>
                <a class="button-link" href="requests?page=1">
                    <button type="button" class="input-button-links">
                        <fmt:message key="REQUEST_HISTORY"/>
                    </button>
                </a>
                <c:if test="${userRequest!= null && tariffServices != null}">
                    <span style="margin-left: 74vh; font-size: 1.3rem;"></span>
                </c:if>
            </c:if>
            <c:if test="${role.equalsTo('ADMIN')}">
                <a class=" button-link
                " href="customers?page=1">
                    <button type="button" class="input-button-links">
                        <fmt:message key="Customers"/>
                    </button>
                </a>
            </c:if>
        </div>
        <c:if test="${role.equalsTo('CUSTOMER') && user.isBlocked == true}">
            <div style="margin: 5vh 0 0 5vh;
                        color: #eaea00">
            <span>
                <fmt:message key="BAN_MESSAGE"/>
            </span>
            </div>
        </c:if>

        <div style="display: flex;
        justify-content: flex-start;
        flex-wrap: nowrap;">

            <div class="person-container">
                <div class="title-container" style="z-index: 1">
                    <p id="title"><fmt:message key="MY_PROFILE"/></p>
                    <a class="button-link" href="userEdit">
                        <button type="button" class="input-button">
                            <fmt:message key="Edit"/>
                        </button>
                    </a>
                </div>
                <table class="table">
                    <tbody>
                    <tr>
                        <th scope="row"><fmt:message key="USER_LOGIN"/></th>
                        <td>${user.getLogin()}</td>
                    </tr>
                    <tr>
                        <th scope="row"><fmt:message key="FIRST_NAME"/></th>
                        <td>${user.getFirstName()}</td>
                    </tr>
                    <tr>
                        <th scope="row"><fmt:message key="LAST_NAME"/></th>
                        <td>${user.getLastName()}</td>
                    </tr>
                    <tr>
                        <th scope="row"><fmt:message key="Email"/></th>
                        <td>${user.getEmail()}</td>
                    </tr>
                    <tr>
                        <th scope="row"><fmt:message key="PHONE_NUMBER"/></th>
                        <td>${user.getPhoneNumber()}</td>
                    </tr>
                    <c:if test="${role.equalsTo('CUSTOMER')}">
                        <tr>
                            <th scope="row"><fmt:message key="Country"/></th>
                            <td>${user.getCountry()}</td>
                        </tr>
                        <tr>
                            <th scope="row"><fmt:message key="City"/></th>
                            <td>${user.getCity()}</td>
                        </tr>
                        <tr>
                            <th scope="row"><fmt:message key="Street"/></th>
                            <td>${user.getStreet()}</td>
                        </tr>

                    </c:if>
                    <tr>
                        <th scope="row" style="padding-top: 2vh"><fmt:message key="Password"/></th>
                        <td style="padding: 2vh">
                            <span>********</span>
                            <span style="position: absolute ; margin-left: 10vh">
                                <a class="link"
                                   role="button"
                                   data-bs-toggle="modal"
                                   data-bs-target="#staticBackdrop"
                                >
                                    <fmt:message key="Change"/>
                                </a>
                            </span>

                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="back-greeting">
                <p style="">
                    <tags:greeting firstname="${user.getFirstName()}" lastname="${user.getLastName()}"></tags:greeting>
                </p>
            </div>

            <c:if test="${userRequest!= null && tariffServices != null}">
                <div style="width: 50vh">
                    <div class="tariff-card">
                        <div class="card-icon">
                            <img src="images/worldwide.png" width="64px" height="64px"/>
                            <p id="current-tariff"><c:out value="${tariffServices.tariff.title}"/></p>
                        </div>
                        <div class="services">
                            <div style="width: 32vh">
                                <p style="text-align: center;"><fmt:message key="Service_List"/></p>
                                <div class="service-list">
                                    <c:forEach var="service" items="${tariffServices.services}">
                                        <li class="card-text">
                                            <c:out value="${service.title}"/>
                                        </li>
                                    </c:forEach>
                                </div>
                                <div>
                                    <label class="price-label"><fmt:message
                                            key="PRICE_PER_DAY"/>:</label>
                                    <span class="price"><dt:numberTag format="${lang}"
                                                                      number="${tariffServices.tariff.price}"/>$</span>
                                </div>
                                <div>
                                    <label id="startDate-label">
                                        <fmt:message key="START_DATE"/>:
                                    </label>

                                    <span class="price"><dt:dateTag format="${lang}"
                                                                    date="${userRequest.startDate}"/></span>
                                </div>
                                <div>
                                    <label id="endDate-label">
                                        <fmt:message key="END_DATE"/>:
                                    </label>

                                    <span class="price"><dt:dateTag format="${lang}"
                                                                    date="${userRequest.endDate}"/></span>
                                </div>
                                <div>
                                    <label id="status-label">
                                        <fmt:message key="Status"/>:
                                    </label>

                                    <span class="price"><fmt:message key="${userRequest.status}"/></span>
                                </div>
                                <div class="btn-container">
                                    <c:if test="${userRequest.status.equalsTo('ACTIVE')}">
                                        <form class="tariff-form" action="changeRequestStatus" method="post">

                                            <input type="submit" class="tariff-button-links button-link"
                                                   value="<fmt:message key="SUSPEND"/>"
                                                   style="background-color: white; width: 17vh">
                                            <input type="hidden" name="id" value="${userRequest.id}">
                                            <input type="hidden" name="status" value="SUSPENDED">
                                        </form>
                                    </c:if>
                                    <c:if test="${userRequest.status.equalsTo('SUSPENDED')}">
                                        <form class="tariff-form" action="changeRequestStatus" method="post">

                                            <input type="submit" class="tariff-button-links button-link"
                                                   value="<fmt:message key="ACTIVATE"/>"
                                                   style="background-color: white ;width: 16vh">
                                            <input type="hidden" name="id" value="${userRequest.id}">
                                            <input type="hidden" name="status" value="ACTIVE">
                                        </form>
                                    </c:if>
                                    <form class="tariff-form price" action="changeRequestStatus" method="post">
                                        <input type="hidden" name="id" value="${userRequest.id}">
                                        <input type="hidden" name="status" value="CLOSED">
                                        <input type="submit" class="tariff-button-links button-link"
                                               value="<fmt:message key="Close"/>"
                                               style="background-color: white ; width: 14vh">
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                    
                </div>

            </c:if>

            <div class="modal fade" id="staticBackdrop" style="top:20vh"
                 data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel"
                 aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">

                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                    aria-label="Close"></button>
                        </div>
                        <form action="changePassword" method="POST">
                            <div class="modal-body">
                                <div class="icon-container">
                                    <img src="images/unlocked.png" width="100px" height="100px"/>
                                </div>
                                <div class="text-con">
                                    <p><fmt:message key="PLEASE_INPUT_NEW_PASSWORD"/></p>
                                </div>
                                <div class="inp-con">
                                    <input class="inp" id="currentPassword" name="currentPassword" maxlength="20"
                                           type="password"
                                           placeholder="<fmt:message key="CURRENT_PASSWORD"/>"
                                           pattern="(^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$)|([a-fA-F0-9]{32})"
                                           oninvalid="this.setCustomValidity('Invalid password !\n Password length must be greater than 8 characters.' +
                                    '\nPassword must contain at least one Capital letter')"
                                           oninput="this.setCustomValidity('')"
                                           required>
                                    <br>
                                    <input class="inp" id="newPassword" name="newPassword" maxlength="20"
                                           type="password"
                                           placeholder="<fmt:message key="NEW_PASSWORD"/>"
                                           pattern="(^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$)|([a-fA-F0-9]{32})"
                                           oninvalid="this.setCustomValidity('Invalid password !\n Password length must be greater than 8 characters.' +
                                    '\nPassword must contain at least one Capital letter')"
                                           oninput="this.setCustomValidity('')"
                                           required>
                                    <br>
                                    <input class="inp" id="confirmPassword" name="confirmPassword" maxlength="20"
                                           type="password"
                                           placeholder="<fmt:message key="CONFIRM_PASSWORD"/>"
                                           pattern="(^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$)|([a-fA-F0-9]{32})"
                                           oninvalid="this.setCustomValidity('Invalid password !\n Password length must be greater than 8 characters.' +
                                    '\nPassword must contain at least one Capital letter')"
                                           oninput="this.setCustomValidity('')"
                                           required>
                                    <br>
                                    <c:if test="${error != null}">
                                        <p class="inp-error">
                                            <fmt:message key="${error}"/>
                                        </p>
                                    </c:if>
                                </div>
                                <div class="btn-con">
                                    <input type="submit" class="btn btn-primary" style="width: 38vh ; margin-top: 5vh"
                                           value="<fmt:message key="Change"/>">
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
            <c:if test="${error != null}">
                <script src="//ajax.aspnetcdn.com/ajax/jQuery/jquery-2.1.1.js" type="text/javascript"></script>
                <script type="text/javascript">
                    $(window).load(function () {
                        $('#staticBackdrop').modal('show');
                    });
                </script>
                <% session.removeAttribute("error");%>
            </c:if>
        </div>

    </div>
</section>


<jsp:include page="../templates/footer.jsp"/>

<script type="text/javascript">
    <%@include file="../../../js/languageUtil.js" %>
</script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous"></script>
</body>
</html>
