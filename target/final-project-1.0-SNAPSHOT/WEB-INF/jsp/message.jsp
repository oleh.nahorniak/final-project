<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${lang}" scope="session"/>
<fmt:setBundle basename="text"/>

<c:if test="${sessionScope.errorMessage != null}">
    <div id="myModal" class="modal fade" style="top:30vh">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="justify-content: center ; text-align: center ; align-items: center">
                    <h5 class="modal-title" style="text-align: center"><fmt:message
                            key="${sessionScope.errorMessage}"/></h5>
                </div>
                <div class="modal-body" style="align-items: center ; justify-content: center; text-align: center">
                    <button type="button" class="btn btn-secondary"
                            data-bs-dismiss="modal">
                        OK
                    </button>
                </div>
            </div>
        </div>
    </div>

    <script src="//ajax.aspnetcdn.com/ajax/jQuery/jquery-2.1.1.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(window).load(function () {
            $('#myModal').modal('show');
        });
    </script>
    <% session.removeAttribute("errorMessage");%>
</c:if>
