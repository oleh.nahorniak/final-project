<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${lang}" scope="session"/>
<fmt:setBundle basename="text"/>


<html>
<head>
    <title>Internet Service Provider</title>
    <style>
        <%@include file="../../../style/tariffEdit.css" %>
    </style>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<body>
<header>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="/application"><fmt:message key="ISP"/></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarNav" style="justify-content: right">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="/application"><fmt:message key="Home"/></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="about"><fmt:message key="About"/></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="contacts" style="margin-right: 15px">
                            <fmt:message key="Contacts"/>
                        </a>
                    </li>
                    <c:if test="${role!=null}">
                        <li class="nav-item">
                            <a class="button-link" href="logout">
                                <button type="button" class="btn btn-outline-primary"
                                        style="width: 80px; margin-right: 15px; margin-left: 15px">
                                    <fmt:message key="Logout"/>
                                </button>
                            </a>
                        </li>
                    </c:if>
                    <li>
                        <select id="lang" onchange="setLanguage()">
                            <c:if test="${lang == 'en'}">
                                <option>en</option>
                                <option>uk</option>
                            </c:if>
                            <c:if test="${lang == 'uk'}">
                                <option>uk</option>
                                <option>en</option>
                            </c:if>
                        </select>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>
<jsp:include page="../message.jsp"></jsp:include>
<section class="middle-container">
    <div class="background-image">
        <div class="links-container">
            <a class="button-link" href="profile">
                <button type="button" class="input-button-links">
                    <fmt:message key="Profile"/>
                </button>
            </a>
            <a class="button-link" href="tariffs">
                <button type="button" class="input-button-links">
                    <fmt:message key="Tariffs"/>
                </button>
            </a>

            <a class="button-link" href="tariffEdit">
                <button type="button" disabled class="input-button-disabled">
                    <fmt:message key="Edit"/>
                </button>
            </a>

        </div>

        <div class="person-container">
            <div class="mdl">
                <form action="tariffEdit" method="post">
                    <div class="t-container">
                        <p class="t-title">
                            <fmt:message key="EDIT_TARIFF"/>
                        </p>
                    </div>
                    <div class="form-container">
                        <div class="form">
                            <c:set var="tariff" scope="request" value="${tariffServices.tariff}"/>
                            <c:set var="services" scope="request" value="${tariffServices.services}"/>
                            <input type="hidden" name="id" value="${param.id}">
                            <c:if test="${lang == 'en'}">
                                <div class="center">
                                    <input class="inp" type="text" name="titleEn" value="${tariff.title}"
                                           placeholder="<fmt:message key="Title_EN"/>*"
                                           pattern="[A-Z]([a-z]{3,15})"
                                           oninvalid="this.setCustomValidity(`<fmt:message key="INVALID_TITLE_ENG"/>`)"
                                           oninput="this.setCustomValidity('')"
                                           required>
                                </div>
                                <div class="center">
                                    <input class="inp" type="text" name="titleUk" value="${additionalTitle}"
                                           placeholder="<fmt:message key="Title_UK"/>*"
                                           pattern="[А-ЯІЄЇ]([а-яієї]{3,15})"
                                           oninvalid="this.setCustomValidity(`<fmt:message key="INVALID_TITLE_UK"/>`)"
                                           oninput="this.setCustomValidity('')"
                                           required>
                                </div>
                            </c:if>
                            <c:if test="${lang == 'uk'}">
                                <div class="center">
                                    <input class="inp" type="text" name="titleUk" value="${tariff.title}"
                                           placeholder="<fmt:message key="Title_UK"/>*"
                                           pattern="[А-ЯІЄЇ]([а-яієї]{3,15})"
                                           oninvalid="this.setCustomValidity(`<fmt:message key="INVALID_TITLE_UK"/>`)"
                                           oninput="this.setCustomValidity('')"
                                           required>
                                </div>
                                <div class="center">
                                    <input class="inp" type="text" name="titleEn" value="${additionalTitle}"
                                           placeholder="<fmt:message key="Title_EN"/>*"
                                           pattern="[A-Z]([a-z]{3,15})"
                                           oninvalid="this.setCustomValidity(`<fmt:message key="INVALID_TITLE_ENG"/>`)"
                                           oninput="this.setCustomValidity('')"
                                           required>
                                </div>
                            </c:if>
                            <div class="center">
                                <input class="inp" type="text" name="code" value="${tariff.code}"
                                       placeholder="<fmt:message key="Code"/>*"
                                       pattern="#[A-Z]{1,15}"
                                       oninvalid="this.setCustomValidity(`<fmt:message key="INVALID_CODE"/>`)"
                                       oninput="this.setCustomValidity('')"
                                       required>
                            </div>
                            <div class="center">
                                <input class="inp" type="number" min="0" step="0.1" name="price" value="${tariff.price}"
                                       placeholder="<fmt:message key="Price"/>*"
                                       required>
                            </div>
                            <div style="margin-left: 5vh">
                                <c:forEach var="service" items="${allServices}">

                                    <c:if test="${services.contains(service)}">
                                        <article class="feature1">
                                            <input type="checkbox" id="feature1" name="services" value="${service.id}"
                                                   checked>
                                            <div>
                                      <span>
                                        <c:out value="${service.title}"/>
                                      </span>
                                            </div>
                                        </article>
                                    </c:if>
                                    <c:if test="${!services.contains(service)}">
                                        <article class="feature1">
                                            <input type="checkbox" id="feature2" name="services" value="${service.id}"
                                            >
                                            <div>
                                      <span>
                                        <c:out value="${service.title}"/>
                                      </span>
                                            </div>
                                        </article>
                                    </c:if>
                                </c:forEach>
                            </div>
                            <div class="center btn-center">
                                <input class="button-35" type="submit" value="<fmt:message key="Edit"/>">
                            </div>
                        </div>
                    </div>
                </form>
            </div>


        </div>
    </div>
</section>

<jsp:include page="../templates/footer.jsp"/>
<script type="text/javascript">
    <%@include file="../../../js/languageUtil.js" %>
</script>
<script src=" https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous"></script>
</body>
</html>
