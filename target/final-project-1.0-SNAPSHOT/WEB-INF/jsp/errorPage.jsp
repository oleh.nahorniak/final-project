<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <style>
        <%@include file="../../style/errorPageStyle.scss" %>
    </style>
</head>
<body>
<p>HTTP: <span>404</span></p>

<code>
    <span style="color: #f0c674;">this_page</span>.
    <em style="color: #b294bb;font-style: unset;">not_found</em> = true;
</code>
<code>
    <span style="color: #f0c674;">if</span>
    (<b style="color: #81a2be;font-weight: 500;">you_spelt_it_wrong</b>)
    {<span style="color: #f0c674;"
           class="sp">try_again()</span>;}
</code>
<code>
    <span style="color: #f0c674;">else if (<b style="color: #81a2be;font-weight: 500;">we_screwed_up</b>)</span>
    {<em style="color: #b294bb;font-style: unset;">alert</em>(
    <i style=" color: #b5bd68;">"We're really sorry about that."</i>);
    <span style="color: #f0c674;">window</span>.
    <em style="color: #b294bb;font-style: unset;">location</em>
    =
    home;}</code>
<center><a href="/application">HOME</a></center>
<script type="text/javascript">

    <%@include file="../../js/errorPage.js" %>


</script>
</body>
</html>
