<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${lang}" scope="session"/>
<fmt:setBundle basename="text"/>
<!DOCTYPE html>
<html>
<head>
    <title>Internet Service Provider</title>
    <style>
        <%@include file="/style/home.css" %>
    </style>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<body>
<header>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="/application"><fmt:message key="ISP"/></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarNav" style="justify-content: right">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="/application">
                            <fmt:message key="Home"/>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="about"><fmt:message key="About"/></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="contacts" style="margin-right: 15px"><fmt:message key="Contacts"/></a>
                    </li>

                    <c:if test="${role!=null}">
                        <li class="nav-item">
                            <a class="nav-link" href="profile" style="margin-right: 15px"><fmt:message
                                    key="Profile"/></a>
                        </li>

                        <li class="nav-item">
                            <a class="button-link" href="logout">
                                <button type="button" class="btn btn-outline-primary"
                                        style="width: 80px; margin-right: 15px;">
                                    <fmt:message key="Logout"/>
                                </button>
                            </a>
                        </li>
                    </c:if>
                    <c:if test="${role==null}">
                        <li class="nav-item">
                            <a class="button-link" href="login">
                                <button type="button" class="btn btn-outline-primary"
                                        style="width: 80px; margin-right: 15px;">
                                    <fmt:message key="Login"/>
                                </button>
                            </a>
                        </li>
                    </c:if>
                    <li>
                        <select id="lang" onchange="setLanguage()">
                            <c:if test="${lang == 'en'}">
                                <option>en</option>
                                <option>uk</option>
                            </c:if>
                            <c:if test="${lang == 'uk'}">
                                <option>uk</option>
                                <option>en</option>
                            </c:if>
                        </select>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>
<section class="middle-container">
    <div class="items-container">
        <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-indicators">
                <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active"
                        aria-current="true" aria-label="Slide 1"></button>
                <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1"
                        aria-label="Slide 2"></button>
                <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2"
                        aria-label="Slide 3"></button>
            </div>
            <div class="carousel-inner">
                <div class="carousel-item active" style="height: 500px;width: 500px">
                    <img src="images/24x7.jpg" class="d-block w-75 img" alt="...">
                    <div class="carousel-caption d-lg-block d-md-inline-block">
                        <h5>Online Support 24 / 7 </h5>
                    </div>
                </div>
                <div class="carousel-item" style="height: 500px ; width: 500px">
                    <img src="images/ISP.jpg" class="d-block w-75 img" alt="...">
                    <div class="carousel-caption d-lg-block d-md-inline-block">
                        <h5>Various services with affordable price </h5>
                    </div>
                </div>
                <div class="carousel-item" style="height: 500px; width: 500px">
                    <img src="images/fast.jpg" class="d-block w-75 img" alt="...">
                    <div class="carousel-caption d-lg-block d-md-inline-block">
                        <h5>As fast as possible connection </h5>

                    </div>
                </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions"
                    data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions"
                    data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>
    </div>
</section>

<jsp:include page="WEB-INF/jsp/templates/footer.jsp"/>
<script type="text/javascript">
    <%@include file="js/languageUtil.js"%>
</script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous"></script>
</body>
</html>