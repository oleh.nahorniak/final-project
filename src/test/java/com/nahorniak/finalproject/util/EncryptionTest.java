package com.nahorniak.finalproject.util;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class EncryptionTest {

    @ParameterizedTest
    @MethodSource("testCases")
    void md5(String password, String expected) {
        String actual = Encryption.md5(password);
        Assert.assertEquals(expected, actual);
    }

    static Stream<Arguments> testCases() {

        return Stream.of(
                Arguments.of("333", "310DCBBF4CCE62F762A2AAA148D556BD"),
                Arguments.of("epam", "A04BAC0616C4C1F038317B3656BD06FD"),
                Arguments.of("qwerty", "D8578EDF8458CE06FBC5BB76A58C5CA4"),
                Arguments.of("test", "098F6BCD4621D373CADE4E832627B4F6"));
    }
}