package com.nahorniak.finalproject.util.mail;

import com.nahorniak.finalproject.DAO.entity.User;
import org.junit.jupiter.api.Test;

import javax.mail.Message;

import static org.junit.jupiter.api.Assertions.*;

class MailerTest {

    @Test
    void sendEmail() {
        User user = new User.Builder()
                .withEmail("olehnahorniakwk@gmail.com")
                .build();

        String message = "Test";
        String subject = "Test";
        boolean expected = true;

        Mailer mailer = new Mailer();
        boolean actual = mailer.sendEmail(user, subject, message);
        assertEquals(actual, expected);

    }
}