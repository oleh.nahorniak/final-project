package com.nahorniak.finalproject.DAO;

import com.nahorniak.finalproject.DAO.entity.Transaction;
import org.junit.jupiter.api.*;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


class TransactionDAOTest {

    private static final String CONNECTION_URL = "jdbc:derby:memory:testdb;create=true";

    private static final String SHUTDOWN_URL = "jdbc:derby:;shutdown=true";
    private static final String DROP_USERS_TABLE = "DROP TABLE users";
    private static final String DROP_ADDRESS_TABLE = "DROP TABLE address";
    private static final String DROP_TRANSACTION_TABLE = "DROP TABLE transactions";

    private static final String CREATE_TRANSACTION_TABLE =
            "CREATE TABLE transactions" +
                    "( " +
                    "    id             INT GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1)," +
                    "    user_id        INT                              NOT NULL," +
                    "    amount         numeric                              NOT NULL," +
                    "    date_and_time      TIMESTAMP                             NOT NULL DEFAULT CURRENT_TIMESTAMP," +
                    "    description         VARCHAR(50)                      NOT NULL," +
                    "    CONSTRAINT pk_transaction PRIMARY KEY (id)," +
                    "    CONSTRAINT FK_user foreign key (user_id) references users(id) on delete cascade" +
                    ")";

    private static final String CREATE_ADDRESS_TABLE =
            "CREATE TABLE address " +
                    "( " +
                    "    id             INT PRIMARY KEY ," +
                    "    country        VARCHAR(50)                              NOT NULL," +
                    "    city        VARCHAR(50)                              NOT NULL," +
                    "    street        VARCHAR(50)                              NOT NULL" +
                    ")";

    private static final String CREATE_USERS_TABLE =
            "CREATE TABLE users " +
                    "(\n" +
                    "    id             INT PRIMARY KEY ,\n" +
                    "    user_role      VARCHAR(50)                              NOT NULL,\n" +
                    "    login          VARCHAR(50)                              NOT NULL,\n" +
                    "    password       VARCHAR(50)                              NOT NULL,\n" +
                    "    first_name     VARCHAR(50)                              NOT NULL,\n" +
                    "    last_name      VARCHAR(50)                              NOT NULL,\n" +
                    "    email          VARCHAR(50)                              NOT NULL,\n" +
                    "    phone_number   VARCHAR(25)                              NOT NULL,\n" +
                    "    balance        numeric(5,2)                             NOT NULL,\n" +
                    "    address_id     INT                                      REFERENCES address(id) on delete cascade,\n" +
                    "    is_blocked     boolean                                  NOT NULL\n" +
                    ")";

    static Connection con;
    static TransactionDAO transactionDAO;

    @BeforeAll
    static void globalSetUp() throws SQLException, IOException {
        con = DriverManager.getConnection(CONNECTION_URL);
    }


    @BeforeEach
    void setUp() throws SQLException {
        transactionDAO = TransactionDAO.getInstance();
        con.createStatement().executeUpdate(CREATE_ADDRESS_TABLE);
        con.createStatement().executeUpdate(CREATE_USERS_TABLE);
        con.createStatement().executeUpdate(CREATE_TRANSACTION_TABLE);
        insertInTable(1, "admin", "12345Qwerty");

    }

    @AfterEach
    void tearDown() throws SQLException {
        con.createStatement().executeUpdate(DROP_TRANSACTION_TABLE);
        con.createStatement().executeUpdate(DROP_USERS_TABLE);
        con.createStatement().executeUpdate(DROP_ADDRESS_TABLE);


    }

    @Test
    void getAllByUserId() throws SQLException {
        int userId = 1;
        createTransaction(userId, 5, "2022-02-23 10:07:40.932848", "TOP_UP");
        createTransaction(userId, -5, "2022-02-23 12:07:40.932848", "PAYMENT");
        createTransaction(userId, -15, "2022-02-23 14:07:40.932848", "PAYMENT");

        String orderBy = "order by id OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";
        int start = 0;
        int count = 2;

        List<Transaction> transactions = transactionDAO.getAllByUserId(userId, orderBy, start, count, con);
        assertNotNull(transactions);
        assertEquals(2, transactions.size());
        assertEquals(5, transactions.get(0).getAmount());
    }

    @Test
    void testGetAllByUserId() throws SQLException {
        int userId = 1;
        createTransaction(userId, 5, "2022-02-23 10:07:40.932848", "TOP_UP");
        createTransaction(userId, -5, "2022-02-23 12:07:40.932848", "PAYMENT");
        createTransaction(userId, -15, "2022-02-23 14:07:40.932848", "PAYMENT");
        String orderBy = "order by id ";

        List<Transaction> transactions = transactionDAO.getAllByUserId(userId, orderBy, con);
        assertNotNull(transactions);
        assertEquals(3, transactions.size());
        assertEquals(-5, transactions.get(1).getAmount());

    }

    @Test
    void getCount() throws SQLException {
        int userId = 1;
        createTransaction(userId, 5, "2022-02-23 10:07:40.932848", "TOP_UP");
        createTransaction(userId, -5, "2022-02-23 12:07:40.932848", "PAYMENT");

        int expected = 2;
        int actual = transactionDAO.getCount(userId, con);
        assertEquals(expected, actual);

    }

    @Test
    void create() throws SQLException {
        int userId = 1;
        createTransaction(userId, 5, "2022-02-23 10:07:40.932848", "TOP_UP");

        int countBeforeInsert = transactionDAO.getCount(userId, con);
        assertEquals(1, countBeforeInsert);

        Transaction transaction = new Transaction.Builder().withId(2).withUserId(userId).withAmount(15)
                .withTimeStamp(Timestamp.valueOf("2022-02-23 12:07:40.932848")).withDescription("TOP_UP").build();

        transactionDAO.create(transaction, con);

        int countAfterInsert = transactionDAO.getCount(userId, con);
        assertEquals(2, countAfterInsert);

    }


    private void createTransaction(int userId, double amount, String timestamp, String description) throws SQLException {
        String transactionInsertion =
                "INSERT INTO transactions (user_id, amount, date_and_time, description)" +
                        "VALUES(" + userId + ", " + amount + ", '" + timestamp + "', '" + description + "')";

        con.createStatement().executeUpdate(transactionInsertion);
    }

    private void insertInTable(int userId, String login, String password) throws SQLException {

        String addressInsertion =
                "INSERT INTO address (id, country, city, street)" +
                        "VALUES(" + userId + ", 'Ukraine', 'Lviv', 'Shevchenko 123')";

        String userInsertion =
                "INSERT INTO users (id, user_role, login, password, first_name," +
                        " last_name, email,phone_number,balance,address_id,is_blocked)" +
                        "VALUES(" + userId + ", 'CUSTOMER', '" + login + "', '" + password + "','Oleh', 'Dmytrov', 'nagornyak68@gmail.com','333-333-1111',0," + userId + ",true)";

        con.createStatement().executeUpdate(addressInsertion);
        con.createStatement().executeUpdate(userInsertion);
    }
}