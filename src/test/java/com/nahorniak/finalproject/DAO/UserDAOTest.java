package com.nahorniak.finalproject.DAO;

import com.nahorniak.finalproject.DAO.entity.Role;
import com.nahorniak.finalproject.DAO.entity.User;
import org.junit.jupiter.api.*;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class UserDAOTest {

    private static final String CONNECTION_URL = "jdbc:derby:memory:testdb;create=true";

    private static final String SHUTDOWN_URL = "jdbc:derby:;shutdown=true";

    private static final String DROP_USERS_TABLE = "DROP TABLE users";
    private static final String DROP_ADDRESS_TABLE = "DROP TABLE address";

    private static final String CREATE_ADDRESS_TABLE =
            "CREATE TABLE address " +
                    "( " +
                    "    id             INT PRIMARY KEY ," +
                    "    country        VARCHAR(50)                              NOT NULL," +
                    "    city        VARCHAR(50)                              NOT NULL," +
                    "    street        VARCHAR(50)                              NOT NULL" +
                    ")";

    private static final String CREATE_USERS_TABLE =
            "CREATE TABLE users " +
                    "(\n" +
                    "    id             INT PRIMARY KEY ,\n" +
                    "    user_role      VARCHAR(50)                              NOT NULL,\n" +
                    "    login          VARCHAR(50)                              NOT NULL,\n" +
                    "    password       VARCHAR(50)                              NOT NULL,\n" +
                    "    first_name     VARCHAR(50)                              NOT NULL,\n" +
                    "    last_name      VARCHAR(50)                              NOT NULL,\n" +
                    "    email          VARCHAR(50)                              NOT NULL,\n" +
                    "    phone_number   VARCHAR(25)                              NOT NULL,\n" +
                    "    balance        numeric(5,2)                             NOT NULL,\n" +
                    "    address_id     INT                                      REFERENCES address(id) on delete cascade,\n" +
                    "    is_blocked     boolean                                  NOT NULL\n" +
                    ")";


    static Connection con;
    static UserDAO userDAO;

    @BeforeAll
    static void globalSetUp() throws SQLException, IOException {
        con = DriverManager.getConnection(CONNECTION_URL);
    }


    @BeforeEach
    void setUp() throws SQLException {
        userDAO = UserDAO.getInstance();
        con.createStatement().executeUpdate(CREATE_ADDRESS_TABLE);
        con.createStatement().executeUpdate(CREATE_USERS_TABLE);

    }

    @AfterEach
    void tearDown() throws SQLException {
        con.createStatement().executeUpdate(DROP_USERS_TABLE);
        con.createStatement().executeUpdate(DROP_ADDRESS_TABLE);

    }

    @Test
    void testConnection() {
        assertNotNull(con);

    }

    @Test
    void getUser() throws SQLException {

        int userid = 1;
        String login = "test";
        String password = "12345Test";

        String addressInsertion =
                "INSERT INTO address (id, country, city, street)" +
                        "VALUES(" + 1 + ", 'Ukraine', 'Lviv', 'Shevchenko 133')";


        String userInsertion =
                "INSERT INTO users (id, user_role, login, password, first_name," +
                        " last_name, email,phone_number,balance,address_id,is_blocked)" +
                        "VALUES(" + userid + ", 'CUSTOMER', '" + login + "', '" + password + "','Oleh', 'Nahornyak', 'nagornyak68@gmail.com','111-111-1111',0,1,true)";

        con.createStatement().executeUpdate(addressInsertion);
        con.createStatement().executeUpdate(userInsertion);

        User user = userDAO.getUser(login, password, con);
        assertNotNull(user);
        assertEquals(login, user.getLogin());
        assertEquals(password, user.getPassword());
        assertEquals(Role.CUSTOMER, user.getRole());
    }

    @Test
    void getUserById() throws SQLException {
        int userid = 3;

        String addressInsertion =
                "INSERT INTO address (id, country, city, street)" +
                        "VALUES(" + 1 + ", 'Ukraine', 'Lviv', 'Shevchenko 133')";


        String userInsertion =
                "INSERT INTO users (id, user_role, login, password, first_name," +
                        " last_name, email,phone_number,balance,address_id,is_blocked)" +
                        "VALUES(" + userid + ", 'CUSTOMER', 'user111', '12345Qwerty','Dmytro', 'Dmytrov', 'nagornyak68@gmail.com','333-333-1111',0,1,true)";

        con.createStatement().executeUpdate(addressInsertion);
        con.createStatement().executeUpdate(userInsertion);

        User user = userDAO.getUserById(userid, con);
        assertNotNull(user);
        assertEquals(userid, user.getId());

    }

    @Test
    void getUserByEmail() throws SQLException {

        String email = "nagornyak68@gmail.com";

        String addressInsertion =
                "INSERT INTO address (id, country, city, street)" +
                        "VALUES(" + 1 + ", 'Ukraine', 'Lviv', 'Shevchenko 133')";


        String userInsertion =
                "INSERT INTO users (id, user_role, login, password, first_name," +
                        " last_name, email,phone_number,balance,address_id,is_blocked)" +
                        "VALUES(" + 1 + ", 'CUSTOMER', 'user111', '12345Qwerty','Dmytro', 'Dmytrov', '" + email + "','333-333-1111',0,1,true)";

        con.createStatement().executeUpdate(addressInsertion);
        con.createStatement().executeUpdate(userInsertion);

        User user = userDAO.getUserByEmail(email, con);
        assertNotNull(user);
        assertEquals(email, user.getEmail());
    }


    @Test
    void changeBlockStatus() throws SQLException {
        boolean blockStatus = true;

        int userid = 1;

        String addressInsertion =
                "INSERT INTO address (id, country, city, street)" +
                        "VALUES(" + userid + ", 'Ukraine', 'Lviv', 'Shevchenko 133')";

        String userInsertion =
                "INSERT INTO users (id, user_role, login, password, first_name," +
                        " last_name, email,phone_number,balance,address_id,is_blocked)" +
                        "VALUES(" + 1 + ", 'CUSTOMER', 'user111', '12345Qwerty','Dmytro', 'Dmytrov', 'nagornyak68@gmail.com','333-333-1111',0,1,true)";

        con.createStatement().executeUpdate(addressInsertion);
        con.createStatement().executeUpdate(userInsertion);

        userDAO.changeBlockStatus(!blockStatus, userid, con);

        User user = userDAO.getUserById(userid, con);
        assertNotNull(user);

        assertNotEquals(blockStatus, user.getIsBlocked());

    }

    @Test
    void getAll() throws SQLException {

        int usersCountToShow = 3;
        int start = 1;

        insertInTable(1, "admin", "2222Qwerty");
        insertInTable(2, "user22", "111111test");
        insertInTable(3, "testing", "2222SSS");
        insertInTable(4, "john111", "2222JOHN");
        insertInTable(5, "ispmanager", "133245");

        String orderBy = "order by u.id OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";

        List<User> list = userDAO.getAll(orderBy, start, usersCountToShow, con);
        assertNotNull(list);
        assertEquals(usersCountToShow, list.size());

    }

    @Test
    void testGetAll() throws SQLException {

        insertInTable(1, "admin", "2222Qwerty");
        insertInTable(2, "user22", "111111test");
        insertInTable(3, "testing", "2222SSS");
        insertInTable(4, "john111", "2222JOHN");
        insertInTable(5, "ispmanager", "133245");

        List<User> users = userDAO.getAll("order by u.id", con);
        assertNotNull(users);
        assertEquals(5, users.size());
    }

    @Test
    void getCount() throws SQLException {
        insertInTable(1, "admin", "2222Qwerty");
        insertInTable(2, "user22", "111111test");
        insertInTable(3, "testing", "2222SSS");
        insertInTable(4, "john111", "2222JOHN");
        insertInTable(5, "ispmanager", "133245");

        int expected = 5;
        int actual = userDAO.getCount(con);

        assertEquals(expected, actual);
    }

    private void insertInTable(int userId, String login, String password) throws SQLException {

        String addressInsertion =
                "INSERT INTO address (id, country, city, street)" +
                        "VALUES(" + userId + ", 'Ukraine', 'Lviv', 'Shevchenko 123')";

        String userInsertion =
                "INSERT INTO users (id, user_role, login, password, first_name," +
                        " last_name, email,phone_number,balance,address_id,is_blocked)" +
                        "VALUES(" + userId + ", 'CUSTOMER', '" + login + "', '" + password + "','Oleh', 'Dmytrov', 'nagornyak68@gmail.com','333-333-1111',0," + userId + ",true)";

        con.createStatement().executeUpdate(addressInsertion);
        con.createStatement().executeUpdate(userInsertion);
    }
}