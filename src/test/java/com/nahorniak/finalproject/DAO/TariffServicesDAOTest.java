package com.nahorniak.finalproject.DAO;

import com.nahorniak.finalproject.DAO.entity.Service;
import com.nahorniak.finalproject.DAO.entity.Tariff;
import com.nahorniak.finalproject.DAO.entity.TariffServices;
import org.junit.jupiter.api.*;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class TariffServicesDAOTest {


    private static final String CONNECTION_URL = "jdbc:derby:memory:testdb1;create=true";
    private static final String SHUTDOWN_URL = "jdbc:derby:;shutdown=true";

    private static final String DROP_TARIFFS_TABLE = "DROP TABLE tariffs  ";
    private static final String DROP_SERVICES_TABLE = "DROP TABLE services  ";
    private static final String DROP_LANG_TABLE = "DROP TABLE language ";
    private static final String DROP_TARIFF_DESCRIPTION_TABLE = "DROP TABLE tariff_description  ";
    private static final String DROP_SERVICE_DESCRIPTION_TABLE = "DROP TABLE service_description  ";
    private static final String DROP_TARIFFS_SERVICES_TABLE = "DROP TABLE tariffs_services  ";

    private static final String CREATE_TARIFFS_TABLE =
            "CREATE TABLE tariffs " +
                    "( " +
                    "    id             INT GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1)," +
                    "    price_per_day  numeric                              NOT NULL," +
                    "    tariff_code        VARCHAR(50)                      NOT NULL," +
                    "    CONSTRAINT pk_tariff PRIMARY KEY (id)" +
                    ")";

    private static final String CREATE_SERVICES_TABLE =
            "CREATE TABLE services " +
                    "( " +
                    "    id             INT ," +
                    "    service_code        VARCHAR(50)                      NOT NULL," +
                    "    CONSTRAINT pk_service PRIMARY KEY (id)" +
                    ")";

    private static final String CREATE_LANG_TABLE =
            "CREATE TABLE language " +
                    "( " +
                    "    id             INT ," +
                    "    name        VARCHAR(50)                      NOT NULL," +
                    "    CONSTRAINT language_pkey PRIMARY KEY (id) " +
                    ")";

    private static final String CREATE_TARIFF_DESCRIPTION_TABLE =
            "CREATE TABLE tariff_description " +
                    "( " +
                    "    tariff_id             INT," +
                    "    lang_id             INT  ," +
                    "    title        VARCHAR(50)                      NOT NULL," +
                    "    CONSTRAINT FK_tariff foreign key (tariff_id) references tariffs(id) on delete cascade ," +
                    "    CONSTRAINT FK_lang foreign key (lang_id) references language (id) on delete cascade " +
                    ")";

    private static final String CREATE_SERVICE_DESCRIPTION_TABLE =
            "CREATE TABLE service_description " +
                    "( " +
                    "    service_id             INT  ," +
                    "    lang_id             INT ," +
                    "    title        VARCHAR(50)                      NOT NULL," +
                    "    CONSTRAINT FK_service foreign key (service_id) references services(id) on delete cascade ," +
                    "    CONSTRAINT FK_lang1 foreign key (lang_id) references language (id) on delete cascade " +
                    ")";

    private static final String CREATE_TARIFF_SERVICES_TABLE =
            "CREATE TABLE tariffs_services " +
                    "( " +
                    "    tariff_id             INT ," +
                    "    service_id             INT ," +
                    "    CONSTRAINT PK_tariffs_services PRIMARY KEY (tariff_id, service_id) , " +
                    "    CONSTRAINT FK_tariff1 foreign key (tariff_id) references tariffs(id) on delete cascade," +
                    "    CONSTRAINT FK_service1 foreign key (service_id) references services(id) on delete cascade" +
                    ")";

    static Connection con;
    static TariffServicesDAO tariffServicesDAO;

    @BeforeAll
    static void globalSetUp() throws SQLException, IOException {
        con = DriverManager.getConnection(CONNECTION_URL);
    }

    @AfterAll
    static void globalTearDown() throws SQLException, IOException {
        con.close();
        try {
            DriverManager.getConnection(SHUTDOWN_URL);
        } catch (SQLException ex) {
            System.err.println("Derby shutdown");
        }
    }

    @BeforeEach
    void setUp() throws SQLException {
        tariffServicesDAO = TariffServicesDAO.getInstance();

        con.createStatement().executeUpdate(CREATE_TARIFFS_TABLE);
        con.createStatement().executeUpdate(CREATE_SERVICES_TABLE);
        con.createStatement().executeUpdate(CREATE_TARIFF_SERVICES_TABLE);
        con.createStatement().executeUpdate(CREATE_LANG_TABLE);
        con.createStatement().executeUpdate(CREATE_TARIFF_DESCRIPTION_TABLE);
        con.createStatement().executeUpdate(CREATE_SERVICE_DESCRIPTION_TABLE);

        String englishInsertion =
                "INSERT INTO language (id, name)" +
                        "VALUES(" + 1 + ", 'en')";

        String ukrainianInsertion =
                "INSERT INTO language (id, name)" +
                        "VALUES(" + 2 + ", 'uk')";


        con.createStatement().executeUpdate(englishInsertion);
        con.createStatement().executeUpdate(ukrainianInsertion);

    }

    @AfterEach
    void tearDown() throws SQLException {

        con.createStatement().executeUpdate(DROP_TARIFFS_SERVICES_TABLE);
        con.createStatement().executeUpdate(DROP_TARIFF_DESCRIPTION_TABLE);
        con.createStatement().executeUpdate(DROP_SERVICE_DESCRIPTION_TABLE);
        con.createStatement().executeUpdate(DROP_LANG_TABLE);
        con.createStatement().executeUpdate(DROP_TARIFFS_TABLE);
        con.createStatement().executeUpdate(DROP_SERVICES_TABLE);


    }

    @Test
    void testConnection() {
        assertNotNull(con);

    }

    @Test
    void getTariffWithServices() throws SQLException {
        String lang = "en";
        int tariff_id = 1;
        createTariff(1, 1, 3.5, "Internet", "Інтернет", "I");

        TariffServices tariffServices = tariffServicesDAO.getTariffWithServices(lang, tariff_id, con);
        assertNotNull(tariffServices);
        assertEquals(tariff_id, tariffServices.getTariff().getId());

    }

    @Test
    void getAll() throws SQLException {
        String lang = "en";
        createTariff(1, 1, 3.5, "Internet", "Інтернет", "I");
        createTariff(2, 2, 5.5, "Cell", "Телефон", "C");
        createTariff(3, 3, 2.5, "Cable TV", "Кабельне ТБ", "CTV");
        createTariff(4, 4, 5.5, "IP-TV", "IP-TV", "IPTV");

        String orderBy = "order by id OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";

        int start = 1;
        int count = 2;

        assertEquals(4, tariffServicesDAO.getTariffsCount("", con));

        Set<TariffServices> tariffServices = tariffServicesDAO.getAll(lang, orderBy, start, count, con);
        assertNotNull(tariffServices);

        assertEquals(2, tariffServices.size());
    }

    @Test
    void testGetAll() throws SQLException {
        String lang = "en";

        createTariff(1, 1, 3.5, "Internet", "Інтернет", "I");
        createTariff(2, 2, 5.5, "Cell", "Телефон", "C");
        createTariff(3, 3, 2.5, "Cable TV", "Кабельне ТБ", "CTV");


        Set<TariffServices> tariffServices = tariffServicesDAO.getAll(lang, "", con);
        assertNotNull(tariffServices);
        assertEquals(3, tariffServices.size());
    }

    @Test
    void getTariffsCount() throws SQLException {
        String lang = "en";
        createTariff(1, 1, 3.5, "Internet", "Інтернет", "I");
        createTariff(2, 2, 5.5, "Cell", "Телефон", "C");
        createTariff(3, 3, 2.5, "Cable TV", "Кабельне ТБ", "CTV");
        createTariff(4, 4, 5.5, "IP-TV", "IP-TV", "IPTV");
        int expected = 4;
        int actual = tariffServicesDAO.getTariffsCount("", con);
        assertEquals(expected, actual);
    }

    @Test
    void getAllServices() throws SQLException {

        String lang = "en";
        createTariff(1, 1, 3.5, "Internet", "Інтернет", "I");
        createTariff(2, 2, 5.5, "Cell", "Телефон", "C");
        createTariff(3, 3, 2.5, "Cable TV", "Кабельне ТБ", "CTV");
        createTariff(4, 4, 5.5, "IP-TV", "IP-TV", "IPTV");

        String orderBy = "order by u.id OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";

        Service internet = new Service.Builder().withId(1).withTitle("Internet").withCode("I").build();
        Service cell = new Service.Builder().withId(2).withTitle("Cell").withCode("C").build();
        Service cableTV = new Service.Builder().withId(3).withTitle("Cable TV").withCode("CTV").build();
        Service IPTV = new Service.Builder().withId(4).withTitle("IP-TV").withCode("IPTV").build();

        Set<Service> services = tariffServicesDAO.getAllServices(lang, con);
        assertNotNull(services);
        assertTrue(services.containsAll(Arrays.asList(internet, cell, cableTV, IPTV)));
    }

    @Test
    void getAllServicesByTariffId() throws SQLException {
        String lang = "en";
        createTariff(1, 1, 3.5, "Internet", "Інтернет", "I");
        createTariff(2, 2, 5.5, "Cell", "Телефон", "C");
        createTariff(3, 3, 2.5, "Cable TV", "Кабельне ТБ", "CTV");
        createTariff(4, 4, 5.5, "IP-TV", "IP-TV", "IPTV");

        Service cableTV = new Service.Builder().withId(3).withTitle("Cable TV").withCode("CTV").build();

        Set<Service> services = tariffServicesDAO.getAllServicesByTariffId(3, lang, con);
        assertNotNull(services);
        assertTrue(services.contains(cableTV));
    }

    @Test
    void updateTariff() throws SQLException {
        String lang = "en";
        createTariff(1, 1, 3.5, "Internet", "Інтернет", "I");

        TariffServices tariffServices = tariffServicesDAO.getTariffWithServices(lang, 1, con);
        assertNotNull(tariffServices);
        String newCode = "I";
        String newTitleEn = "Internet";
        String newTitleUk = "Інтернет";
        double newPrice = 5.0;

        Tariff tariff = tariffServices.getTariff();
        tariff.setCode(newCode);
        tariff.setTitle(newTitleEn);
        tariff.setPrice(newPrice);

        tariffServicesDAO.updateTariff(tariff.getId(), tariff.getCode(), tariff.getPrice(), tariff.getTitle(), newTitleUk, Arrays.asList(1), con);

        TariffServices newTariffServices = tariffServicesDAO.getTariffWithServices(lang, 1, con);
        assertNotNull(newTariffServices);

        Tariff newTariff = newTariffServices.getTariff();
        assertEquals(newCode, newTariff.getCode());
        assertEquals(newTitleEn, newTariff.getTitle());
        assertEquals(newPrice, newTariff.getPrice());

    }

    @Test
    void insertTariff() throws SQLException {

        String lang = "en";

        double price = 5;
        String title = "IP-TV";
        String code = "IPTV";

        Tariff tariff = new Tariff.Builder().withPrice(price).withTitle(title).withCode(code).build();
        Service service = new Service.Builder().withId(4).withTitle(title).withCode(code).build();
        Set<Service> services = Set.of(service);

        TariffServices expected = new TariffServices.Builder().withTariff(tariff).withServices(services).build();

        createTariff(1, 1, 4, "Internet", "Інтернет", "I");

        tariffServicesDAO.insertTariff(price, code, title, title, Arrays.asList(1), con);

        TariffServices actual = tariffServicesDAO.getTariffWithServices("en", 2, con);

        assertNotNull(actual);
        assertEquals(price, actual.getTariff().getPrice());
        assertEquals(title, actual.getTariff().getTitle());
        assertEquals(code, actual.getTariff().getCode());

    }

    @Test
    void deleteTariff() throws SQLException {
        String lang = "en";
        createTariff(1, 1, 3.5, "Internet", "Інтернет", "I");
        createTariff(2, 2, 5.5, "Cell", "Телефон", "C");
        createTariff(3, 3, 2.5, "Cable TV", "Кабельне ТБ", "CTV");
        createTariff(4, 4, 5.5, "IP-TV", "IP-TV", "IPTV");

        int beforeDeleting = 4;
        assertEquals(beforeDeleting, tariffServicesDAO.getTariffsCount("", con));

        tariffServicesDAO.deleteTariff(3, con);

        int afterDeleting = 3;
        assertEquals(afterDeleting, tariffServicesDAO.getTariffsCount("", con));
    }

    @Test
    void updateTariffTitle() throws SQLException {
        String lang = "en";

        String newTitleEn = "Cell";
        String newTitleUk = "Телефон";

        createTariff(1, 1, 3.5, "Internet", "Інтернет", "I");

        tariffServicesDAO.updateTariffTitle(1, newTitleEn, lang, con);
        tariffServicesDAO.updateTariffTitle(1, newTitleUk, "uk", con);

        TariffServices tariffEn = tariffServicesDAO.getTariffWithServices(lang, 1, con);
        TariffServices tariffUk = tariffServicesDAO.getTariffWithServices("uk", 1, con);

        assertNotNull(tariffEn);
        assertNotNull(tariffUk);

        assertEquals(newTitleEn, tariffEn.getTariff().getTitle());
        assertEquals(newTitleUk, tariffUk.getTariff().getTitle());
    }

    @Test
    void setServicesForTariff() throws SQLException {
        String lang = "en";

        createTariff(1, 1, 3.5, "Internet", "Інтернет", "I");
        createTariff(2, 2, 5.5, "Cell", "Телефон", "C");
        createTariff(3, 3, 2.5, "Cable TV", "Кабельне ТБ", "CTV");
        createTariff(4, 4, 5.5, "IP-TV", "IP-TV", "IPTV");

        tariffServicesDAO.setServicesForTariff(1, Arrays.asList(1, 2, 3, 4), con);
        Set<Service> services = tariffServicesDAO.getAllServicesByTariffId(1, lang, con);

        assertNotNull(services);
        int expected = 4;
        int actual = services.size();
        assertEquals(expected, actual);
    }

    @Test
    void getAdditionalTariffTitle() throws SQLException {
        String langUk = "uk";
        String langEn = "en";

        createTariff(1, 1, 3.5, "Internet", "Інтернет", "I");

        String titleEn = tariffServicesDAO.getAdditionalTariffTitle(1, langUk, con);
        String titleUk = tariffServicesDAO.getAdditionalTariffTitle(1, langEn, con);

        assertNotNull(titleEn);
        assertNotNull(titleUk);

        assertEquals(titleEn, "Internet");
        assertEquals(titleUk, "Інтернет");
    }

    private void createTariff(int tariffId, int serviceId, double price, String titleEn, String titleUk, String code) throws SQLException {
        String tariffInsertion =
                "INSERT INTO tariffs ( price_per_day,tariff_code)" +
                        "VALUES(" + price + ",'" + code + "')";

        String serviceInsertion =
                "INSERT INTO services (id,service_code)" +
                        "VALUES(" + serviceId + ",'" + code + "')";

        String tariffServices =
                "INSERT INTO tariffs_services (tariff_id,service_id)" +
                        "VALUES(" + tariffId + "," + serviceId + ")";

        String tariffDescriptionEnInsertion =
                "INSERT INTO tariff_description(tariff_id,lang_id,title)" +
                        "VALUES(" + tariffId + "," + 1 + ",'" + titleEn + "')";

        String tariffDescriptionUkInsertion =
                "INSERT INTO tariff_description(tariff_id,lang_id,title)" +
                        "VALUES(" + tariffId + "," + 2 + ",'" + titleUk + "')";

        String serviceDescriptionEnInsertion =
                "INSERT INTO service_description(service_id,lang_id,title)" +
                        "VALUES(" + serviceId + "," + 1 + ",'" + titleEn + "')";

        String serviceDescriptionUkInsertion =
                "INSERT INTO service_description(service_id,lang_id,title)" +
                        "VALUES(" + serviceId + "," + 2 + ",'" + titleUk + "')";

        con.createStatement().executeUpdate(tariffInsertion);
        con.createStatement().executeUpdate(serviceInsertion);
        con.createStatement().executeUpdate(tariffServices);
        con.createStatement().executeUpdate(tariffDescriptionEnInsertion);
        con.createStatement().executeUpdate(tariffDescriptionUkInsertion);
        con.createStatement().executeUpdate(serviceDescriptionEnInsertion);
        con.createStatement().executeUpdate(serviceDescriptionUkInsertion);
    }
}