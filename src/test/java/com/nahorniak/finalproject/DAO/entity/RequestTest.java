package com.nahorniak.finalproject.DAO.entity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class RequestTest {

    Request request;

    @BeforeEach
    void setUp() {
        request = new Request.Builder().withId(1)
                .withUserId(2)
                .withTariffId(3)
                .withStartDate(LocalDate.parse("2022-02-23"))
                .withEndDate(LocalDate.parse("2022-02-25"))
                .withStatus("CLOSED")
                .build();
    }

    @Test
    void getId() {
        int expected = 1;
        int actual = request.getId();
        assertEquals(expected, actual);
    }

    @Test
    void setId() {
        int expected = 1;
        int actual = request.getId();
        assertEquals(expected, actual);

        expected = 4;
        request.setId(expected);
        actual = request.getId();
        assertEquals(expected, request.getId());
    }

    @Test
    void getUserId() {
        int expected = 2;
        int actual = request.getUserId();
        assertEquals(expected, actual);
    }

    @Test
    void setUserId() {
        int expected = 2;
        int actual = request.getUserId();
        assertEquals(expected, actual);

        expected = 4;
        request.setUserId(expected);
        actual = request.getUserId();
        assertEquals(expected, actual);
    }


    @Test
    void getTariffId() {
        int expected = 3;
        int actual = request.getTariffId();
        assertEquals(expected, actual);
    }

    @Test
    void setTariffId() {
        int expected = 3;
        int actual = request.getTariffId();
        assertEquals(expected, actual);

        expected = 5;
        request.setTariffId(expected);
        actual = request.getTariffId();
        assertEquals(expected, actual);
    }

    @Test
    void getStartDate() {
        LocalDate expected = LocalDate.parse("2022-02-23");
        LocalDate actual = request.getStartDate();
        assertEquals(actual, expected);
    }

    @Test
    void setStartDate() {
        LocalDate expected = LocalDate.parse("2022-02-23");
        LocalDate actual = request.getStartDate();
        assertEquals(actual, expected);

        expected = LocalDate.parse("2022-04-23");
        request.setStartDate(expected);
        actual = request.getStartDate();
        assertEquals(actual, expected);
    }

    @Test
    void getEndDate() {
        LocalDate expected = LocalDate.parse("2022-02-25");
        LocalDate actual = request.getEndDate();
        assertEquals(actual, expected);
    }

    @Test
    void setEndDate() {
        LocalDate expected = LocalDate.parse("2022-02-25");
        LocalDate actual = request.getEndDate();
        assertEquals(actual, expected);

        expected = LocalDate.parse("2022-04-23");
        request.setEndDate(expected);
        actual = request.getEndDate();
        assertEquals(actual, expected);
    }

    @Test
    void getStatus() {
        Status expected = Status.CLOSED;
        Status actual = request.getStatus();
        assertEquals(expected, actual);
    }

    @Test
    void setStatus() {
        Status expected = Status.CLOSED;
        Status actual = request.getStatus();
        assertEquals(expected, actual);

        expected = Status.ACTIVE;
        request.setStatus(expected);
        actual = request.getStatus();
        assertEquals(expected, actual);
    }

    @Test
    void testEquals() {
        Request newRequest = new Request.Builder().withId(1)
                .withUserId(2)
                .withTariffId(3)
                .withStartDate(LocalDate.parse("2022-02-23"))
                .withEndDate(LocalDate.parse("2022-02-25"))
                .withStatus("CLOSED")
                .build();

        assertTrue(request.equals(newRequest));
    }

    @Test
    void testHashCode() {
        Request newRequest = new Request.Builder().withId(1)
                .withUserId(2)
                .withTariffId(3)
                .withStartDate(LocalDate.parse("2022-02-23"))
                .withEndDate(LocalDate.parse("2022-02-25"))
                .withStatus("CLOSED")
                .build();

        assertTrue(request.hashCode() == newRequest.hashCode());
    }
}