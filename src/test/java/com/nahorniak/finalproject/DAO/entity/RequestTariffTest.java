package com.nahorniak.finalproject.DAO.entity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class RequestTariffTest {

    RequestTariff requestTariff;

    @BeforeEach
    void setUp() {
        Request request = new Request.Builder().withId(1)
                .withUserId(2)
                .withTariffId(3)
                .withStartDate(LocalDate.parse("2022-02-23"))
                .withEndDate(LocalDate.parse("2022-02-25"))
                .withStatus("CLOSED")
                .build();

        Tariff tariff = new Tariff.Builder().withId(1)
                .withTitle("Standard")
                .withCode("#ST")
                .withPrice(3.5)
                .build();

        Service service1 = new Service.Builder().withId(1)
                .withTitle("Internet")
                .withCode("#I")
                .build();

        Service service2 = new Service.Builder().withId(2)
                .withTitle("Cell")
                .withCode("#C")
                .build();

        Set<Service> services = Set.of(service1, service2);

        TariffServices tariffServices = new TariffServices.Builder().withTariff(tariff).withServices(services).build();

        requestTariff = new RequestTariff.Builder().withRequest(request).withTariffServices(tariffServices).build();
    }

    @Test
    void getRequest() {
        Request expected = new Request.Builder().withId(1)
                .withUserId(2)
                .withTariffId(3)
                .withStartDate(LocalDate.parse("2022-02-23"))
                .withEndDate(LocalDate.parse("2022-02-25"))
                .withStatus("CLOSED")
                .build();

        Request actual = requestTariff.getRequest();
        assertNotNull(actual);
        assertEquals(expected, actual);
    }

    @Test
    void setRequest() {
        Request expected = new Request.Builder().withId(1)
                .withUserId(2)
                .withTariffId(3)
                .withStartDate(LocalDate.parse("2022-02-23"))
                .withEndDate(LocalDate.parse("2022-02-25"))
                .withStatus("CLOSED")
                .build();

        Request actual = requestTariff.getRequest();
        assertNotNull(actual);
        assertEquals(expected, actual);

        expected = new Request.Builder().withId(2)
                .withUserId(3)
                .withTariffId(4)
                .withStartDate(LocalDate.parse("2022-02-25"))
                .withEndDate(LocalDate.parse("2022-02-27"))
                .withStatus("ACTIVE")
                .build();

        requestTariff.setRequest(expected);
        actual = requestTariff.getRequest();
        assertNotNull(actual);
        assertEquals(expected, actual);
    }

    @Test
    void getTariffServices() {
        Tariff tariff = new Tariff.Builder().withId(1)
                .withTitle("Standard")
                .withCode("#ST")
                .withPrice(3.5)
                .build();

        Service service1 = new Service.Builder().withId(1)
                .withTitle("Internet")
                .withCode("#I")
                .build();

        Service service2 = new Service.Builder().withId(2)
                .withTitle("Cell")
                .withCode("#C")
                .build();

        Set<Service> services = Set.of(service1, service2);

        TariffServices expected = new TariffServices.Builder().withTariff(tariff).withServices(services).build();

        TariffServices actual = requestTariff.getTariffServices();
        assertNotNull(actual);
        assertEquals(expected, actual);

    }

    @Test
    void setTariffServices() {
        Tariff tariff = new Tariff.Builder().withId(1)
                .withTitle("Standard")
                .withCode("#ST")
                .withPrice(3.5)
                .build();

        Service service1 = new Service.Builder().withId(1)
                .withTitle("Internet")
                .withCode("#I")
                .build();

        Service service2 = new Service.Builder().withId(2)
                .withTitle("Cell")
                .withCode("#C")
                .build();

        Service service3 = new Service.Builder().withId(2)
                .withTitle("IP-TV")
                .withCode("#IPTV")
                .build();

        Set<Service> services = Set.of(service1, service2, service3);

        TariffServices expected = new TariffServices.Builder().withTariff(tariff).withServices(services).build();

        requestTariff.setTariffServices(expected);
        TariffServices actual = requestTariff.getTariffServices();

        assertNotNull(actual);
        assertEquals(expected, actual);
        assertEquals(3, actual.getServices().size());
    }
}