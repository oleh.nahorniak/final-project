package com.nahorniak.finalproject.DAO;

import com.nahorniak.finalproject.DAO.entity.Request;
import com.nahorniak.finalproject.DAO.entity.Status;
import org.junit.jupiter.api.*;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class RequestDAOTest {

    private static final String CONNECTION_URL = "jdbc:derby:memory:testdb;create=true";

    private static final String SHUTDOWN_URL = "jdbc:derby:;shutdown=true";

    private static final String DROP_USERS_TABLE = "DROP TABLE users";
    private static final String DROP_ADDRESS_TABLE = "DROP TABLE address";
    private static final String DROP_TARIFFS_TABLE = "DROP TABLE tariffs  ";
    private static final String DROP_SERVICES_TABLE = "DROP TABLE services  ";
    private static final String DROP_LANG_TABLE = "DROP TABLE language ";
    private static final String DROP_TARIFF_DESCRIPTION_TABLE = "DROP TABLE tariff_description  ";
    private static final String DROP_SERVICE_DESCRIPTION_TABLE = "DROP TABLE service_description  ";
    private static final String DROP_TARIFFS_SERVICES_TABLE = "DROP TABLE tariffs_services  ";
    private static final String DROP_REQUEST_TABLE = "DROP TABLE request  ";

    private static final String CREATE_REQUEST_TABLE =
            "CREATE TABLE request " +
                    "( " +
                    "    id             INT GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1)," +
                    "    user_id        INT                              NOT NULL," +
                    "    tariff_id      INT                              NOT NULL," +
                    "    start_date     DATE                             NOT NULL DEFAULT CURRENT_DATE," +
                    "    end_date       DATE                             NOT NULL," +
                    "    status         VARCHAR(50)                      NOT NULL DEFAULT 'ACTIVE'," +
                    "    CONSTRAINT pk_request PRIMARY KEY (id)," +
                    "    CONSTRAINT FK_user foreign key (user_id) references users(id) on delete cascade," +
                    "    CONSTRAINT FK_tariffId foreign key (tariff_id) references tariffs(id) on delete cascade" +
                    ")";

    private static final String CREATE_TARIFFS_TABLE =
            "CREATE TABLE tariffs " +
                    "( " +
                    "    id             INT GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1)," +
                    "    price_per_day  numeric                              NOT NULL," +
                    "    tariff_code        VARCHAR(50)                      NOT NULL," +
                    "    CONSTRAINT pk_tariff PRIMARY KEY (id)" +
                    ")";

    private static final String CREATE_SERVICES_TABLE =
            "CREATE TABLE services " +
                    "( " +
                    "    id             INT ," +
                    "    service_code        VARCHAR(50)                      NOT NULL," +
                    "    CONSTRAINT pk_service PRIMARY KEY (id)" +
                    ")";

    private static final String CREATE_LANG_TABLE =
            "CREATE TABLE language " +
                    "( " +
                    "    id             INT ," +
                    "    name        VARCHAR(50)                      NOT NULL," +
                    "    CONSTRAINT language_pkey PRIMARY KEY (id) " +
                    ")";

    private static final String CREATE_TARIFF_DESCRIPTION_TABLE =
            "CREATE TABLE tariff_description " +
                    "( " +
                    "    tariff_id             INT," +
                    "    lang_id             INT  ," +
                    "    title        VARCHAR(50)                      NOT NULL," +
                    "    CONSTRAINT FK_tariff foreign key (tariff_id) references tariffs(id) on delete cascade ," +
                    "    CONSTRAINT FK_lang foreign key (lang_id) references language (id) on delete cascade " +
                    ")";

    private static final String CREATE_SERVICE_DESCRIPTION_TABLE =
            "CREATE TABLE service_description " +
                    "( " +
                    "    service_id             INT  ," +
                    "    lang_id             INT ," +
                    "    title        VARCHAR(50)                      NOT NULL," +
                    "    CONSTRAINT FK_service foreign key (service_id) references services(id) on delete cascade ," +
                    "    CONSTRAINT FK_lang1 foreign key (lang_id) references language (id) on delete cascade " +
                    ")";

    private static final String CREATE_TARIFF_SERVICES_TABLE =
            "CREATE TABLE tariffs_services " +
                    "( " +
                    "    tariff_id             INT ," +
                    "    service_id             INT ," +
                    "    CONSTRAINT PK_tariffs_services PRIMARY KEY (tariff_id, service_id) , " +
                    "    CONSTRAINT FK_tariff1 foreign key (tariff_id) references tariffs(id) on delete cascade," +
                    "    CONSTRAINT FK_service1 foreign key (service_id) references services(id) on delete cascade" +
                    ")";
    private static final String CREATE_ADDRESS_TABLE =
            "CREATE TABLE address " +
                    "( " +
                    "    id             INT PRIMARY KEY ," +
                    "    country        VARCHAR(50)                              NOT NULL," +
                    "    city        VARCHAR(50)                              NOT NULL," +
                    "    street        VARCHAR(50)                              NOT NULL" +
                    ")";

    private static final String CREATE_USERS_TABLE =
            "CREATE TABLE users " +
                    "(\n" +
                    "    id             INT PRIMARY KEY ,\n" +
                    "    user_role      VARCHAR(50)                              NOT NULL,\n" +
                    "    login          VARCHAR(50)                              NOT NULL,\n" +
                    "    password       VARCHAR(50)                              NOT NULL,\n" +
                    "    first_name     VARCHAR(50)                              NOT NULL,\n" +
                    "    last_name      VARCHAR(50)                              NOT NULL,\n" +
                    "    email          VARCHAR(50)                              NOT NULL,\n" +
                    "    phone_number   VARCHAR(25)                              NOT NULL,\n" +
                    "    balance        numeric(5,2)                             NOT NULL,\n" +
                    "    address_id     INT                                      REFERENCES address(id) on delete cascade,\n" +
                    "    is_blocked     boolean                                  NOT NULL\n" +
                    ")";


    static Connection con;
    static RequestDAO requestDAO;

    @BeforeAll
    static void globalSetUp() throws SQLException, IOException {
        con = DriverManager.getConnection(CONNECTION_URL);
    }


    @BeforeEach
    void setUp() throws SQLException {
        requestDAO = RequestDAO.getInstance();
        con.createStatement().executeUpdate(CREATE_ADDRESS_TABLE);
        con.createStatement().executeUpdate(CREATE_USERS_TABLE);
        con.createStatement().executeUpdate(CREATE_TARIFFS_TABLE);
        con.createStatement().executeUpdate(CREATE_SERVICES_TABLE);
        con.createStatement().executeUpdate(CREATE_TARIFF_SERVICES_TABLE);
        con.createStatement().executeUpdate(CREATE_LANG_TABLE);
        con.createStatement().executeUpdate(CREATE_TARIFF_DESCRIPTION_TABLE);
        con.createStatement().executeUpdate(CREATE_SERVICE_DESCRIPTION_TABLE);
        con.createStatement().executeUpdate(CREATE_REQUEST_TABLE);

        String englishInsertion =
                "INSERT INTO language (id, name)" +
                        "VALUES(" + 1 + ", 'en')";

        String ukrainianInsertion =
                "INSERT INTO language (id, name)" +
                        "VALUES(" + 2 + ", 'uk')";


        con.createStatement().executeUpdate(englishInsertion);
        con.createStatement().executeUpdate(ukrainianInsertion);

        insertInTable(1, "admin", "12345Qwerty");
        createTariff(1, 1, 3.5, "Internet", "Інтернет", "I");
        createTariff(2, 2, 5.5, "Cell", "Телефон", "C");

    }

    @AfterEach
    void tearDown() throws SQLException {
        con.createStatement().executeUpdate(DROP_REQUEST_TABLE);
        con.createStatement().executeUpdate(DROP_USERS_TABLE);
        con.createStatement().executeUpdate(DROP_ADDRESS_TABLE);
        con.createStatement().executeUpdate(DROP_TARIFFS_SERVICES_TABLE);
        con.createStatement().executeUpdate(DROP_TARIFF_DESCRIPTION_TABLE);
        con.createStatement().executeUpdate(DROP_SERVICE_DESCRIPTION_TABLE);
        con.createStatement().executeUpdate(DROP_LANG_TABLE);
        con.createStatement().executeUpdate(DROP_TARIFFS_TABLE);
        con.createStatement().executeUpdate(DROP_SERVICES_TABLE);


    }

    @Test
    void getInstance() {
        RequestDAO requestDAO = RequestDAO.getInstance();
        assertNotNull(requestDAO);
    }

    @Test
    void getAllByUserId() throws SQLException {
        int userId = 1;
        createRequest(userId, 1, "2022-02-23", "2022-02-25", "CLOSED");
        createRequest(userId, 2, "2022-02-25", "2022-02-27", "CLOSED");
        createRequest(userId, 1, "2022-02-28", "2022-03-30", "SUSPENDED");

        String orderBy = "order by id OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";
        int start = 1;
        int count = 2;

        List<Request> request = requestDAO.getAllByUserId(userId, orderBy, start, count, con);
        assertNotNull(request);
        assertEquals(2, request.size());
    }

    @Test
    void testGetAllByUserId() throws SQLException {
        int userId = 1;
        createRequest(userId, 1, "2022-02-23", "2022-02-25", "CLOSED");
        createRequest(userId, 2, "2022-02-25", "2022-02-27", "CLOSED");
        createRequest(userId, 1, "2022-02-28", "2022-03-30", "CLOSED");
        createRequest(userId, 1, "2022-04-01", "2022-04-07", "ACTIVE");
        String orderBy = "order by id ";

        List<Request> requests = requestDAO.getAllByUserId(userId, orderBy, con);
        assertNotNull(requests);
        assertEquals(4, requests.size());

    }

    @Test
    void isActiveRequest() throws SQLException {
        int userId = 1;
        createRequest(userId, 1, "2022-02-23", "2022-02-25", "CLOSED");
        createRequest(userId, 2, "2022-02-25", "2022-02-27", "CLOSED");
        createRequest(userId, 1, "2022-02-28", "2022-03-30", "CLOSED");
        createRequest(userId, 1, "2022-04-01", "2022-04-07", "ACTIVE");

        boolean expected = true;
        boolean actual = requestDAO.isActiveRequest(userId, con);
        assertEquals(expected, actual);
    }

    @Test
    void isSuspendedRequest() throws SQLException {
        int userId = 1;
        createRequest(userId, 1, "2022-02-23", "2022-02-25", "CLOSED");
        createRequest(userId, 2, "2022-02-25", "2022-02-27", "CLOSED");
        createRequest(userId, 1, "2022-04-01", "2022-04-07", "ACTIVE");

        boolean expected = false;
        boolean actual = requestDAO.isSuspendedRequest(userId, con);
        assertEquals(expected, actual);
    }

    @Test
    void getActiveOrSuspendedRequest() throws SQLException {
        int userId = 1;
        createRequest(userId, 1, "2022-02-23", "2022-02-25", "CLOSED");
        createRequest(userId, 2, "2022-02-25", "2022-02-27", "CLOSED");
        createRequest(userId, 1, "2022-04-01", "2022-04-07", "ACTIVE");

        Request expected = new Request.Builder()
                .withId(3)
                .withUserId(1)
                .withTariffId(1)
                .withStartDate(LocalDate.parse("2022-04-01"))
                .withEndDate(LocalDate.parse("2022-04-07"))
                .withStatus("ACTIVE").build();

        Request active = requestDAO.getActiveOrSuspendedRequest(userId, "ACTIVE", con);
        Request suspended = requestDAO.getActiveOrSuspendedRequest(userId, "SUSPENDED", con);
        assertNotNull(active);
        assertNull(suspended);
        assertEquals(expected, active);
    }


    @Test
    void insertRequest() throws SQLException {

        int userId = 1;
        Request expected = new Request.Builder()
                .withId(1)
                .withUserId(userId)
                .withTariffId(1)
                .withStartDate(LocalDate.now())
                .withEndDate(LocalDate.parse("2022-04-07"))
                .withStatus("ACTIVE").build();

        requestDAO.insertRequest(expected, con);
        Request actual = requestDAO.getActiveOrSuspendedRequest(userId, "ACTIVE", con);
        assertNotNull(actual);
        assertEquals(expected, actual);

    }

    @Test
    void getCount() throws SQLException {
        int userId = 1;
        createRequest(userId, 1, "2022-02-23", "2022-02-25", "CLOSED");
        createRequest(userId, 2, "2022-02-25", "2022-02-27", "CLOSED");
        createRequest(userId, 1, "2022-04-01", "2022-04-07", "ACTIVE");

        int expected = 3;
        int actual = requestDAO.getCount(userId, con);

        assertEquals(expected, actual);
    }


    private void createRequest(int userId, int tariffId, String startDate, String endDate, String status) throws SQLException {

        String requestInsertion =
                "INSERT INTO request (user_id,tariff_id,start_date,end_date,status)" +
                        "VALUES(" + userId + "," + tariffId + ",'" + startDate + "','" + endDate + "','" + status + "')";

        con.createStatement().executeUpdate(requestInsertion);

    }

    private void createTariff(int tariffId, int serviceId, double price, String titleEn, String titleUk, String code) throws SQLException {
        String tariffInsertion =
                "INSERT INTO tariffs ( price_per_day,tariff_code)" +
                        "VALUES(" + price + ",'" + code + "')";

        String serviceInsertion =
                "INSERT INTO services (id,service_code)" +
                        "VALUES(" + serviceId + ",'" + code + "')";

        String tariffServices =
                "INSERT INTO tariffs_services (tariff_id,service_id)" +
                        "VALUES(" + tariffId + "," + serviceId + ")";

        String tariffDescriptionEnInsertion =
                "INSERT INTO tariff_description(tariff_id,lang_id,title)" +
                        "VALUES(" + tariffId + "," + 1 + ",'" + titleEn + "')";

        String tariffDescriptionUkInsertion =
                "INSERT INTO tariff_description(tariff_id,lang_id,title)" +
                        "VALUES(" + tariffId + "," + 2 + ",'" + titleUk + "')";

        String serviceDescriptionEnInsertion =
                "INSERT INTO service_description(service_id,lang_id,title)" +
                        "VALUES(" + serviceId + "," + 1 + ",'" + titleEn + "')";

        String serviceDescriptionUkInsertion =
                "INSERT INTO service_description(service_id,lang_id,title)" +
                        "VALUES(" + serviceId + "," + 2 + ",'" + titleUk + "')";

        con.createStatement().executeUpdate(tariffInsertion);
        con.createStatement().executeUpdate(serviceInsertion);
        con.createStatement().executeUpdate(tariffServices);
        con.createStatement().executeUpdate(tariffDescriptionEnInsertion);
        con.createStatement().executeUpdate(tariffDescriptionUkInsertion);
        con.createStatement().executeUpdate(serviceDescriptionEnInsertion);
        con.createStatement().executeUpdate(serviceDescriptionUkInsertion);
    }

    private void insertInTable(int userId, String login, String password) throws SQLException {

        String addressInsertion =
                "INSERT INTO address (id, country, city, street)" +
                        "VALUES(" + userId + ", 'Ukraine', 'Lviv', 'Shevchenko 123')";

        String userInsertion =
                "INSERT INTO users (id, user_role, login, password, first_name," +
                        " last_name, email,phone_number,balance,address_id,is_blocked)" +
                        "VALUES(" + userId + ", 'CUSTOMER', '" + login + "', '" + password + "','Oleh', 'Dmytrov', 'nagornyak68@gmail.com','333-333-1111',0," + userId + ",true)";

        con.createStatement().executeUpdate(addressInsertion);
        con.createStatement().executeUpdate(userInsertion);
    }


}