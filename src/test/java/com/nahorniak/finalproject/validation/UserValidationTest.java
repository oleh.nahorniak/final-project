package com.nahorniak.finalproject.validation;

import com.nahorniak.finalproject.DAO.entity.User;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class UserValidationTest {

    @ParameterizedTest
    @MethodSource("testCases")
    void isValidUser(User user, boolean expected) {
        boolean actual = UserValidation.isValidUser(user);
        Assert.assertEquals(expected, actual);

    }

    static Stream<Arguments> testCases() {

        User test1 = new User.Builder().withLogin("admin")
                .withPassword("12345Test")
                .withFirstName("Dmytro")
                .withLastName("Dmytrov")
                .withEmail("testing@gmail.com")
                .withPhoneNumber("111-111-1111")
                .withRole("ADMIN")
                .build();

        User test2 = new User.Builder().withLogin("test")
                .withPassword("111222Qwerty")
                .withFirstName("Taras")
                .withLastName("Tarasov")
                .withEmail("nagornyak68@gmail.com")
                .withPhoneNumber("121-121-1212")
                .withRole("CUSTOMER")
                .withCountry("Ukraine")
                .withCity("Lviv")
                .withStreet("Shevchenko street, 130A")
                .build();

        User test3 = new User.Builder().withLogin("test")
                .withPassword("1y")
                .withFirstName("Taras")
                .withLastName("Tarasov")
                .withEmail("nagornyak68@gmail.com")
                .withPhoneNumber("121-121-1212")
                .withRole("CUSTOMER")
                .withCountry("Ukraine")
                .withCity("Lviv")
                .withStreet("Shevchenko street, 130A")
                .build();


        return Stream.of(
                Arguments.of(test1, true),
                Arguments.of(test2, true),
                Arguments.of(test3, false));
    }
}