package com.nahorniak.finalproject.validation;

import com.nahorniak.finalproject.DAO.entity.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class TariffValidationTest {

    @ParameterizedTest
    @MethodSource("testCases")
    void isValid(String titleEn, String titleUk, String code, double price, boolean expected) {
        boolean actual = TariffValidation.isValid(titleEn, titleUk, code, price);
        assertEquals(expected, actual);
    }

    static Stream<Arguments> testCases() {
        return Stream.of(
                Arguments.of("Internet", "Інтернет", "#I", 5, true),
                Arguments.of("Інтернет", "Internet", "#I", 5, false),
                Arguments.of("Internet", "Інтернет", "#I", -5, false),
                Arguments.of("Internet", "Інтернет", "Is", 5, false));
    }
}