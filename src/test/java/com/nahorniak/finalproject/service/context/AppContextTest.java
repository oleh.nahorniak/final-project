package com.nahorniak.finalproject.service.context;

import com.nahorniak.finalproject.service.RequestService;
import com.nahorniak.finalproject.service.TariffService;
import com.nahorniak.finalproject.service.TransactionService;
import com.nahorniak.finalproject.service.UserService;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AppContextTest {

    @Test
    void getInstance() {
        AppContext appContext = AppContext.getInstance();
        assertNotNull(appContext);
    }

    @Test
    void getRequestService() {
        AppContext appContext = AppContext.getInstance();
        RequestService requestService = appContext.getRequestService();
        assertNotNull(requestService);
    }

    @Test
    void getTariffService() {
        AppContext appContext = AppContext.getInstance();
        TariffService tariffService = AppContext.getInstance().getTariffService();
        assertNotNull(tariffService);
    }

    @Test
    void getUserService() {
        AppContext appContext = AppContext.getInstance();
        UserService userService = appContext.getUserService();
        assertNotNull(userService);
    }

    @Test
    void getTransactionService() {
        AppContext appContext = AppContext.getInstance();
        TransactionService transactionService = appContext.getTransactionService();
        assertNotNull(transactionService);
    }
}