package com.nahorniak.finalproject.util.PDF;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.nahorniak.finalproject.DAO.entity.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * CustomersPDFBuilder - class to save customers in pdf
 *
 * @author Oleh Nahorniak.
 */
public class CustomersPDFBuilder implements PDFBuilder {

    public void openPDF(HttpServletRequest request, HttpServletResponse response, List<User> users) {
        ResourceBundle resourceBundle = ResourceBundle.getBundle("text",
                new Locale((String) request.getSession().getAttribute("lang")));

        Document document = new Document(PageSize.A4, 50, 50, 50, 50);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            PdfWriter.getInstance(document, baos);
            document.open();


            BaseFont arial = BaseFont.createFont("C:\\Windows\\Fonts\\arial.ttf", "cp1251",
                    BaseFont.EMBEDDED);

            Paragraph title = new Paragraph(resourceBundle.getString("Customers"), new Font(arial, 16));
            title.setAlignment(Element.ALIGN_CENTER);
            Chapter chapter = new Chapter(title, 1);

            chapter.setNumberDepth(0);

            document.add(chapter);

            PdfPTable pdfPTable = new PdfPTable(11);
            pdfPTable.setSpacingBefore(25);
            pdfPTable.setWidthPercentage(100);
            Font font1 = new Font(arial, 11);
            Font font = new Font(arial, 8);

            PdfPCell pdfPCell1 = new PdfPCell(new Paragraph("#", font1));
            PdfPCell pdfPCell2 = new PdfPCell(new Paragraph(resourceBundle.getString("USER_LOGIN"), font1));
            PdfPCell pdfPCell3 = new PdfPCell(new Paragraph(resourceBundle.getString("FIRST_NAME"), font1));
            PdfPCell pdfPCell4 = new PdfPCell(new Paragraph(resourceBundle.getString("LAST_NAME"), font1));
            PdfPCell pdfPCell5 = new PdfPCell(new Paragraph(resourceBundle.getString("Balance"), font1));
            PdfPCell pdfPCell6 = new PdfPCell(new Paragraph(resourceBundle.getString("Email"), font1));
            PdfPCell pdfPCell7 = new PdfPCell(new Paragraph(resourceBundle.getString("PHONE_NUMBER"), font1));
            PdfPCell pdfPCell8 = new PdfPCell(new Paragraph(resourceBundle.getString("Country"), font1));
            PdfPCell pdfPCell9 = new PdfPCell(new Paragraph(resourceBundle.getString("City"), font1));
            PdfPCell pdfPCell10 = new PdfPCell(new Paragraph(resourceBundle.getString("Street"), font1));
            PdfPCell pdfPCell11 = new PdfPCell(new Paragraph(resourceBundle.getString("Status"), font1));

            pdfPTable.addCell(pdfPCell1);
            pdfPTable.addCell(pdfPCell2);
            pdfPTable.addCell(pdfPCell3);
            pdfPTable.addCell(pdfPCell4);
            pdfPTable.addCell(pdfPCell5);
            pdfPTable.addCell(pdfPCell6);
            pdfPTable.addCell(pdfPCell7);
            pdfPTable.addCell(pdfPCell8);
            pdfPTable.addCell(pdfPCell9);
            pdfPTable.addCell(pdfPCell10);
            pdfPTable.addCell(pdfPCell11);

            document.add(pdfPTable);

            int i = 1;
            for (User user : users) {
                String status = user.getIsBlocked() == false ? "UNBLOCKED" : "BLOCKED";
                PdfPTable table = new PdfPTable(11);
                table.setWidthPercentage(100);
                table.addCell(new Phrase(String.valueOf(i++), font));
                table.addCell(new Phrase(user.getLogin(), font));
                table.addCell(new Phrase(user.getFirstName(), font));
                table.addCell(new Phrase(user.getLastName(), font));
                table.addCell(new Phrase(String.valueOf(user.getBalance()) + " $", font));
                table.addCell(new Phrase(user.getEmail(), font));
                table.addCell(new Phrase(user.getPhoneNumber(), font));
                table.addCell(new Phrase(user.getCountry(), font));
                table.addCell(new Phrase(user.getCity(), font));
                table.addCell(new Phrase(user.getStreet(), font));
                table.addCell(new Phrase(resourceBundle.getString(status), font));

                document.add(table);
            }

            Paragraph footer = new Paragraph("© Internet Service Provider 2022", new Font(arial, 10));
            footer.setSpacingBefore(15);
            footer.setAlignment(Element.ALIGN_CENTER);
            document.add(footer);

            document.close();

            String filename = "\"Customers_" + LocalDate.now() + ".pdf\"";
            openInBrowser(response, baos, filename);
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
