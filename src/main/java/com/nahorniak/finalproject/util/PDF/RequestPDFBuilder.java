package com.nahorniak.finalproject.util.PDF;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.nahorniak.finalproject.DAO.entity.Request;
import com.nahorniak.finalproject.DAO.entity.RequestTariff;
import com.nahorniak.finalproject.DAO.entity.Tariff;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * RequestPDFBuilder - class to save requests in pdf
 *
 * @author Oleh Nahorniak.
 */
public class RequestPDFBuilder implements PDFBuilder {

    public void openPDF(HttpServletRequest request, HttpServletResponse response, List<RequestTariff> requests) {
        ResourceBundle resourceBundle = ResourceBundle.getBundle("text",
                new Locale((String) request.getSession().getAttribute("lang")));

        Document document = new Document(PageSize.A4, 50, 50, 50, 50);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        try {
            PdfWriter.getInstance(document, baos);
            document.open();

            BaseFont arial = BaseFont.createFont("C:\\Windows\\Fonts\\arial.ttf", "cp1251",
                    BaseFont.EMBEDDED);

            Paragraph title = new Paragraph(resourceBundle.getString("REQUESTS"), new Font(arial, 16));
            title.setAlignment(Element.ALIGN_CENTER);
            Chapter chapter = new Chapter(title, 1);

            chapter.setNumberDepth(0);

            document.add(chapter);

            PdfPTable pdfPTable = new PdfPTable(6);
            pdfPTable.setSpacingBefore(25);
            pdfPTable.setWidthPercentage(100);
            Font font1 = new Font(arial, 16);
            Font font = new Font(arial, 12);

            PdfPCell pdfPCell1 = new PdfPCell(new Paragraph("#", font1));
            PdfPCell pdfPCell2 = new PdfPCell(new Paragraph(resourceBundle.getString("TARIFF_TITLE"), font1));
            PdfPCell pdfPCell3 = new PdfPCell(new Paragraph(resourceBundle.getString("Price"), font1));
            PdfPCell pdfPCell4 = new PdfPCell(new Paragraph(resourceBundle.getString("START_DATE"), font1));
            PdfPCell pdfPCell5 = new PdfPCell(new Paragraph(resourceBundle.getString("END_DATE"), font1));
            PdfPCell pdfPCell6 = new PdfPCell(new Paragraph(resourceBundle.getString("Status"), font1));

            pdfPTable.addCell(pdfPCell1);
            pdfPTable.addCell(pdfPCell2);
            pdfPTable.addCell(pdfPCell3);
            pdfPTable.addCell(pdfPCell4);
            pdfPTable.addCell(pdfPCell5);
            pdfPTable.addCell(pdfPCell6);
            document.add(pdfPTable);

            int i = 1;
            for (RequestTariff r : requests) {
                PdfPTable table = new PdfPTable(6);
                table.setWidthPercentage(100);


                Tariff tariff = r.getTariffServices().getTariff();
                Request req = r.getRequest();

                table.addCell(new Phrase(String.valueOf(i++), font));
                table.addCell(new Phrase(tariff.getTitle(), font));
                table.addCell(new Phrase(String.valueOf(tariff.getPrice()) + " $", font));
                table.addCell(new Phrase(String.valueOf(req.getStartDate()), font));
                table.addCell(new Phrase(String.valueOf(req.getEndDate()), font));
                table.addCell(new Phrase(String.valueOf(req.getStatus()), font));

                document.add(table);
            }

            Paragraph footer = new Paragraph("© Internet Service Provider 2022", new Font(arial, 10));
            footer.setSpacingBefore(15);
            footer.setAlignment(Element.ALIGN_CENTER);
            document.add(footer);

            document.close();

            String filename = "\"Requests_" + LocalDate.now() + ".pdf\"";
            openInBrowser(response, baos, filename);

        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
