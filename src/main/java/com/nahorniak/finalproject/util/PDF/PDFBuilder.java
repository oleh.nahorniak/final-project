package com.nahorniak.finalproject.util.PDF;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * PDFBuilder interface
 *
 * @author Oleh Nahorniak.
 */
public interface PDFBuilder {

    /**
     * method to open pdf in browser
     *
     * @param response
     * @param baos
     * @param filename
     */
    default void openInBrowser(HttpServletResponse response, ByteArrayOutputStream baos, String filename) {
        // setting some response headers

        response.setHeader("content-disposition", "filename=" + filename);
        response.setHeader("Expires", "0");
        response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
        response.setHeader("Pragma", "public");
        // setting the content type
        response.setContentType("application/pdf");
        // the content length
        response.setContentLength(baos.size());
        // write ByteArrayOutputStream to the ServletOutputStream
        OutputStream os = null;
        try {
            os = response.getOutputStream();
            baos.writeTo(os);
            os.flush();
            os.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
