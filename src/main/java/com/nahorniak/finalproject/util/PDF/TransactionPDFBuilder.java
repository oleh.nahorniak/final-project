package com.nahorniak.finalproject.util.PDF;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.nahorniak.finalproject.DAO.entity.Transaction;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * TransactionPDFBuilder - class to save transactions in pdf
 *
 * @author Oleh Nahorniak.
 */
public class TransactionPDFBuilder implements PDFBuilder {

    public void openPDF(HttpServletRequest request, HttpServletResponse response, List<Transaction> transactions) {


        ResourceBundle resourceBundle = ResourceBundle.getBundle("text",
                new Locale((String) request.getSession().getAttribute("lang")));

        Document document = new Document(PageSize.A4, 50, 50, 50, 50);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            PdfWriter.getInstance(document, baos);
            document.open();


            BaseFont arial = BaseFont.createFont("C:\\Windows\\Fonts\\arial.ttf", "cp1251",
                    BaseFont.EMBEDDED);

            Paragraph title = new Paragraph(resourceBundle.getString("TRANSACTIONS"), new Font(arial, 16));
            title.setAlignment(Element.ALIGN_CENTER);

            Chapter chapter = new Chapter(title, 1);

            chapter.setNumberDepth(0);

            document.add(chapter);

            PdfPTable pdfPTable = new PdfPTable(4);
            pdfPTable.setSpacingBefore(25);
            pdfPTable.setWidthPercentage(100);
            Font font1 = new Font(arial, 16);
            Font font = new Font(arial, 12);

            PdfPCell pdfPCell1 = new PdfPCell(new Paragraph("#", font1));
            PdfPCell pdfPCell2 = new PdfPCell(new Paragraph(resourceBundle.getString("TRANSACTION_DATE_TIME"), font1));
            PdfPCell pdfPCell3 = new PdfPCell(new Paragraph(resourceBundle.getString("TRANSACTION_AMOUNT"), font1));
            PdfPCell pdfPCell4 = new PdfPCell(new Paragraph(resourceBundle.getString("TRANSACTION_DESCRIPTION"), font1));

            pdfPTable.addCell(pdfPCell1);
            pdfPTable.addCell(pdfPCell2);
            pdfPTable.addCell(pdfPCell3);
            pdfPTable.addCell(pdfPCell4);

            document.add(pdfPTable);
            int i = 1;
            for (Transaction t : transactions) {
                PdfPTable table = new PdfPTable(4);
                table.setWidthPercentage(100);

                table.addCell(new Phrase(String.valueOf(i++), font));
                table.addCell(new Phrase(new SimpleDateFormat("MM.dd.yyyy HH:mm:ss").format(t.getTimestamp()), font));
                table.addCell(new Phrase(String.valueOf(t.getAmount()) + " $", font));
                table.addCell(new Phrase(resourceBundle.getString(t.getDescription()), font));
                document.add(table);
            }

            Paragraph footer = new Paragraph("© Internet Service Provider 2022", new Font(arial, 10));
            footer.setSpacingBefore(15);
            footer.setAlignment(Element.ALIGN_CENTER);
            document.add(footer);

            document.close();

            String filename = "\"Transactions_" + LocalDate.now() + ".pdf\"";
            openInBrowser(response, baos, filename);
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
