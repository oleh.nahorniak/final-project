package com.nahorniak.finalproject.util.PDF;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.nahorniak.finalproject.DAO.entity.Tariff;
import com.nahorniak.finalproject.DAO.entity.TariffServices;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Set;

/**
 * TariffPDFBuilder - class to save tariffs in pdf
 *
 * @author Oleh Nahorniak.
 */
public class TariffPDFBuilder implements PDFBuilder {

    public void openPDF(HttpServletRequest request, HttpServletResponse response, Set<TariffServices> tariffs) {
        ResourceBundle resourceBundle = ResourceBundle.getBundle("text",
                new Locale((String) request.getSession().getAttribute("lang")));

        Document document = new Document(PageSize.A4, 50, 50, 50, 50);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            PdfWriter.getInstance(document, baos);
            document.open();


            BaseFont arial = BaseFont.createFont("C:\\Windows\\Fonts\\arial.ttf", "cp1251",
                    BaseFont.EMBEDDED);

            Paragraph title = new Paragraph(resourceBundle.getString("Tariffs"), new Font(arial, 16));
            title.setAlignment(Element.ALIGN_CENTER);
            Chapter chapter = new Chapter(title, 1);

            chapter.setNumberDepth(0);

            document.add(chapter);

            PdfPTable pdfPTable = new PdfPTable(5);
            pdfPTable.setSpacingBefore(25);
            Font font1 = new Font(arial, 16);
            Font font = new Font(arial, 12);

            PdfPCell pdfPCell1 = new PdfPCell(new Paragraph("#", font1));
            PdfPCell pdfPCell2 = new PdfPCell(new Paragraph(resourceBundle.getString("TARIFF_TITLE"), font1));
            PdfPCell pdfPCell3 = new PdfPCell(new Paragraph(resourceBundle.getString("Code"), font1));
            PdfPCell pdfPCell4 = new PdfPCell(new Paragraph(resourceBundle.getString("Service_List"), font1));
            PdfPCell pdfPCell5 = new PdfPCell(new Paragraph(resourceBundle.getString("PRICE_PER_DAY"), font1));

            pdfPTable.addCell(pdfPCell1);
            pdfPTable.addCell(pdfPCell2);
            pdfPTable.addCell(pdfPCell3);
            pdfPTable.addCell(pdfPCell4);
            pdfPTable.addCell(pdfPCell5);
            document.add(pdfPTable);

            int i = 1;
            for (TariffServices t : tariffs) {
                PdfPTable table = new PdfPTable(5);
                Tariff tariff = t.getTariff();
                table.addCell(new Phrase(String.valueOf(i++), font));
                table.addCell(new Phrase(tariff.getTitle(), font));
                table.addCell(new Phrase(tariff.getCode(), font));
                table.addCell(new Phrase(t.getServices().toString(), font));
                table.addCell(new Phrase(String.valueOf(tariff.getPrice()) + " $", font));
                document.add(table);
            }

            Paragraph footer = new Paragraph("© Internet Service Provider 2022", new Font(arial, 10));
            footer.setSpacingBefore(15);
            footer.setAlignment(Element.ALIGN_CENTER);
            document.add(footer);

            document.close();

            String filename = "\"Tariffs_" + LocalDate.now() + ".pdf\"";
            openInBrowser(response, baos, filename);
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


}
