package com.nahorniak.finalproject.util.tags;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * DateTime tag that format date and time  to Ukrainian and English format
 */
public class DateTimeTag extends TagSupport {

    private static final String US_FORMAT = "d MMMM yyyy h:mm aa";
    private static final String UA_FORMAT = "d MMMM yyyy HH:mm";

    private String format;
    private String datetime;


    public void setFormat(String format) {
        this.format = format;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    @Override
    public int doStartTag() throws JspException {
        JspWriter out = pageContext.getOut();
        try {
            if (format.equals("en")) {
                datetime = new SimpleDateFormat(US_FORMAT, Locale.US)
                        .format(Timestamp.valueOf(datetime));
            } else if (format.equals("uk")) {
                datetime = new SimpleDateFormat(UA_FORMAT, new Locale("uk"))
                        .format(Timestamp.valueOf(datetime));
            }
            out.print(datetime);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return SKIP_BODY;
    }
}
