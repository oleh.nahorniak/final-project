package com.nahorniak.finalproject.util.tags;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.Locale;

/**
 * NumberTag tag that format number to Ukrainian and English format
 */
public class NumberTag extends TagSupport {

    private String format;
    private double number;

    public void setFormat(String format) {
        this.format = format;
    }

    public void setNumber(double number) {
        this.number = number;
    }

    @Override
    public int doStartTag() throws JspException {
        JspWriter out = pageContext.getOut();
        try {
            if (format.equals("en")) {
                out.print(String.format(Locale.US, "%.1f", number));
            } else if (format.equals("uk")) {
                out.print(String.format(new Locale("uk"), "%.1f", number));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return SKIP_BODY;
    }
}
