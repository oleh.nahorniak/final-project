package com.nahorniak.finalproject.util.tags;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

/**
 * Date tag that format date to Ukrainian and English format
 */
public class DateTag extends TagSupport {

    private static final String DATE_FORMAT = "d MMMM yyyy";

    private String format;
    private String date;


    public void setFormat(String format) {
        this.format = format;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public int doStartTag() throws JspException {
        JspWriter out = pageContext.getOut();

        try {
            if (format.equals("en")) {
                date = LocalDate.parse(date)
                        .format(DateTimeFormatter.ofPattern(DATE_FORMAT, Locale.US));
            } else if (format.equals("uk")) {
                date = LocalDate.parse(date)
                        .format(DateTimeFormatter.ofPattern(DATE_FORMAT, new Locale("uk")));
            }
            out.print(date);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return SKIP_BODY;
    }

}
