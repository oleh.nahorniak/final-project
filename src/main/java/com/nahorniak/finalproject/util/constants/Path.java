package com.nahorniak.finalproject.util.constants;

/**
 * class Path that contains servlet names and paths to jsp pages
 */

public final class Path {
    public static final String TARIFF_EDIT_JSP = "/WEB-INF/jsp/admin/tariffEdit.jsp";
    public static final String NEW_TARIFF_JSP = "WEB-INF/jsp/admin/newTariff.jsp";
    public static final String CUSTOMERS_JSP = "WEB-INF/jsp/admin/customers.jsp";
    public static final String REGISTER_JSP = "WEB-INF/jsp/admin/register.jsp";
    public static final String TARIFFS_JSP = "WEB-INF/jsp/common/tariffs.jsp";
    public static final String PROFILE_JSP = "WEB-INF/jsp/common/profile.jsp";
    public static final String USER_EDIT_JSP = "WEB-INF/jsp/common/userEdit.jsp";
    public static final String REQUESTS_JSP = "WEB-INF/jsp/customer/requests.jsp";
    public static final String TOP_UP_ACCOUNT_JSP = "WEB-INF/jsp/customer/topUpAccount.jsp";
    public static final String TRANSACTIONS_JSP = "WEB-INF/jsp/customer/transactionsHistory.jsp";
    public static final String LOGIN_JSP = "login.jsp";

    public static final String TARIFF_EDIT = "tariffEdit";
    public static final String NEW_TARIFF = "newTariff";
    public static final String CUSTOMERS = "customers";
    public static final String REGISTER = "register";
    public static final String TARIFFS = "tariffs";
    public static final String PROFILE = "profile";
    public static final String USER_EDIT = "userEdit";
    public static final String REQUESTS = "requests";
    public static final String TOP_UP = "topUpAccount";
    public static final String TRANSACTIONS = "transactionsHistory";
    public static final String LOGIN = "login";
}
