package com.nahorniak.finalproject.util.constants;

/**
 * class MailConstants that contains Mail messages and subjects
 */
public final class MailConstants {

    public static final String MAIL_BAN_SUBJECT = "Got Banned";
    public static final String MAIL_BAN_MESSAGE = "Адміністратор заблокував Вас !\nБудь ласка зв'яжіться з адміністратором для " +
            "детального ознайомлення" +
            "\n-----------------------------\n" +
            "Administrator banned you ! \nPlease contact with administrator for details\n\nAdministrator - ispmanagerua@gmail.com";

    public static final String MAIL_UNBAN_SUBJECT = "Got Unbanned";
    public static final String MAIL_UNBAN_MESSAGE = "Адміністратор розблокував Вас ! " +
            "\nAdministrator unbanned you !";

    public static final String MAIL_NOTIFICATION_SUBJECT = "Notification";
    public static final String MAIL_FORGOT_PASSWORD = "Forgot password";

    public static final String MAIL_TARIFF_ARREARS = "Ви заблоковані через заборгованість за тарифом, на даний момент тариф призупинено !\n" +
            "You are blocked due to tariff arrears, currently the tariff is suspended !";
    public static final String MAIL_TARIFF_EXPIRES = "Термін дії вашого тарифу закінчився ! \nYour tariff has expired! " +
            "\n\nДякуємо, що вибрали нас :) --- Thank you for choosing us :)";
}
