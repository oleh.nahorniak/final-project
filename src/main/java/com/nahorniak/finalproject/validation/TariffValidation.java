package com.nahorniak.finalproject.validation;

import java.util.regex.Pattern;

/**
 * TariffValidation - class to validate tariff
 *
 * @author Oleh Nahorniak.
 */
public class TariffValidation {
    private static final String TITLE_EN = "[A-Z]([a-z]{3,15})";
    private static final String TITLE_UK = "[А-ЯІЄЇ]([а-яієї]{3,15})";
    private static final String CODE = "#[A-Z]{1,15}";

    /**
     * method to validate tariff
     *
     * @param titleEn
     * @param titleUk
     * @param code
     * @param price
     * @return result of validation ( true/ false)
     */
    public static boolean isValid(String titleEn, String titleUk, String code, double price) {

        Pattern pattern = Pattern.compile(TITLE_EN);

        if (titleEn == null || !pattern.matcher(titleEn).matches()) return false;

        System.out.println(1);


        pattern = Pattern.compile(TITLE_UK);

        if (titleUk == null || !pattern.matcher(titleUk).matches()) return false;
        System.out.println(2);

        pattern = Pattern.compile(CODE);

        if (code == null || !pattern.matcher(code).matches()) return false;
        System.out.println(3);

        if (price <= 0) return false;
        System.out.println(4);

        return true;

    }
}
