package com.nahorniak.finalproject.validation;

import com.nahorniak.finalproject.DAO.entity.Role;
import com.nahorniak.finalproject.DAO.entity.User;

import java.util.regex.Pattern;

/**
 * UserValidation - class to validate user
 *
 * @author Oleh Nahorniak.
 */
public class UserValidation {

    private static final String LOGIN =
            "^[a-zA-Z0-9]([._-](?![._-])|[a-zA-Z0-9]){2,18}[a-zA-Z0-9]$";
    private static final String PASSWORD =
            "(^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{8,}$)|([a-fA-F0-9]{32})";

    private static final String FIRST_NAME_PATTERN =
            "^[A-ZА-ЯІЇЄ]([a-zа-яіїє]{2,15})";

    private static final String LAST_NAME =
            "^[A-ZА-ЯІЇЄ]([a-zа-яіїє]{2,15})";

    private static final String EMAIL =
            "^[a-z0-9](\\.?[a-z0-9]){5,}@g(oogle)?mail\\.com$";

    private static final String PHONE =
            "[0-9]{3}-[0-9]{3}-[0-9]{4}";

    private static final String COUNTRY =
            "([A-ZА-ЯІЇЄ]([a-zа-яіїє]{1,15}) ?)+";

    private static final String CITY =
            "([A-ZА-ЯІЇЄ]([a-zа-яіїє]{1,15})( |-)?)+";

    private static final String STREET =
            "(\\w+\\s*(road|street|вулиця|вул|square|rd|st|sq), \\w{1,5})";


    /**
     * method to validate user
     *
     * @param user
     * @return result of validation ( true/ false)
     */
    public static boolean isValidUser(User user) {

        String login = user.getLogin();
        String password = user.getPassword();
        String firstName = user.getFirstName();
        String lastName = user.getLastName();
        String email = user.getEmail();
        String phone = user.getPhoneNumber();
        String country = user.getCountry();
        String city = user.getCity();
        String street = user.getStreet();
        Role role = user.getRole();

        Pattern pattern = Pattern.compile(LOGIN);

        if (login == null || !pattern.matcher(login).matches()) return false;

        pattern = Pattern.compile(PASSWORD);

        if (password == null || !pattern.matcher(password).matches()) return false;

        pattern = Pattern.compile(FIRST_NAME_PATTERN);

        if (firstName == null || !pattern.matcher(firstName).matches()) return false;

        pattern = Pattern.compile(LAST_NAME);

        if (lastName == null || !pattern.matcher(lastName).matches()) return false;

        pattern = Pattern.compile(EMAIL);

        if (email == null || !pattern.matcher(email).matches()) return false;

        pattern = Pattern.compile(PHONE);

        if (phone == null || !pattern.matcher(phone).matches()) return false;

        if (role == null) return false;


        pattern = Pattern.compile(COUNTRY);

        if (country != null && !country.isBlank() && !pattern.matcher(country).matches()) return false;

        pattern = Pattern.compile(CITY);

        if (city != null && !city.isBlank() && !pattern.matcher(city).matches()) return false;

        pattern = Pattern.compile(STREET);

        if (street != null && !street.isBlank() && !pattern.matcher(street).matches()) return false;

        return true;
    }
}
