package com.nahorniak.finalproject.DAO;

/**
 * SQLQuery class
 *
 * @author Oleh Nahorniak.
 */
final class SQLQuery {

    /**
     * UserQuery class store all userDAO's query
     */
    static class UserQuery {

        public static final String GET_USER = "select u.id,u.user_role,u.login,u.password,u.first_name,u.last_name," +
                "u.email,u.phone_number," +
                "u.is_blocked,u.balance,a.country,a.city ,a.street from users u " +
                "left outer join address a on a.id = u.address_id " +
                "where u.login =? and password =?";

        public static final String UPDATE_USER = "update users " +
                "set password=?, first_name=?, last_name=?, email=?," +
                " phone_number=?, balance=?, is_blocked=? " +
                "where id=? ;" +
                "update address set country=?, city=?, street=? " +
                "where id in (select address_id from users where id=?)";

        public static final String GET_USER_BY_ID =
                "select u.id,u.user_role,u.login,u.password,u.first_name,u.last_name," +
                        "u.email,u.phone_number," +
                        "u.is_blocked,u.balance,a.country,a.city ,a.street from users u " +
                        "left outer join address a on a.id = u.address_id " +
                        "where u.id =? ";

        public static final String GET_USER_BY_EMAIL =
                "select u.id,u.user_role,u.login,u.password,u.first_name,u.last_name," +
                        "u.email,u.phone_number," +
                        "u.is_blocked,u.balance,a.country,a.city ,a.street from users u " +
                        "left outer join address a on a.id = u.address_id " +
                        "where u.email =? ";

        public static final String INSERT_USER = "insert into address(country,city,street) values(?,?,?); " +
                "insert into users(user_role,login,password,first_name,last_name,email,phone_number,address_id) " +
                "values(CAST(? AS user_role),?,?,?,?,?,?,currval('address_id_seq')) ;";

        public static final String GET_ALL = "select u.id,user_role,login,password,first_name,last_name," +
                "email,phone_number,is_blocked,balance,country,city,street " +
                "from users u " +
                "left outer join address a on a.id = u.address_id " +
                "group by u.id asc";

        public static final String GET_ALL_CUSTOMERS = "select u.id,user_role,login,password,first_name,last_name," +
                "email,phone_number,is_blocked,balance,country,city,street " +
                "from users u " +
                "left outer join address a on a.id = u.address_id " +
                "where user_role ='CUSTOMER'";

        public static final String CHANGE_USER_BLOCK_STATUS = "update users set is_blocked=? where id =?";

        public static final String GET_COUNT = "select count(*) from users where user_role ='CUSTOMER'";
    }

    /**
     * TariffServicesQuery class store all TariffServicesDAO's query
     */
    static class TariffServicesQuery {

        public static final String GET_ALL_TARIFFS = "select t.id,tariff_code,price_per_day,title from tariffs t " +
                "join tariff_description on t.id = tariff_description.tariff_id " +
                "join language l on tariff_description.lang_id = l.id " +
                "where l.name = ? ";

        public static final String GET_TARIFFS_COUNT = "select count(*) from tariffs ";

        public static final String GET_TARIFF = "select t.id,tariff_code,price_per_day,title from tariffs t " +
                "join tariff_description on t.id = tariff_description.tariff_id " +
                "join language l on tariff_description.lang_id = l.id " +
                "where l.name = ? and t.id = ?";

        public static final String GET_ALL_SERVICES = "SELECT s.id, service_code,title " +
                "FROM services s " +
                "join service_description sd on s.id = sd.service_id " +
                "join language l on l.id = sd.lang_id " +
                "where l.name = ?";

        public static final String GET_ALL_SERVICES_BY_TARIFF = "select s.id,service_code,title from services s " +
                "join service_description sd on sd.service_id = s.id " +
                "join tariffs_services ts on ts.service_id = s.id " +
                "join language l on l.id = sd.lang_id " +
                "where ts.tariff_id = ? and l.name = ?";

        public static final String DELETE_TARIFF_SERVICES = "delete from tariffs_services where tariff_id=?";

        public static final String SET_SERVICES_FOR_TARIFF = "insert into tariffs_services values(?,?)";

        public static final String UPDATE_TARIFF_TITLE = "update tariff_description set title=? " +
                "where tariff_id = ? and lang_id = (select id from language where name=?)";

        public static final String INSERT_TARIFF = "insert into tariffs (price_per_day,tariff_code) values (?,?)";

        public static final String INSERT_TARIFF_DESCRIPTION = "insert into tariff_description values (?," +
                "(select id from language where name=?),?)";

        public static final String DELETE_TARIFF = "delete from tariffs where id = ?";

        public static final String UPDATE_TARIFF = "update tariffs set price_per_day=?, tariff_code=? " +
                "where id =?";

        public static final String GET_ADDITIONAL_TARIFF_TITLE = "select title from tariff_description td " +
                "join language l on l.id = td.lang_id  " +
                "where td.tariff_id = ? and l.name != ? ";
    }

    /**
     * RequestQuery class store all RequestDAO's query
     */
    static class RequestQuery {
        public static final String GET_ALL_BY_USER_ID = "SELECT id, user_id, tariff_id, start_date, end_date, status " +
                "FROM request" + " where user_id = ? ";

        public static final String GET_COUNT_BY_USER = "SELECT count(*) from request where user_id = ?";

        public static final String GET_ACTIVE = "SELECT id, user_id, tariff_id, start_date, end_date, status " +
                "FROM request " +
                "where user_id=? and status = 'ACTIVE'";

        public static final String GET_SUSPENDED = "SELECT id, user_id, tariff_id, start_date, end_date, status " +
                "FROM request " +
                "where user_id=? and status = 'SUSPENDED'";

        public static final String CHANGE_REQUEST_STATUS = "UPDATE request set status = CAST(? AS request_status) where id =?";

        public static final String CLOSE_REQUEST = "UPDATE request set status = CAST(? AS request_status) , end_date = NOW() where id =?";

        public static final String CREATE_REQUEST = "INSERT INTO request(user_id, tariff_id, end_date)VALUES (?, ?, ?)";
    }

    /**
     * TransactionQuery class store all TransactionDAO's query
     */
    static class TransactionQuery {
        public static final String GET_ALL_BY_USER_ID = "SELECT id, user_id, amount, date_and_time, description " +
                "FROM transactions " +
                "where user_id = ? ";

        public static final String GET_COUNT = "select count(*) from transactions where user_id = ?";

        public static final String CREATE = "INSERT INTO transactions( " +
                "user_id, amount, date_and_time, description) VALUES (?, ?, ?, ?)";
    }
}
