package com.nahorniak.finalproject.DAO.entity;

import java.io.Serializable;
import java.util.Objects;

/**
 * Service bean
 *
 * @author Oleh Nahorniak.
 */
public class Service implements Serializable {

    private static final long serialVersionUID = 4231123694875273598L;
    private int id;
    private String code;
    private String title;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return title;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Service service = (Service) o;
        return id == service.id && title.equals(service.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title);
    }

    /**
     * Builder for creating service object
     */

    public static class Builder {

        private final Service service;

        public Builder() {
            service = new Service();
        }

        public Service.Builder withId(int id) {
            service.id = id;
            return this;
        }

        public Service.Builder withCode(String code) {
            service.code = code;
            return this;
        }

        public Service.Builder withTitle(String title) {
            service.title = title;
            return this;
        }

        public Service build() {
            return service;
        }
    }
}
