package com.nahorniak.finalproject.DAO.entity;

import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * TariffServices bean
 *
 * @author Oleh Nahorniak.
 */

public class TariffServices {

    private Tariff tariff;
    private Set<Service> services;

    public Tariff getTariff() {
        return tariff;
    }

    public void setTariff(Tariff tariff) {
        this.tariff = tariff;
    }

    public Set<Service> getServices() {
        return services;
    }

    public void setServices(Set<Service> services) {
        this.services = services;
    }

    @Override
    public String toString() {
        return "TariffServices{" +
                "tariff=" + tariff +
                ", services=" + services +
                '}';
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TariffServices that = (TariffServices) o;
        return Objects.equals(tariff, that.tariff) && Objects.equals(services, that.services);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tariff, services);
    }


    /**
     * Builder for creating tariff with services object
     */

    public static class Builder {

        private final TariffServices tariffServices;

        public Builder() {
            tariffServices = new TariffServices();
        }

        public TariffServices.Builder withTariff(Tariff tariff) {
            tariffServices.tariff = tariff;
            return this;
        }

        public TariffServices.Builder withServices(Set<Service> services) {
            tariffServices.services = services;
            return this;
        }

        public TariffServices build() {
            return tariffServices;
        }
    }
}
