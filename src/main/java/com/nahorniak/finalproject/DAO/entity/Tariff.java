package com.nahorniak.finalproject.DAO.entity;

import java.io.Serializable;
import java.util.Objects;

/**
 * Tariff bean
 *
 * @author Oleh Nahorniak.
 */

public class Tariff implements Serializable {
    private static final long serialVersionUID = -3046234675344342329L;
    private int id;
    private String code;
    private String title;
    private double price;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Tariff{" +
                "id=" + id +
                ", code='" + code + '\'' +
                ", title='" + title + '\'' +
                ", price=" + price +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tariff tariff = (Tariff) o;
        return id == tariff.id && Double.compare(tariff.price, price) == 0 && Objects.equals(code, tariff.code) && Objects.equals(title, tariff.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, code, title, price);
    }


    /**
     * Builder for creating tariff object
     */

    public static class Builder {

        private final Tariff tariff;

        public Builder() {
            tariff = new Tariff();
        }

        public Builder withId(int id) {
            tariff.id = id;
            return this;
        }

        public Builder withCode(String code) {
            tariff.code = code;
            return this;
        }

        public Builder withTitle(String title) {
            tariff.title = title;
            return this;
        }

        public Builder withPrice(Double price) {
            tariff.price = price;
            return this;
        }

        public Tariff build() {
            return tariff;
        }
    }
}
