package com.nahorniak.finalproject.DAO.entity;

/**
 * RequestTariff bean
 *
 * @author Oleh Nahorniak.
 */
public class RequestTariff {

    private Request request;
    private TariffServices tariffServices;

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public TariffServices getTariffServices() {
        return tariffServices;
    }

    public void setTariffServices(TariffServices tariffServices) {
        this.tariffServices = tariffServices;
    }


    /**
     * Builder for creating requestTariff object
     */

    public static class Builder {

        private final RequestTariff requestTariff;

        public Builder() {
            requestTariff = new RequestTariff();
        }

        public RequestTariff.Builder withRequest(Request request) {
            requestTariff.request = request;
            return this;
        }

        public RequestTariff.Builder withTariffServices(TariffServices tariffServices) {
            requestTariff.tariffServices = tariffServices;
            return this;
        }

        public RequestTariff build() {
            return requestTariff;
        }
    }
}
