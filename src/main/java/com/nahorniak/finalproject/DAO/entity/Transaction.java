package com.nahorniak.finalproject.DAO.entity;

import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDateTime;

/**
 * Transaction bean
 *
 * @author Oleh Nahorniak.
 */
public class Transaction {

    private int id;
    private int userId;
    private double amount;
    private Timestamp timestamp;
    private String description;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Payment{" +
                "id=" + id +
                ", userId=" + userId +
                ", amount=" + amount +
                ", timestamp=" + timestamp +
                ", description='" + description + '\'' +
                '}';
    }

    /**
     * Builder for creating transaction object
     */

    public static class Builder {

        private final Transaction transaction;

        public Builder() {
            transaction = new Transaction();
        }

        public Transaction.Builder withId(int id) {
            transaction.id = id;
            return this;
        }

        public Transaction.Builder withUserId(int userId) {
            transaction.userId = userId;
            return this;
        }

        public Transaction.Builder withAmount(double amount) {
            transaction.amount = amount;
            return this;
        }

        public Transaction.Builder withTimeStamp(Timestamp timestamp) {
            transaction.timestamp = timestamp;
            return this;
        }

        public Transaction.Builder withDescription(String description) {
            transaction.description = description;
            return this;
        }

        public Transaction build() {
            return transaction;
        }
    }
}
