package com.nahorniak.finalproject.DAO.entity;

/**
 * Request status
 *
 * @author Oleh Nahorniak.
 */
public enum Status {
    ACTIVE("ACTIVE"), SUSPENDED("SUSPENDED"), CLOSED("CLOSED");

    private String value;

    Status(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public boolean equalsTo(String name) {
        return value.equals(name);
    }
}
