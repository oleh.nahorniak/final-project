package com.nahorniak.finalproject.DAO.entity;

import java.io.Serializable;

/**
 * User bean
 *
 * @author Oleh Nahorniak.
 */
public class User implements Serializable {

    private static final long serialVersionUID = -7769428999318642935L;
    private int id;
    private String login;
    private String password;
    private Role role;
    private boolean isBlocked;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String email;
    private String street;
    private String country;
    private String city;
    private double balance;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public boolean getIsBlocked() {
        return isBlocked;
    }

    public void setBlocked(boolean blocked) {
        isBlocked = blocked;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance += balance;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", role=" + role +
                ", isBlocked=" + isBlocked +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", email='" + email + '\'' +
                ", street='" + street + '\'' +
                ", country='" + country + '\'' +
                ", city='" + city + '\'' +
                ", balance=" + balance +
                '}';
    }


    /**
     * Builder for creating user object
     */

    public static class Builder {

        private final User person;

        public Builder() {
            person = new User();
        }

        public Builder withLogin(String login) {
            person.login = login;
            return this;
        }

        public Builder withPassword(String password) {
            person.password = password;
            return this;
        }

        public Builder withId(int id) {
            person.id = id;
            return this;
        }

        public Builder withFirstName(String firstName) {
            person.firstName = firstName;
            return this;
        }


        public Builder withLastName(String lastName) {
            person.lastName = lastName;
            return this;
        }

        public Builder withCountry(String country) {
            person.country = country;
            return this;
        }

        public Builder withStreet(String street) {
            person.street = street;
            return this;
        }

        public Builder withCity(String city) {
            person.city = city;
            return this;
        }

        public Builder withPhoneNumber(String phoneNumber) {
            person.phoneNumber = phoneNumber;
            return this;
        }

        public Builder withEmail(String email) {
            person.email = email;
            return this;
        }

        public Builder withRole(String roleName) {
            Role role = null;
            for (Role value : Role.values()) {
                if (value.equalsTo(roleName)) role = value;
            }
            person.role = role;
            return this;
        }

        public Builder withStatus(boolean isBlocked) {
            person.isBlocked = isBlocked;
            return this;
        }

        public Builder withBalance(double balance) {
            person.balance = balance;
            return this;
        }

        public User build() {
            return person;
        }

    }


}
