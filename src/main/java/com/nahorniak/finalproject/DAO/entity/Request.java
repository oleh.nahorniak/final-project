package com.nahorniak.finalproject.DAO.entity;

import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDate;
import java.util.Objects;

/**
 * Request bean
 *
 * @author Oleh Nahorniak.
 */
public class Request implements Serializable {

    private static final long serialVersionUID = -7615397990004896671L;
    private int id;
    private int userId;
    private int tariffId;
    private LocalDate startDate;
    private LocalDate endDate;
    private Status status;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getTariffId() {
        return tariffId;
    }

    public void setTariffId(int tariffId) {
        this.tariffId = tariffId;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Request{" +
                "id=" + id +
                ", userId=" + userId +
                ", tariffId=" + tariffId +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", status=" + status +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Request request = (Request) o;
        return id == request.id && userId == request.userId && tariffId == request.tariffId && Objects.equals(startDate, request.startDate) && Objects.equals(endDate, request.endDate) && status == request.status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, userId, tariffId, startDate, endDate, status);
    }


    /**
     * Builder for creating request object
     */

    public static class Builder {

        private final Request request;

        public Builder() {
            request = new Request();
        }

        public Request.Builder withId(int id) {
            request.id = id;
            return this;
        }

        public Request.Builder withUserId(int userId) {
            request.userId = userId;
            return this;
        }

        public Request.Builder withTariffId(int tariffId) {
            request.tariffId = tariffId;
            return this;
        }

        public Request.Builder withStartDate(LocalDate startDate) {
            request.startDate = startDate;
            return this;
        }

        public Request.Builder withEndDate(LocalDate endDate) {
            request.endDate = endDate;
            return this;
        }

        public Request.Builder withStatus(String statusName) {
            Status status = null;
            for (Status value : Status.values()) {
                if (value.equalsTo(statusName)) status = value;
            }
            request.status = status;
            return this;
        }

        public Request build() {
            return request;
        }
    }
}
