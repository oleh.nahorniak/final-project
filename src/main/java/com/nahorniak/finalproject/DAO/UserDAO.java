package com.nahorniak.finalproject.DAO;

import com.nahorniak.finalproject.DAO.entity.User;
import com.nahorniak.finalproject.DAO.mapper.EntityMapper;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


import static com.nahorniak.finalproject.DAO.SQLQuery.RequestQuery.GET_ALL_BY_USER_ID;
import static com.nahorniak.finalproject.DAO.constants.Fields.*;
import static com.nahorniak.finalproject.DAO.SQLQuery.UserQuery.*;

/**
 * UserDAO class
 *
 * @author Oleh Nahorniak.
 */

public class UserDAO {
    private static UserDAO userDAO;

    /**
     * method to get instance of UserDAO
     *
     * @return UserDAO instance
     */
    public static synchronized UserDAO getInstance() {
        if (userDAO == null) {
            userDAO = new UserDAO();
        }
        return userDAO;
    }

    private UserDAO() {
    }


    /**
     * method to get user by his login and password
     *
     * @param login
     * @param password
     * @param connection
     * @return User object
     * @throws SQLException
     */
    public User getUser(String login, String password, Connection connection) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement(GET_USER)) {
            ps.setString(1, login);
            ps.setString(2, password);
            ResultSet rs = ps.executeQuery();
            User user = null;
            if (rs.next()) {
                UserMapper userMapper = new UserMapper();
                user = userMapper.map(rs);
            }
            DBConnection.close(rs);
            return user;
        } catch (SQLException e) {
            DBConnection.rollback(connection);
            e.printStackTrace();
            throw e;
        } finally {
            DBConnection.commit(connection);
        }
    }

    /**
     * method to get user by his id
     *
     * @param id
     * @param connection
     * @return User object
     * @throws SQLException
     */
    public User getUserById(int id, Connection connection) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement(GET_USER_BY_ID)) {
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                UserMapper userMapper = new UserMapper();
                return userMapper.map(rs);
            }
        } catch (SQLException e) {
            DBConnection.rollback(connection);
            throw e;
        } finally {
            DBConnection.commit(connection);
        }
        return null;
    }

    /**
     * method to get User by his email
     *
     * @param email
     * @param connection
     * @return User object
     * @throws SQLException
     */
    public User getUserByEmail(String email, Connection connection) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement(GET_USER_BY_EMAIL)) {
            ps.setString(1, email);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                UserMapper userMapper = new UserMapper();
                return userMapper.map(rs);
            }
        } catch (SQLException e) {
            DBConnection.rollback(connection);
            throw e;
        } finally {
            DBConnection.commit(connection);
        }
        return null;
    }

    /**
     * method to update user
     *
     * @param user
     * @param connection
     * @throws SQLException
     */
    public void updateUser(User user, Connection connection) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement(UPDATE_USER)) {
            int k = 1;
            ps.setString(k++, user.getPassword());
            ps.setString(k++, user.getFirstName());
            ps.setString(k++, user.getLastName());
            ps.setString(k++, user.getEmail());
            ps.setString(k++, user.getPhoneNumber());
            ps.setDouble(k++, user.getBalance());
            ps.setBoolean(k++, user.getIsBlocked());
            ps.setInt(k++, user.getId());
            ps.setString(k++, user.getCountry());
            ps.setString(k++, user.getCity());
            ps.setString(k++, user.getStreet());
            ps.setInt(k, user.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            DBConnection.rollback(connection);
            throw e;
        } finally {
            DBConnection.commit(connection);
        }
    }

    /**
     * method to insert new user into database
     *
     * @param user
     * @param connection
     * @throws SQLException
     */
    public void insertUser(User user, Connection connection) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement(INSERT_USER)) {
            int k = 1;
            ps.setString(k++, user.getCountry());
            ps.setString(k++, user.getCity());
            ps.setString(k++, user.getStreet());
            ps.setString(k++, user.getRole().getValue());
            ps.setString(k++, user.getLogin());
            ps.setString(k++, user.getPassword());
            ps.setString(k++, user.getFirstName());
            ps.setString(k++, user.getLastName());
            ps.setString(k++, user.getEmail());
            ps.setString(k, user.getPhoneNumber());
            ps.executeUpdate();
        } catch (SQLException e) {
            DBConnection.rollback(connection);
            throw e;
        } finally {
            DBConnection.commit(connection);
        }
    }

    /**
     * method to block or unblock user
     *
     * @param blockFlag
     * @param id
     * @param connection
     */
    public void changeBlockStatus(boolean blockFlag, int id, Connection connection) {
        try (PreparedStatement ps = connection.prepareStatement(CHANGE_USER_BLOCK_STATUS)) {
            ps.setBoolean(1, blockFlag);
            ps.setInt(2, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            DBConnection.rollback(connection);
            e.printStackTrace();
        } finally {
            DBConnection.commit(connection);
        }
    }

    /**
     * method to get Users (pagination)
     *
     * @param orderBy
     * @param count
     * @param start
     * @param connection
     * @return List of users
     * @throws SQLException
     */
    public List<User> getAll(String orderBy, int count, int start, Connection connection) throws SQLException {
        List<User> users = new ArrayList<>();
        StringBuilder query = new StringBuilder(GET_ALL_CUSTOMERS);
        if (orderBy != null) {
            query.append(orderBy);
        }

        try (PreparedStatement ps = connection.prepareStatement(query.toString())) {

            if (orderBy != null) {
                ps.setInt(1, count);
                ps.setInt(2, start);
            }

            ResultSet rs = ps.executeQuery();
            UserMapper userMapper = new UserMapper();
            while (rs.next()) {
                User person = userMapper.map(rs);
                users.add(person);
            }
            DBConnection.close(rs);
        } catch (SQLException e) {
            throw e;
        }
        return users;
    }

    /**
     * method to get all Users
     *
     * @param orderBy
     * @param connection
     * @return List of Users
     * @throws SQLException
     */
    public List<User> getAll(String orderBy, Connection connection) throws SQLException {
        List<User> users = new ArrayList<>();
        StringBuilder query = new StringBuilder(GET_ALL_CUSTOMERS);
        if (orderBy != null) {
            query.append(orderBy);
        }
        try (Statement statement = connection.createStatement()) {
            ResultSet rs = statement.executeQuery(query.toString());
            UserMapper userMapper = new UserMapper();
            while (rs.next()) {
                User person = userMapper.map(rs);
                users.add(person);
            }
            DBConnection.close(rs);
        } catch (SQLException e) {
            throw e;
        }
        return users;
    }

    /**
     * method to get count of users
     *
     * @param connection
     * @return count of users
     * @throws SQLException
     */
    public int getCount(Connection connection) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement(GET_COUNT)) {
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            }
            DBConnection.close(rs);
        } catch (SQLException e) {
            DBConnection.rollback(connection);
            throw e;
        } finally {
            DBConnection.commit(connection);
        }
        return 0;
    }

    /**
     * UserMapper class to map all user information in one entity
     */
    private static class UserMapper implements EntityMapper<User> {

        @Override
        public User map(ResultSet rs) {
            User.Builder builder = new User.Builder();
            User person = null;
            try {
                person = builder.withId(rs.getInt(ENTITY__ID))
                        .withFirstName(rs.getString(USER__FIRST_NAME))
                        .withLastName(rs.getString(USER__LAST_NAME))
                        .withLogin(rs.getString(USER__LOGIN))
                        .withPassword(rs.getString(USER__PASSWORD))
                        .withRole(rs.getString(USER__ROLE))
                        .withStreet(rs.getString(ADDRESS__STREET))
                        .withCountry(rs.getString(ADDRESS__COUNTRY))
                        .withCity(rs.getString(ADDRESS__CITY))
                        .withEmail(rs.getString(USER__EMAIL))
                        .withPhoneNumber(rs.getString(USER__PHONE_NUMBER))
                        .withStatus(rs.getBoolean(USER__IS_BLOCKED))
                        .withBalance(rs.getDouble(USER__BALANCE))
                        .build();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return person;
        }
    }
}
