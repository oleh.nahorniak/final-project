package com.nahorniak.finalproject.DAO.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Entity mapper interface
 *
 * @author Oleh Nahorniak.
 */
public interface EntityMapper<T> {
    T map(ResultSet rs);
}
