package com.nahorniak.finalproject.DAO;

import com.nahorniak.finalproject.DAO.entity.Request;
import com.nahorniak.finalproject.DAO.entity.Status;
import com.nahorniak.finalproject.DAO.mapper.EntityMapper;
import jdk.jshell.spi.SPIResolutionException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.nahorniak.finalproject.DAO.SQLQuery.TariffServicesQuery.GET_ALL_TARIFFS;
import static com.nahorniak.finalproject.DAO.constants.Fields.*;
import static com.nahorniak.finalproject.DAO.SQLQuery.RequestQuery.*;

/**
 * RequestDao class
 *
 * @author Oleh Nahorniak.
 */
public class RequestDAO {

    private static final String ACTIVE = "ACTIVE";
    private static final String SUSPENDED = "SUSPENDED";

    private static RequestDAO requestDAO;

    /**
     * method to get instance of RequestDAO
     *
     * @return requestDao instance
     */
    public static synchronized RequestDAO getInstance() {
        if (requestDAO == null) {
            requestDAO = new RequestDAO();
        }
        return requestDAO;
    }

    private RequestDAO() {
    }

    /**
     * method to get requests by user id (pagination)
     *
     * @return List of Requests
     */
    public List<Request> getAllByUserId(int userId, String orderBy, int count, int start, Connection connection) {
        List<Request> requests = new ArrayList<>();
        StringBuilder query = new StringBuilder(GET_ALL_BY_USER_ID);
        if (orderBy != null) {
            query.append(orderBy);
        }
        RequestMapper mapper = new RequestMapper();
        try (PreparedStatement ps = connection.prepareStatement(query.toString())) {
            ps.setInt(1, userId);

            if (orderBy != null) {
                ps.setInt(2, count);
                ps.setInt(3, start);
            }

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Request request = mapper.map(rs);
                requests.add(request);
            }
            DBConnection.close(rs);
        } catch (SQLException e) {
            DBConnection.rollback(connection);
            e.printStackTrace();
        } finally {
            DBConnection.commit(connection);
        }
        return requests;
    }

    /**
     * method to get all requests by user id
     *
     * @return List of Requests
     */
    public List<Request> getAllByUserId(int userId, String orderBy, Connection connection) {
        List<Request> requests = new ArrayList<>();
        StringBuilder query = new StringBuilder(GET_ALL_BY_USER_ID);
        if (orderBy != null) {
            query.append(orderBy);
        }
        RequestMapper mapper = new RequestMapper();
        try (PreparedStatement ps = connection.prepareStatement(query.toString())) {
            ps.setInt(1, userId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Request request = mapper.map(rs);
                requests.add(request);
            }
            DBConnection.close(rs);
        } catch (SQLException e) {
            DBConnection.rollback(connection);
            e.printStackTrace();
        } finally {
            DBConnection.commit(connection);
        }
        return requests;
    }

    /**
     * method to find if user has active request
     *
     * @return true if user has active request already / false if he doesn't
     */
    public boolean isActiveRequest(int userId, Connection connection) throws SQLException {
        try {
            return getActiveOrSuspendedRequest(userId, ACTIVE, connection) != null;
        } catch (SQLException e) {
            throw e;
        }
    }

    /**
     * method to find if user has suspended request
     *
     * @return true if user has suspended request already / false if he doesn't
     */
    public boolean isSuspendedRequest(int userId, Connection connection) throws SQLException {
        try {
            return getActiveOrSuspendedRequest(userId, SUSPENDED, connection) != null;
        } catch (SQLException e) {
            throw e;
        }
    }

    /**
     * method to get active or suspended request by user id
     *
     * @return Active or Suspended Request
     */
    public Request getActiveOrSuspendedRequest(int userId, String query, Connection connection) throws SQLException {
        if (query.equals(ACTIVE)) {
            query = GET_ACTIVE;
        } else if (query.equals(SUSPENDED)) {
            query = GET_SUSPENDED;
        }
        Request request = null;
        RequestMapper mapper = new RequestMapper();
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, userId);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                request = mapper.map(rs);
            }
            DBConnection.close(rs);
        } catch (SQLException e) {
            DBConnection.rollback(connection);
            throw e;
        } finally {
            DBConnection.commit(connection);
        }
        return request;
    }

    /**
     * method to change request status
     */
    public void changeStatus(int requestId, String status, Connection connection) throws SQLException {
        StringBuilder query = new StringBuilder("");

        if (Status.CLOSED.equalsTo(status)) query.append(CLOSE_REQUEST);
        else if (Status.SUSPENDED.equalsTo(status) || Status.ACTIVE.equalsTo(status))
            query.append(CHANGE_REQUEST_STATUS);
        try (PreparedStatement ps = connection.prepareStatement(query.toString())) {
            ps.setString(1, status);
            ps.setInt(2, requestId);
            ps.executeUpdate();
        } catch (SQLException e) {
            DBConnection.rollback(connection);
            throw e;
        } finally {
            DBConnection.commit(connection);
        }
    }

    /**
     * method to create new user request
     */
    public void insertRequest(Request request, Connection connection) {
        try (PreparedStatement ps = connection.prepareStatement(CREATE_REQUEST)) {
            ps.setInt(1, request.getUserId());
            ps.setInt(2, request.getTariffId());
            ps.setDate(3, Date.valueOf(request.getEndDate()));
            ps.executeUpdate();
        } catch (SQLException e) {
            DBConnection.rollback(connection);
            e.printStackTrace();
        } finally {
            DBConnection.commit(connection);
        }
    }

    /**
     * method to get count of user request to make pagination
     */
    public int getCount(int userId, Connection connection) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement(GET_COUNT_BY_USER)) {
            ps.setInt(1, userId);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            }
            DBConnection.close(rs);
        } catch (SQLException e) {
            DBConnection.rollback(connection);
            throw e;
        } finally {
            DBConnection.commit(connection);
        }
        return 0;
    }


    /**
     * RequestMapper class to map all request information in one entity
     */
    private static class RequestMapper implements EntityMapper<Request> {

        @Override
        public Request map(ResultSet rs) {
            Request.Builder builder = new Request.Builder();
            Request request = null;
            try {
                request =
                        builder.withId(rs.getInt(ENTITY__ID))
                                .withUserId(rs.getInt(USER_ID))
                                .withTariffId(rs.getInt(TARIFF__ID))
                                .withStartDate(rs.getDate(REQUEST__START_DATE).toLocalDate())
                                .withEndDate(rs.getDate(REQUEST__END_DATE).toLocalDate())
                                .withStatus(rs.getString(REQUEST__STATUS))
                                .build();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return request;
        }
    }
}
