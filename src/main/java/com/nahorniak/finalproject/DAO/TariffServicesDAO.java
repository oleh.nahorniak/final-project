package com.nahorniak.finalproject.DAO;

import com.nahorniak.finalproject.DAO.entity.Service;
import com.nahorniak.finalproject.DAO.entity.Tariff;
import com.nahorniak.finalproject.DAO.entity.TariffServices;
import com.nahorniak.finalproject.DAO.mapper.EntityMapper;

import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import static com.nahorniak.finalproject.DAO.constants.Fields.*;
import static com.nahorniak.finalproject.DAO.constants.Fields.TARIFF__PRICE;
import static com.nahorniak.finalproject.DAO.SQLQuery.TariffServicesQuery.*;

/**
 * TariffServicesDao class
 *
 * @author Oleh Nahorniak.
 */
public class TariffServicesDAO {

    private static TariffServicesDAO tariffServicesDAO;

    /**
     * method to get instance of TariffServicesDAO
     *
     * @return TariffServicesDAO instance
     */
    public static synchronized TariffServicesDAO getInstance() {
        if (tariffServicesDAO == null) {
            tariffServicesDAO = new TariffServicesDAO();
        }
        return tariffServicesDAO;
    }

    private TariffServicesDAO() {

    }

    /**
     * get Tariff with services by his id
     *
     * @param lang
     * @param id
     * @param connection
     * @return TariffServices object
     * @throws SQLException
     */
    public TariffServices getTariffWithServices(String lang, int id, Connection connection) throws SQLException {
        TariffServices tariffServices = null;
        TariffServices.Builder builder = new TariffServices.Builder();
        TariffMapper tariffMapper = new TariffMapper();


        try (PreparedStatement ps = connection.prepareStatement(GET_TARIFF)) {
            ps.setString(1, lang);
            ps.setInt(2, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Tariff tariff = tariffMapper.map(rs);
                Set<Service> services = getAllServicesByTariffId(tariff.getId(), lang, connection);

                if (services.isEmpty()) throw new SQLException();

                tariffServices = builder.withTariff(tariff)
                        .withServices(services)
                        .build();
            }
            DBConnection.close(rs);
        } catch (SQLException e) {
            DBConnection.rollback(connection);
            throw e;
        } finally {
            DBConnection.commit(connection);
        }
        return tariffServices;
    }

    /**
     * method to get tariff with services to make pagination
     *
     * @param lang
     * @param orderBy
     * @param count
     * @param start
     * @param connection
     * @return Set of Tariffs and Services
     */
    public Set<TariffServices> getAll(String lang, String orderBy, int count, int start, Connection connection) {
        Set<TariffServices> tariffServices = new LinkedHashSet<>();
        StringBuilder query = new StringBuilder(GET_ALL_TARIFFS);
        if (orderBy != null) {
            query.append(orderBy);
        }
        try (PreparedStatement ps = connection.prepareStatement(query.toString())) {
            ps.setString(1, lang);

            if (orderBy != null) {
                ps.setInt(2, count);
                ps.setInt(3, start);
            }

            ResultSet rs = ps.executeQuery();
            tariffServices = tariffMapper(rs, lang, connection);
            DBConnection.close(rs);
        } catch (SQLException e) {
            DBConnection.rollback(connection);
            e.printStackTrace();
        } finally {
            DBConnection.commit(connection);
        }
        return tariffServices;
    }

    /**
     * method to get all Tariffs with services
     *
     * @param lang
     * @param orderBy
     * @param connection
     * @return Set of Tariffs and Services
     */
    public Set<TariffServices> getAll(String lang, String orderBy, Connection connection) {
        Set<TariffServices> tariffServices = new LinkedHashSet<>();
        StringBuilder query = new StringBuilder(GET_ALL_TARIFFS);
        if (orderBy != null) {
            query.append(orderBy);
        }
        try (PreparedStatement ps = connection.prepareStatement(query.toString())) {
            ps.setString(1, lang);
            ResultSet rs = ps.executeQuery();
            tariffServices = tariffMapper(rs, lang, connection);
            DBConnection.close(rs);
        } catch (SQLException e) {
            DBConnection.rollback(connection);
            e.printStackTrace();
        } finally {
            DBConnection.commit(connection);
        }
        return tariffServices;
    }

    /**
     * tariffMapper method to get TariffServices object from Tariff and Set of Services
     *
     * @param rs
     * @param lang
     * @param connection
     * @return Set of Tariff with services
     * @throws SQLException
     */
    private Set<TariffServices> tariffMapper(ResultSet rs, String lang, Connection connection) throws SQLException {
        Set<TariffServices> tariffServices = new LinkedHashSet<>();
        TariffMapper tariffMapper = new TariffMapper();
        while (rs.next()) {
            Tariff tariff = tariffMapper.map(rs);

            Set<Service> services = getAllServicesByTariffId(tariff.getId(), lang, connection);

            if (services.isEmpty()) throw new SQLException();

            TariffServices.Builder builder = new TariffServices.Builder();

            TariffServices ts = builder.withTariff(tariff)
                    .withServices(services)
                    .build();

            tariffServices.add(ts);
        }
        return tariffServices;
    }


    /**
     * method to get count of tariffs that company can provide to their customers
     *
     * @param filter
     * @param connection
     * @return count of tariffs that company can provide to their customers
     * @throws SQLException
     */
    public int getTariffsCount(String filter, Connection connection) throws SQLException {
        int count = 0;
        StringBuilder query = new StringBuilder(GET_TARIFFS_COUNT);

        if (filter != null || filter.isBlank()) {
            query.append(filter);
        }

        try (Statement stmt = connection.createStatement()) {
            ResultSet rs = stmt.executeQuery(query.toString());
            if (rs.next()) {
                count = rs.getInt(1);
            }
            DBConnection.close(rs);
        } catch (SQLException e) {
            DBConnection.rollback(connection);
            throw e;
        } finally {
            DBConnection.commit(connection);
        }
        return count;
    }

    /**
     * method to get all services company has
     *
     * @param lang
     * @param connection
     * @return Set of services that company has at that moment
     */
    public Set<Service> getAllServices(String lang, Connection connection) {
        Set<Service> services = new LinkedHashSet<>();
        ServiceMapper serviceMapper = new ServiceMapper();
        try (PreparedStatement ps = connection.prepareStatement(GET_ALL_SERVICES)) {
            ps.setString(1, lang);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Service service = serviceMapper.map(rs);
                services.add(service);
            }
            DBConnection.close(rs);
        } catch (SQLException e) {
            DBConnection.rollback(connection);
            e.printStackTrace();
        } finally {
            DBConnection.commit(connection);
        }
        return services;
    }

    /**
     * method to get all service by tariff id
     *
     * @param tariffId
     * @param lang
     * @param connection
     * @return Set of services that are combined in tariff
     */
    public Set<Service> getAllServicesByTariffId(int tariffId, String lang, Connection connection) {
        Set<Service> services = new LinkedHashSet<>();
        ServiceMapper serviceMapper = new ServiceMapper();
        try (PreparedStatement ps = connection.prepareStatement(GET_ALL_SERVICES_BY_TARIFF)) {
            ps.setInt(1, tariffId);
            ps.setString(2, lang);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                services.add(serviceMapper.map(rs));
            }
            DBConnection.close(rs);
            return services;
        } catch (SQLException e) {
            DBConnection.rollback(connection);
            e.printStackTrace();
        } finally {
            DBConnection.commit(connection);
        }
        return services;
    }


    /**
     * method to update tariff
     *
     * @param tariff_id
     * @param code
     * @param price
     * @param titleEn
     * @param titleUk
     * @param services_id
     * @param connection
     * @throws SQLException
     */
    public void updateTariff(int tariff_id, String code, Double price, String titleEn,
                             String titleUk, List<Integer> services_id, Connection connection) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement(UPDATE_TARIFF)) {
            ps.setDouble(1, price);
            ps.setString(2, code);
            ps.setInt(3, tariff_id);
            ps.executeUpdate();

            updateTariffTitle(tariff_id, titleEn, "en", connection);
            updateTariffTitle(tariff_id, titleUk, "uk", connection);

            setServicesForTariff(tariff_id, services_id, connection);
        } catch (SQLException e) {
            DBConnection.rollback(connection);
            throw e;
        } finally {
            DBConnection.commit(connection);
        }
    }


    /**
     * method to insert new tariff into database
     *
     * @param price
     * @param code
     * @param titleEn
     * @param titleUk
     * @param services
     * @param connection
     * @throws SQLException
     */
    public void insertTariff(Double price, String code, String titleEn, String titleUk,
                             List<Integer> services, Connection connection) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement(INSERT_TARIFF, Statement.RETURN_GENERATED_KEYS)) {
            ps.setDouble(1, price);
            ps.setString(2, code);
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                int id = rs.getInt(1);
                System.out.println(id);
                insertTariffTitle(id, titleEn, "en", connection);
                insertTariffTitle(id, titleUk, "uk", connection);
                setServicesForTariff(id, services, connection);
            } else throw new SQLException();
        } catch (SQLException e) {
            DBConnection.rollback(connection);
            throw e;
        } finally {
            DBConnection.commit(connection);
        }

    }

    /**
     * method to delete tariff from database
     *
     * @param tariff_id
     * @param connection
     * @throws SQLException
     */
    public void deleteTariff(int tariff_id, Connection connection) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement(DELETE_TARIFF)) {
            ps.setInt(1, tariff_id);
            ps.executeUpdate();
        } catch (SQLException e) {
            DBConnection.rollback(connection);
            throw e;
        } finally {
            DBConnection.commit(connection);
        }
    }

    /**
     * method to insert tariff title
     *
     * @param tariff_id
     * @param title
     * @param lang
     * @param connection
     * @throws SQLException
     */
    private void insertTariffTitle(int tariff_id, String title, String lang, Connection connection) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement(INSERT_TARIFF_DESCRIPTION)) {
            ps.setInt(1, tariff_id);
            ps.setString(2, lang);
            ps.setString(3, title);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw e;
        }
    }

    /**
     * method to update tariff title
     *
     * @param tariff_id
     * @param title
     * @param lang
     * @param connection
     * @throws SQLException
     */
    public void updateTariffTitle(int tariff_id, String title, String lang, Connection connection) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement(UPDATE_TARIFF_TITLE)) {
            ps.setString(1, title);
            ps.setInt(2, tariff_id);
            ps.setString(3, lang);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw e;
        }
    }


    /**
     * setServicesForTariff method that sets services for tariff
     *
     * @param tariff_id
     * @param services_id
     * @param connection
     * @throws SQLException
     */
    public void setServicesForTariff(int tariff_id, List<Integer> services_id, Connection connection) throws SQLException {
        try (PreparedStatement deletePS = connection.prepareStatement(DELETE_TARIFF_SERVICES);
             PreparedStatement insertPS = connection.prepareStatement(SET_SERVICES_FOR_TARIFF)) {
            deletePS.setInt(1, tariff_id);
            deletePS.executeUpdate();

            for (int service_id : services_id) {
                insertPS.setInt(1, tariff_id);
                insertPS.setInt(2, service_id);
                insertPS.executeUpdate();
            }
        } catch (SQLException e) {
            throw e;
        }
    }

    /**
     * getAdditionalTariffTitle method gets additional tariff title
     *
     * @param tariff_id
     * @param lang
     * @param connection
     * @return Additional title of tariff
     * @throws SQLException
     */
    public String getAdditionalTariffTitle(int tariff_id, String lang, Connection connection) throws SQLException {
        String title = null;
        try (PreparedStatement ps = connection.prepareStatement(GET_ADDITIONAL_TARIFF_TITLE)) {
            ps.setInt(1, tariff_id);
            ps.setString(2, lang);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                title = rs.getString(1);
            }
            DBConnection.close(rs);
        } catch (SQLException e) {
            DBConnection.rollback(connection);
            throw e;
        } finally {
            DBConnection.commit(connection);
        }
        return title;
    }

    /**
     * ServiceMapper class to map all service information in one entity
     */

    private static class ServiceMapper implements EntityMapper<Service> {

        @Override
        public Service map(ResultSet rs) {
            Service.Builder builder = new Service.Builder();
            Service service = null;

            try {
                service = builder.withId(rs.getInt(ENTITY__ID))
                        .withCode(rs.getString(SERVICE__CODE))
                        .withTitle(rs.getString(ENTITY__TITLE))
                        .build();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return service;
        }
    }

    /**
     * TariffMapper class to map all tariff information in one entity
     */

    private static class TariffMapper implements EntityMapper<Tariff> {

        @Override
        public Tariff map(ResultSet rs) {
            Tariff.Builder builder = new Tariff.Builder();
            Tariff tariff = null;
            try {
                tariff = builder.withId(rs.getInt(ENTITY__ID))
                        .withCode(rs.getString(TARIFF__CODE))
                        .withTitle(rs.getString(ENTITY__TITLE))
                        .withPrice(rs.getDouble(TARIFF__PRICE))
                        .build();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return tariff;
        }
    }
}
