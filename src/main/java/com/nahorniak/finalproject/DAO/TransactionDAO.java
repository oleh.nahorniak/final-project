package com.nahorniak.finalproject.DAO;

import com.nahorniak.finalproject.DAO.entity.Transaction;
import com.nahorniak.finalproject.DAO.mapper.EntityMapper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.nahorniak.finalproject.DAO.constants.Fields.*;
import static com.nahorniak.finalproject.DAO.SQLQuery.TransactionQuery.*;

/**
 * TransactionDAO class
 *
 * @author Oleh Nahorniak.
 */

public class TransactionDAO {

    private static TransactionDAO transactionDAO;

    /**
     * method to get instance of TransactionDAO
     *
     * @return TransactionDAO instance
     */
    public static synchronized TransactionDAO getInstance() {
        if (transactionDAO == null) {
            transactionDAO = new TransactionDAO();
        }
        return transactionDAO;
    }

    private TransactionDAO() {

    }

    /**
     * method to get transactions (pagination)
     *
     * @param user_id
     * @param orderBy
     * @param count
     * @param start
     * @param connection
     * @return List of Transactions
     * @throws SQLException
     */
    public List<Transaction> getAllByUserId(int user_id, String orderBy, int count, int start, Connection connection) throws SQLException {
        List<Transaction> transactions = new ArrayList<>();
        StringBuilder query = new StringBuilder(GET_ALL_BY_USER_ID);
        if (orderBy != null) {
            query.append(orderBy);
        }

        TransactionMapper mapper = new TransactionMapper();
        try (PreparedStatement ps = connection.prepareStatement(query.toString())) {
            ps.setInt(1, user_id);

            if (orderBy != null) {
                ps.setInt(2, count);
                ps.setInt(3, start);
            }

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Transaction transaction = mapper.map(rs);
                transactions.add(transaction);
            }
            DBConnection.close(rs);
        } catch (SQLException e) {
            DBConnection.rollback(connection);
            throw e;
        } finally {
            DBConnection.commit(connection);
        }
        return transactions;
    }

    /**
     * method to get all user's transactions
     *
     * @param user_id
     * @param orderBy
     * @param connection
     * @return List of user's transactions
     * @throws SQLException
     */
    public List<Transaction> getAllByUserId(int user_id, String orderBy, Connection connection) throws SQLException {
        List<Transaction> transactions = new ArrayList<>();
        StringBuilder query = new StringBuilder(GET_ALL_BY_USER_ID);
        if (orderBy != null) {
            query.append(orderBy);
        }
        TransactionMapper mapper = new TransactionMapper();
        try (PreparedStatement ps = connection.prepareStatement(query.toString())) {
            ps.setInt(1, user_id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Transaction transaction = mapper.map(rs);
                transactions.add(transaction);
            }
            DBConnection.close(rs);
        } catch (SQLException e) {
            DBConnection.rollback(connection);
            throw e;
        } finally {
            DBConnection.commit(connection);
        }
        return transactions;
    }

    /**
     * method to insert new transaction into database
     *
     * @param transaction
     * @param connection
     * @throws SQLException
     */
    public void create(Transaction transaction, Connection connection) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement(CREATE)) {
            int k = 1;
            ps.setInt(k++, transaction.getUserId());
            ps.setDouble(k++, transaction.getAmount());
            ps.setTimestamp(k++, transaction.getTimestamp());
            ps.setString(k++, transaction.getDescription());
            ps.executeUpdate();
        } catch (SQLException e) {
            DBConnection.rollback(connection);
            throw e;
        } finally {
            DBConnection.commit(connection);
        }
    }

    /**
     * method to get count of user's transactions
     *
     * @param userId
     * @param connection
     * @return
     * @throws SQLException
     */
    public int getCount(int userId, Connection connection) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement(GET_COUNT)) {
            ps.setInt(1, userId);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            }
            DBConnection.close(rs);
        } catch (SQLException e) {
            DBConnection.rollback(connection);
            throw e;
        } finally {
            DBConnection.commit(connection);
        }
        return 0;
    }

    /**
     * TransactionMapper class to map all transaction information in one entity
     */
    private static class TransactionMapper implements EntityMapper<Transaction> {
        @Override
        public Transaction map(ResultSet rs) {
            Transaction.Builder builder = new Transaction.Builder();
            Transaction transaction = null;
            try {
                transaction =
                        builder.withId(rs.getInt(ENTITY__ID))
                                .withUserId(rs.getInt(USER_ID))
                                .withAmount(rs.getDouble(TRANSACTION__AMOUNT))
                                .withTimeStamp(rs.getTimestamp(TRANSACTION__TIMESTAMP))
                                .withDescription(rs.getString(TRANSACTION__DESCRIPTION))
                                .build();
            } catch (SQLException e) {
                throw new IllegalStateException();
            }
            return transaction;
        }
    }
}
