package com.nahorniak.finalproject.DAO.constants;

/**
 * all database fields
 *
 * @author Oleh Nahorniak.
 */
public final class Fields {

    public static final String ENTITY__ID = "id";
    public static final String ENTITY__TITLE = "title";
    public static final String ENTITY__LANG_ID = "lang_id";


    public static final String USER__ROLE = "user_role";
    public static final String USER__LOGIN = "login";
    public static final String USER__PASSWORD = "password";
    public static final String USER__FIRST_NAME = "first_name";
    public static final String USER__LAST_NAME = "last_name";
    public static final String USER__EMAIL = "email";
    public static final String USER__PHONE_NUMBER = "phone_number";
    public static final String USER__IS_BLOCKED = "is_blocked";
    public static final String USER__BALANCE = "balance";
    public static final String USER__ADDRESS_ID = "address_id";
    public static final String USER_ID = "user_id";

    public static final String ADDRESS__COUNTRY = "country";
    public static final String ADDRESS__CITY = "city";
    public static final String ADDRESS__STREET = "street";

    public static final String LANGUAGE__NAME = "name";

    public static final String TARIFF__ID = "tariff_id";
    public static final String TARIFF__PRICE = "price_per_day";
    public static final String TARIFF__CODE = "tariff_code";

    public static final String SERVICE__ID = "service_id";
    public static final String SERVICE__CODE = "service_code";

    public static final String REQUEST__START_DATE = "start_date";
    public static final String REQUEST__END_DATE = "end_date";

    public static final String REQUEST__STATUS = "status";

    public static final String TRANSACTION__AMOUNT = "amount";
    public static final String TRANSACTION__TIMESTAMP = "date_and_time";
    public static final String TRANSACTION__DESCRIPTION = "description";

}
