package com.nahorniak.finalproject.service.impl;

import com.nahorniak.finalproject.DAO.DBConnection;
import com.nahorniak.finalproject.DAO.TransactionDAO;
import com.nahorniak.finalproject.DAO.entity.*;
import com.nahorniak.finalproject.service.RequestService;
import com.nahorniak.finalproject.service.TariffService;
import com.nahorniak.finalproject.service.TransactionService;
import com.nahorniak.finalproject.service.UserService;
import com.nahorniak.finalproject.service.context.AppContext;
import com.nahorniak.finalproject.util.mail.MailSender;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static com.nahorniak.finalproject.util.constants.MailConstants.*;

/**
 * Transaction Service implementation
 *
 * @author Oleh Nahorniak.
 */
public class TransactionServiceImpl implements TransactionService {
    private TransactionDAO transactionDAO;

    public TransactionServiceImpl() {
        this.transactionDAO = TransactionDAO.getInstance();
    }


    @Override
    public void topUp(User user, double amount, Connection connection) throws SQLException {
        RequestService requestService = AppContext.getInstance().getRequestService();
        UserService userService = AppContext.getInstance().getUserService();

        Transaction.Builder builder = new Transaction.Builder();
        Transaction transaction =
                builder.withUserId(user.getId())
                        .withAmount(amount)
                        .withTimeStamp(Timestamp.from(Instant.now()))
                        .withDescription("TOP_UP")
                        .build();

        save(transaction, connection);
        user.setBalance(amount);
        userService.update(user, connection);

        if (user.getIsBlocked() && user.getBalance() > 0) {

            Request request = requestService.getRequestForUser(user.getId(), connection);
            if (request != null && request.getStatus().equalsTo("SUSPENDED")) {
                requestService.activateRequest(request.getId(), connection);
            }
            user.setBlocked(false);
            userService.update(user, connection);
        }

    }

    @Override
    public void save(Transaction transaction, Connection connection) throws SQLException {
        transactionDAO.create(transaction, connection);
    }

    @Override
    public void getAll(HttpServletRequest request) {
        DBConnection dbConnection = DBConnection.getInstance();
        HttpSession session = request.getSession();

        int id = (int) session.getAttribute("userId");


        List<Transaction> transactions = new ArrayList<>();

        String paramPage = request.getParameter("page");
        String sortBy = request.getParameter("sortBy");
        String order = request.getParameter("order");

        String orderBy = " order by id asc limit ? offset ?";

        if (sortBy != null && order != null) {
            orderBy = " order by " + sortBy + " " + order + " limit ? offset ?";
        }

        int page = 1;
        int itemsPerPage = 8;


        try (Connection connection = dbConnection.getConnection()) {

            int allItems = transactionDAO.getCount(id, connection);
            int maxPages = (int) Math.ceil(allItems * 1.0 / itemsPerPage);


            if (paramPage != null && paramPage.matches("[0-9]+")) {
                page = Integer.parseInt(paramPage);
            }

            if (page == 1) {
                page -= 1;
            } else if (page > 1) {
                page = (page * itemsPerPage) - itemsPerPage;
            }

            System.out.println(page);

            transactions = transactionDAO.getAllByUserId(id, orderBy, itemsPerPage, page, connection);

            request.setAttribute("transactions", transactions);
            request.setAttribute("maxPages", maxPages);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Transaction> getAllByUser(int userId, String orderBy) {
        List<Transaction> transactions = new ArrayList<>();
        DBConnection dbConnection = DBConnection.getInstance();
        try (Connection connection = dbConnection.getConnection()) {
            transactions = transactionDAO.getAllByUserId(userId, orderBy, connection);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return transactions;
    }

    public void dailyPaymentAndBlock() {
        UserService userService = AppContext.getInstance().getUserService();
        DBConnection dbConnection = DBConnection.getInstance();
        try (Connection connection = dbConnection.getConnection()) {
            List<User> users = userService.getAll(connection);
            for (User user : users) {
                dailyPaymentAndBlock(user, connection);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void dailyPaymentAndBlock(User user, Connection connection) {

        RequestService requestService = AppContext.getInstance().getRequestService();
        TariffService tariffService = AppContext.getInstance().getTariffService();
        UserService userService = AppContext.getInstance().getUserService();
        try {
            Request request = requestService.getRequestForUser(user.getId(), connection);
            if (request != null && user.getIsBlocked() == false && request.getStatus().equalsTo("ACTIVE")) {
                TariffServices tariff = tariffService.getTariffWithServices("en", request.getTariffId(), connection);

                long millis = System.currentTimeMillis();
                LocalDate date = LocalDate.now();

                if (date.compareTo(request.getEndDate()) < 0) {
                    double price = tariff.getTariff().getPrice();

                    user.setBalance(-price);


                    Transaction.Builder builder = new Transaction.Builder();
                    Transaction transaction = builder
                            .withUserId(user.getId())
                            .withTimeStamp(Timestamp.from(Instant.now()))
                            .withAmount(-price)
                            .withDescription("PAYMENT_FOR_SERVICES")
                            .build();

                    save(transaction, connection);
                    if (user.getBalance() < 0) {

                        new MailSender(user, MAIL_TARIFF_ARREARS, MAIL_NOTIFICATION_SUBJECT).start();

                        user.setBlocked(true);
                        requestService.suspendRequest(request.getId(), connection);
                    }
                    userService.update(user, connection);
                } else {
                    requestService.closeRequest(request.getId(), connection);
                    new MailSender(user, MAIL_TARIFF_EXPIRES, MAIL_NOTIFICATION_SUBJECT).start();

                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
