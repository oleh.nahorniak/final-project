package com.nahorniak.finalproject.service.impl;

import com.nahorniak.finalproject.DAO.DBConnection;
import com.nahorniak.finalproject.DAO.UserDAO;
import com.nahorniak.finalproject.DAO.entity.User;
import com.nahorniak.finalproject.service.UserService;

import javax.servlet.http.HttpServletRequest;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * User Service implementation
 *
 * @author Oleh Nahorniak.
 */
public class UserServiceImpl implements UserService {

    private UserDAO userDAO;

    public UserServiceImpl() {
        this.userDAO = UserDAO.getInstance();
    }


    @Override
    public void create(User user, Connection connection) throws SQLException {
        userDAO.insertUser(user, connection);
    }

    @Override
    public void update(User user, Connection connection) throws SQLException {
        userDAO.updateUser(user, connection);
    }

    @Override
    public List<User> getAll(Connection connection) throws SQLException {
        return userDAO.getAll("", connection);
    }

    @Override
    public void changeBlockStatus(boolean flag, int userId, HttpServletRequest request) throws SQLException {
        DBConnection dbConnection = DBConnection.getInstance();
        try (Connection connection = dbConnection.getConnection()) {
            userDAO.changeBlockStatus(flag, userId, connection);
            List<User> customers = userDAO.getAll("", connection);
            request.getSession().setAttribute("customers", customers);
        } catch (SQLException e) {
            throw e;
        }
    }

    @Override
    public void setUserList(HttpServletRequest request) throws SQLException {
        List<User> customers = new ArrayList<>();
        DBConnection dbConnection = DBConnection.getInstance();

        String paramPage = request.getParameter("page");
        String sortBy = request.getParameter("sortBy");
        String order = request.getParameter("order");

        String orderBy = " order by id asc limit ? offset ?";

        if (sortBy != null && order != null) {
            orderBy = " order by " + sortBy + " " + order + " limit ? offset ?";
        }

        int page = 1;
        int itemsPerPage = 5;


        try (Connection connection = dbConnection.getConnection()) {

            int allItems = userDAO.getCount(connection);
            int maxPages = (int) Math.ceil(allItems * 1.0 / itemsPerPage);

            if (paramPage != null && paramPage.matches("[0-9]+")) {
                page = Integer.parseInt(paramPage);
            }

            if (page == 1) {
                page -= 1;
            } else if (page > 1) {
                page = (page * itemsPerPage) - itemsPerPage;
            }

            customers = userDAO.getAll(orderBy, itemsPerPage, page, connection);
            if (customers != null) {
                request.setAttribute("customers", customers);
            }
            request.setAttribute("maxPages", maxPages);
        } catch (SQLException e) {
            throw e;
        }
    }

    @Override
    public List<User> getAll(String orderBy) {
        DBConnection dbConnection = DBConnection.getInstance();
        List<User> users = new ArrayList<>();
        try (Connection connection = dbConnection.getConnection()) {
            users = userDAO.getAll(orderBy, connection);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users;
    }

    @Override
    public User getUser(String login, String password, Connection connection) throws SQLException {
        return userDAO.getUser(login, password, connection);
    }

    @Override
    public User getUserById(int id) throws SQLException {
        DBConnection dbConnection = DBConnection.getInstance();
        try (Connection connection = dbConnection.getConnection()) {
            return userDAO.getUserById(id, connection);
        }
    }

    @Override
    public User getUserByEmail(String email) throws SQLException {
        DBConnection dbConnection = DBConnection.getInstance();
        try (Connection connection = dbConnection.getConnection()) {
            return userDAO.getUserByEmail(email, connection);
        }
    }


}
