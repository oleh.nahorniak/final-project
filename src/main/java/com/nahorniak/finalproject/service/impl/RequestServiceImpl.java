package com.nahorniak.finalproject.service.impl;

import com.nahorniak.finalproject.DAO.DBConnection;
import com.nahorniak.finalproject.DAO.RequestDAO;
import com.nahorniak.finalproject.DAO.entity.*;
import com.nahorniak.finalproject.service.RequestService;
import com.nahorniak.finalproject.service.TariffService;
import com.nahorniak.finalproject.service.TransactionService;
import com.nahorniak.finalproject.service.UserService;
import com.nahorniak.finalproject.service.context.AppContext;
import com.nahorniak.finalproject.util.mail.MailSender;
import com.nahorniak.finalproject.util.reCaptcha.VerifyUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import static com.nahorniak.finalproject.util.constants.MailConstants.*;

/**
 * Request Service implementation
 *
 * @author Oleh Nahorniak.
 */
public class RequestServiceImpl implements RequestService {
    private static final String ACTIVE = "ACTIVE";
    private static final String SUSPENDED = "SUSPENDED";

    private final RequestDAO requestDAO;


    public RequestServiceImpl() {
        this.requestDAO = RequestDAO.getInstance();

    }

    @Override
    public Request getRequestForUser(int user_id, Connection connection) throws SQLException {
        Request request = null;

        if (requestDAO.isActiveRequest(user_id, connection)) {
            request = requestDAO.getActiveOrSuspendedRequest(user_id, ACTIVE, connection);
        } else if (requestDAO.isSuspendedRequest(user_id, connection)) {
            request = requestDAO.getActiveOrSuspendedRequest(user_id, SUSPENDED, connection);
        }

        return request;
    }

    @Override
    public void setRequestForUser(HttpServletRequest request, Connection connection) throws SQLException {
        TariffService tariffService = AppContext.getInstance().getTariffService();
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        if (user != null && user.getRole() == Role.CUSTOMER) {
            int userId = user.getId();
            String lang = (String) session.getAttribute("lang");
            Request userRequest = getRequestForUser(userId, connection);
            if (userRequest != null) {
                TariffServices tariffServices = tariffService.getTariffWithServices(lang, userRequest.getTariffId(), connection);
                request.setAttribute("userRequest", userRequest);
                request.setAttribute("tariffServices", tariffServices);
            }
        }
    }

    @Override
    public void changeRequest(HttpServletRequest request) {
        DBConnection dbConnection = DBConnection.getInstance();
        RequestDAO requestDAO = RequestDAO.getInstance();

        String id = request.getParameter("id");
        String status = request.getParameter("status");
        User user = (User) request.getSession().getAttribute("user");
        HttpSession session = request.getSession();


        try (Connection connection = dbConnection.getConnection()) {
            if (id != null && status != null) {
                int requestId = Integer.parseInt(id);
                requestDAO.changeStatus(requestId, status, connection);
                if (Status.ACTIVE.equalsTo(status)) {
                    session.setAttribute("errorMessage", "TARIFF_ACTIVATED");
                } else if (Status.SUSPENDED.equalsTo(status)) {
                    session.setAttribute("errorMessage", "TARIFF_SUSPENDED");
                } else if (Status.CLOSED.equalsTo(status)) {
                    session.setAttribute("errorMessage", "TARIFF_CLOSED");
                }
                System.out.println(status);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getAllByUser(HttpServletRequest request) {
        TariffService tariffService = AppContext.getInstance().getTariffService();

        String applicationLang = (String) request.getSession().getAttribute("lang");
        String paramPage = request.getParameter("page");
        String sortBy = request.getParameter("sortBy");
        String order = request.getParameter("order");

        List<RequestTariff> requestTariffs = new ArrayList<>();
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");

        DBConnection dbConnection = DBConnection.getInstance();

        String orderBy = " order by id asc limit ? offset ?";

        if (sortBy != null && order != null) {
            orderBy = " order by " + sortBy + " " + order + " limit ? offset ?";
        }

        int page = 1;
        int itemsPerPage = 5;


        if (user != null && user.getRole() == Role.CUSTOMER) {
            int userId = user.getId();
            try (Connection connection = dbConnection.getConnection()) {

                int allItems = requestDAO.getCount(userId, connection);
                int maxPages = (int) Math.ceil(allItems * 1.0 / itemsPerPage);

                if (paramPage != null && paramPage.matches("[0-9]+")) {
                    page = Integer.parseInt(paramPage);
                }


                if (page == 1) {
                    page -= 1;
                } else if (page > 1) {
                    page = (page * itemsPerPage) - itemsPerPage;
                }

                List<Request> userRequests = requestDAO.getAllByUserId(userId, orderBy, itemsPerPage, page, connection);
                userRequests.stream().forEach(x -> {
                    try {
                        TariffServices tariffServices = tariffService.getTariffWithServices(applicationLang, x.getTariffId(), connection);
                        RequestTariff requestTariff = new RequestTariff();
                        requestTariff.setRequest(x);
                        requestTariff.setTariffServices(tariffServices);
                        requestTariffs.add(requestTariff);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                });

                request.setAttribute("requestTariffs", requestTariffs);
                request.setAttribute("maxPages", maxPages);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public List<RequestTariff> getAll(String lang, int userId, String orderBy) {
        TariffService tariffService = AppContext.getInstance().getTariffService();
        List<Request> requests = new ArrayList<>();
        List<RequestTariff> requestTariffs = new ArrayList<>();
        DBConnection dbConnection = DBConnection.getInstance();

        try (Connection connection = dbConnection.getConnection()) {
            requests = requestDAO.getAllByUserId(userId, orderBy, connection);
            requests.stream().forEach(x -> {
                try {
                    TariffServices tariffServices = tariffService.getTariffWithServices(lang, x.getTariffId(), connection);
                    RequestTariff requestTariff = new RequestTariff();
                    requestTariff.setRequest(x);
                    requestTariff.setTariffServices(tariffServices);
                    requestTariffs.add(requestTariff);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            });
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return requestTariffs;
    }

    @Override
    public void connect(HttpServletRequest request) {
        TariffService tariffService = AppContext.getInstance().getTariffService();
        UserService userService = AppContext.getInstance().getUserService();
        TransactionService transactionService = AppContext.getInstance().getTransactionService();

        DBConnection dbConnection = DBConnection.getInstance();
        HttpSession session = request.getSession();
        String lang = (String) session.getAttribute("lang");
        User user = (User) session.getAttribute("user");
        String id = request.getParameter("tariffId");
        String date = request.getParameter("endDate");
        System.out.println(request.getParameter("g-recaptcha-response"));


        if (user != null && id != null && lang != null && date != null) {
            int userId = user.getId();
            int tariffId = Integer.parseInt(id);
            try (Connection connection = dbConnection.getConnection()) {
                Date endDate = Date.valueOf(date);

                String gRecaptchaResponse = request.getParameter("g-recaptcha-response");
                if (VerifyUtils.verify(gRecaptchaResponse)) {
                    if (!requestDAO.isSuspendedRequest(userId, connection) && !requestDAO.isActiveRequest(userId, connection)) {
                        TariffServices tariff = tariffService.getTariffWithServices(lang, tariffId, connection);

                        double price = tariff.getTariff().getPrice();

                        user.setBalance(-price);


                        if (user.getBalance() > 0) {
                            userService.update(user, connection);

                            Transaction.Builder builder = new Transaction.Builder();
                            Transaction transaction =
                                    builder.withUserId(userId)
                                            .withAmount(-price)
                                            .withTimeStamp(Timestamp.from(Instant.now()))
                                            .withDescription("TARIFF_CONNECTION")
                                            .build();

                            transactionService.save(transaction, connection);

                            Request.Builder requestBuilder = new Request.Builder();
                            Request userRequest =
                                    requestBuilder
                                            .withUserId(userId)
                                            .withTariffId(tariffId)
                                            .withEndDate(endDate.toLocalDate())
                                            .build();

                            requestDAO.insertRequest(userRequest, connection);

                            ResourceBundle resourceBundle = ResourceBundle.getBundle("text",
                                    new Locale((String) request.getSession().getAttribute("lang")));

                            String message = tariff.getTariff().getTitle() + " - " + resourceBundle.getString("TARIFF_SUCCESSFULLY_CONNECTED") + " \n"
                                    + resourceBundle.getString("THANK_YOU_FOR_CHOOSING_US");

                            new MailSender(user, message, MAIL_NOTIFICATION_SUBJECT).start();

                            session.setAttribute("errorMessage", "TARIFF_SUCCESSFULLY_CONNECTED");
                            session.setAttribute("user", user);
                        } else {
                            session.setAttribute("errorMessage", "NOT_ENOUGH_MONEY_TO_CONNECT_TARIFF");
                            user.setBalance(price);
                        }
                    } else session.setAttribute("errorMessage", "THERE_IS_ALREADY_ACTIVE_OR_SUSPENDED_REQUEST");
                } else {
                    session.setAttribute("captchaError", "INVALID_CAPTCHA");
                    session.setAttribute("tariffId", tariffId);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void create(Request request, Connection connection) {
        requestDAO.insertRequest(request, connection);
    }

    @Override
    public void suspendRequest(int requestId, Connection connection) throws SQLException {
        requestDAO.changeStatus(requestId, "SUSPENDED", connection);
    }

    @Override
    public void closeRequest(int requestId, Connection connection) throws SQLException {
        requestDAO.changeStatus(requestId, "CLOSED", connection);
    }

    @Override
    public void activateRequest(int requestId, Connection connection) throws SQLException {
        requestDAO.changeStatus(requestId, "ACTIVE", connection);
    }
}
