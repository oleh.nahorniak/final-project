package com.nahorniak.finalproject.service.impl;

import com.nahorniak.finalproject.DAO.DBConnection;
import com.nahorniak.finalproject.DAO.TariffServicesDAO;
import com.nahorniak.finalproject.DAO.entity.TariffServices;
import com.nahorniak.finalproject.service.TariffService;
import com.nahorniak.finalproject.service.UserService;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Tariff Service implementation
 *
 * @author Oleh Nahorniak.
 */
public class TariffServiceImpl implements TariffService {

    private TariffServicesDAO tariffServicesDAO;


    public TariffServiceImpl() {
        tariffServicesDAO = TariffServicesDAO.getInstance();
    }


    @Override
    public TariffServices getTariffWithServices(String lang, int id, Connection connection) throws SQLException {
        return tariffServicesDAO.getTariffWithServices(lang, id, connection);
    }

    @Override
    public Set<TariffServices> getAll(String lang, String orderBy) {
        Set<TariffServices> tariffServices = new LinkedHashSet<>();
        DBConnection dbConnection = DBConnection.getInstance();

        try (Connection connection = dbConnection.getConnection()) {
            tariffServices = tariffServicesDAO.getAll(lang, orderBy, connection);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tariffServices;
    }


}
