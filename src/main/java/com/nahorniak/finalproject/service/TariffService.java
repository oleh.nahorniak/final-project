package com.nahorniak.finalproject.service;

import com.nahorniak.finalproject.DAO.entity.TariffServices;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Set;

/**
 * Tariff Service interface
 *
 * @author Oleh Nahorniak.
 */
public interface TariffService {

    TariffServices getTariffWithServices(String lang, int id, Connection connection) throws SQLException;

    Set<TariffServices> getAll(String lang, String orderBy);
}
