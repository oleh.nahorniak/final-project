package com.nahorniak.finalproject.service;

import com.nahorniak.finalproject.DAO.entity.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * User Service interface
 *
 * @author Oleh Nahorniak.
 */
public interface UserService {

    void create(User user, Connection connection) throws SQLException;

    void update(User user, Connection connection) throws SQLException;

    List<User> getAll(Connection connection) throws SQLException;

    List<User> getAll(String orderBy);

    void changeBlockStatus(boolean flag, int userId, HttpServletRequest request) throws SQLException;

    void setUserList(HttpServletRequest request) throws SQLException;

    User getUser(String login, String password, Connection connection) throws SQLException;

    User getUserById(int id) throws SQLException;

    User getUserByEmail(String email) throws SQLException;
}
