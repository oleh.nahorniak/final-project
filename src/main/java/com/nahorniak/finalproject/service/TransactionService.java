package com.nahorniak.finalproject.service;

import com.nahorniak.finalproject.DAO.entity.Transaction;
import com.nahorniak.finalproject.DAO.entity.User;

import javax.servlet.http.HttpServletRequest;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * Transaction Service interface
 *
 * @author Oleh Nahorniak.
 */
public interface TransactionService {

    void topUp(User user, double amount, Connection connection) throws SQLException;

    void save(Transaction transaction, Connection connection) throws SQLException;

    void getAll(HttpServletRequest request);

    void dailyPaymentAndBlock();

    void dailyPaymentAndBlock(User user, Connection connection);

    List<Transaction> getAllByUser(int userId, String orderBy);
}
