package com.nahorniak.finalproject.service.context;

import com.nahorniak.finalproject.service.RequestService;
import com.nahorniak.finalproject.service.TariffService;
import com.nahorniak.finalproject.service.TransactionService;
import com.nahorniak.finalproject.service.UserService;
import com.nahorniak.finalproject.service.impl.RequestServiceImpl;
import com.nahorniak.finalproject.service.impl.TariffServiceImpl;
import com.nahorniak.finalproject.service.impl.TransactionServiceImpl;
import com.nahorniak.finalproject.service.impl.UserServiceImpl;

/**
 * Class creates all dao and services on app starts.
 *
 * @author Oleh Nahorniak.
 */

public class AppContext {
    private static volatile AppContext appContext = new AppContext();
    private final RequestService requestService = new RequestServiceImpl();
    private final TariffService tariffService = new TariffServiceImpl();
    private final UserService userService = new UserServiceImpl();
    private final TransactionService transactionService = new TransactionServiceImpl();

    public static AppContext getInstance() {
        return appContext;
    }
    
    public RequestService getRequestService() {
        return requestService;
    }

    public TariffService getTariffService() {
        return tariffService;
    }

    public UserService getUserService() {
        return userService;
    }

    public TransactionService getTransactionService() {
        return transactionService;
    }
}
