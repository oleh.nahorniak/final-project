package com.nahorniak.finalproject.service;

import com.nahorniak.finalproject.DAO.entity.Request;
import com.nahorniak.finalproject.DAO.entity.RequestTariff;

import javax.servlet.http.HttpServletRequest;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * Request Service interface
 *
 * @author Oleh Nahorniak.
 */
public interface RequestService {
    Request getRequestForUser(int user_id, Connection connection) throws SQLException;

    void setRequestForUser(HttpServletRequest request, Connection connection) throws SQLException;

    void changeRequest(HttpServletRequest request);

    void getAllByUser(HttpServletRequest request);

    List<RequestTariff> getAll(String lang, int userId, String orderBy);

    void connect(HttpServletRequest request);

    void create(Request request, Connection connection);

    void suspendRequest(int requestId, Connection connection) throws SQLException;

    void closeRequest(int requestId, Connection connection) throws SQLException;

    void activateRequest(int requestId, Connection connection) throws SQLException;
}
