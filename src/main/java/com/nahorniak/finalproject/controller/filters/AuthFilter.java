package com.nahorniak.finalproject.controller.filters;

import com.nahorniak.finalproject.DAO.entity.User;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


/**
 * Authorization filter.
 *
 * @author Oleh Nahorniak
 */

@WebFilter(filterName = "AuthFilter")
public class AuthFilter implements Filter {
    private static final Logger log = Logger.getLogger(AuthFilter.class);


    /**
     * doFilter method that filter request, already logged user cannot receive login page
     *
     * @param request
     * @param response
     * @param chain
     * @throws ServletException
     * @throws IOException
     */

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
        log.debug("Filter starts");
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        HttpSession session = req.getSession();
        User person = (User) session.getAttribute("user");
        if (person != null) {
            resp.sendRedirect("profile");
        } else {
            log.debug("Filter finished");
            chain.doFilter(req, resp);
        }
    }

    /**
     * Destroy method.
     */

    public void destroy() {
        log.debug("Filter destruction starts");
        //-----------------
        log.debug("Filter destruction finished");
    }

}
