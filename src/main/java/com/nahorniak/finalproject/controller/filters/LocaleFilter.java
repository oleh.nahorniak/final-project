package com.nahorniak.finalproject.controller.filters;

import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Locale filter.
 *
 * @author Oleh Nahorniak
 */
@WebFilter(filterName = "LocaleFilter")
public class LocaleFilter implements Filter {

    private static final Logger log = Logger.getLogger(LocaleFilter.class);

    /**
     * Destroy method.
     */

    public void destroy() {
        log.debug("Filter destruction starts");
        // do nothing
        log.debug("Filter destruction finished");
    }


    /**
     * doFilter method , takes locale from cookies and set it to session
     *
     * @param request
     * @param response
     * @param chain
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
        log.debug("Filter starts");
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        HttpSession session = req.getSession();

        if (session.isNew()) {
            Cookie lang = null;
            Cookie[] cookies = req.getCookies();

            if (cookies != null)
                for (Cookie cookie : cookies) {
                    if (cookie.getName().equals("lang")) lang = cookie;
                }

            if (lang == null) {
                lang = new Cookie("lang", "en");
                lang.setMaxAge(60 * 60 * 24 * 7);
                resp.addCookie(lang);
                log.trace("Request locale = null, set locale --> " + lang.getName() + " : " + lang.getValue());
            }

            session.setAttribute("lang", lang.getValue());
        } else if (session.getAttribute("lang") == null) session.setAttribute("lang", "en");

        log.debug("Filter finished");
        chain.doFilter(request, response);
    }
}
