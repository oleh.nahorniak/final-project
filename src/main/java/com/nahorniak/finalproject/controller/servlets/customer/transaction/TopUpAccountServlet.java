package com.nahorniak.finalproject.controller.servlets.customer.transaction;

import com.nahorniak.finalproject.DAO.entity.User;
import com.nahorniak.finalproject.DAO.DBConnection;
import com.nahorniak.finalproject.service.TransactionService;
import com.nahorniak.finalproject.service.context.AppContext;
import com.nahorniak.finalproject.util.mail.MailSender;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import static com.nahorniak.finalproject.util.constants.MailConstants.*;
import static com.nahorniak.finalproject.util.constants.Path.TOP_UP;
import static com.nahorniak.finalproject.util.constants.Path.TOP_UP_ACCOUNT_JSP;

/**
 * TopUpAccountServlet - servlet to top up account .
 *
 * @author Oleh Nahorniak.
 */
@WebServlet(value = "/topUpAccount", name = "topUpAccount")
public class TopUpAccountServlet extends HttpServlet {

    /**
     * doGet method - forwards to topUpAccount.jsp
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher(TOP_UP_ACCOUNT_JSP).forward(request, response);
    }

    /**
     * doPost method - allows customer to top up account
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String value = request.getParameter("value");
        HttpSession session = request.getSession();

        StringBuilder message = new StringBuilder("");

        TransactionService transactionService = AppContext.getInstance().getTransactionService();

        if (value != null && !value.isBlank()) {
            double number = Double.parseDouble(value);
            User user = (User) session.getAttribute("user");

            DBConnection dbConnection = DBConnection.getInstance();

            try (Connection connection = dbConnection.getConnection()) {

                transactionService.topUp(user, number, connection);


                String msg = "Ваш рахунок поповнено на " + number + "$ ! \nДякуємо, що вибрали нас :)";

                new MailSender(user, msg, MAIL_NOTIFICATION_SUBJECT).start();

                message.append("BALANCE_HAS_BEEN_REPLENISHED");
                session.setAttribute("user", user);

            } catch (SQLException e) {
                message.append("BALANCE_TOP_UP_FAIL");
                e.printStackTrace();
            }
        } else message.append("PLEASE_INPUT_NUMBER");

        session.setAttribute("errorMessage", message);
        response.sendRedirect(TOP_UP);
    }
}

