package com.nahorniak.finalproject.controller.servlets.outOfControl;

import com.nahorniak.finalproject.DAO.DBConnection;
import com.nahorniak.finalproject.DAO.entity.User;
import com.nahorniak.finalproject.service.UserService;
import com.nahorniak.finalproject.service.context.AppContext;
import com.nahorniak.finalproject.util.mail.MailSender;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Locale;
import java.util.Random;
import java.util.ResourceBundle;

import static com.nahorniak.finalproject.util.constants.MailConstants.*;
import static com.nahorniak.finalproject.util.constants.Path.LOGIN;

/**
 * DeleteAttributes - servlet to send recovery code on email
 *
 * @author Oleh Nahorniak.
 */

@WebServlet(name = "forgotPassword", value = "/forgotPassword")
public class ForgotPasswordServlet extends HttpServlet {
    /**
     * doGet method - sendRedirect to error-page
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendError(404);
    }


    /**
     * doPost method - send recovery code on email
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String email = request.getParameter("email");
        HttpSession session = request.getSession();
        UserService userService = AppContext.getInstance().getUserService();

        try {
            User user = userService.getUserByEmail(email);
            System.out.println(user);
            if (user == null) {
                session.setAttribute("error", "THERE_IS_NO_USER_WITH_THIS_EMAIL");
                response.sendRedirect(LOGIN);
            } else {
                Random random = new Random();
                int number = random.nextInt(99999);
                String code = String.format("%06d", number);

                ResourceBundle resourceBundle = ResourceBundle.getBundle("text",
                        new Locale((String) request.getSession().getAttribute("lang")));

                String message = resourceBundle.getString("RECOVERY_CODE") + code;

                new MailSender(user, message, MAIL_FORGOT_PASSWORD).start();

                session.setAttribute("email", email);
                session.setAttribute("code", code);
                response.sendRedirect(LOGIN);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
