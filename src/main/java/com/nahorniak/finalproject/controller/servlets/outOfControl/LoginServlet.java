package com.nahorniak.finalproject.controller.servlets.outOfControl;

import com.nahorniak.finalproject.DAO.DBConnection;
import com.nahorniak.finalproject.DAO.entity.User;
import com.nahorniak.finalproject.DAO.entity.Role;
import com.nahorniak.finalproject.service.UserService;
import com.nahorniak.finalproject.service.context.AppContext;
import com.nahorniak.finalproject.service.impl.UserServiceImpl;
import com.nahorniak.finalproject.util.Encryption;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import static com.nahorniak.finalproject.util.constants.Path.LOGIN_JSP;

/**
 * DeleteAttributes - servlet to login
 *
 * @author Oleh Nahorniak.
 */
@WebServlet(value = "/login", name = "login")
public class LoginServlet extends HttpServlet {
    public static final String PROFILE = "profile";
    public static final String LOGIN = "login";

    /**
     * doPost method to login and get all rights
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DBConnection dbConnection = DBConnection.getInstance();
        UserService userService = AppContext.getInstance().getUserService();
        HttpSession session = request.getSession();
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        if (login != null && password != null) {


            try (Connection connection = dbConnection.getConnection()) {
                Role role = Role.UNKNOWN;
                String encryptedPassword = Encryption.md5(password);
                User user = userService.getUser(login, encryptedPassword, connection);

                if (user == null) {
                    session.setAttribute("errorMessage", "INCORRECT_LOGIN_OR_PASSWORD");
                    response.sendRedirect(LOGIN);
                } else {
                    role = user.getRole();
                    int id = user.getId();
                    session.setAttribute("user", user);
                    session.setAttribute("role", role);
                    session.setAttribute("userId", id);
                    moveToPage(request, response, role);
                }
            } catch (SQLException e) {
                session.setAttribute("errorMessage", "INCORRECT_LOGIN_OR_PASSWORD");
                response.sendRedirect(LOGIN);
            }
        } else {
            session.setAttribute("errorMessage", "INCORRECT_LOGIN_OR_PASSWORD");
            response.sendRedirect(LOGIN);
        }
    }

    /**
     * doGet method - forwards to login.jsp
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher(LOGIN_JSP).forward(request, response);
    }

    private void moveToPage(HttpServletRequest req, HttpServletResponse resp, Role role) throws ServletException, IOException {
        if (role == Role.CUSTOMER || role == Role.ADMIN) {
            resp.sendRedirect(PROFILE);
        } else if (role == Role.UNKNOWN) {
            req.getSession().setAttribute("errorMessage", "INCORRECT_LOGIN_OR_PASSWORD");
            resp.sendRedirect(LOGIN);
        }
    }
}