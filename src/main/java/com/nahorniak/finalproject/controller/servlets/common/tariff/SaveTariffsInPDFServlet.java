package com.nahorniak.finalproject.controller.servlets.common.tariff;

import com.nahorniak.finalproject.DAO.entity.TariffServices;
import com.nahorniak.finalproject.service.TariffService;
import com.nahorniak.finalproject.service.context.AppContext;
import com.nahorniak.finalproject.util.PDF.TariffPDFBuilder;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * saveTariffsInPDF - servlet to save all tariffs in pdf format .
 *
 * @author Oleh Nahorniak.
 */
@WebServlet(name = "saveTariffsInPDF", value = "/saveTariffsInPDF")
public class SaveTariffsInPDFServlet extends HttpServlet {

    /**
     * doGet method - redirect to error-page
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendError(404);
    }

    /**
     * doPost method - allows user to save all available tariffs in one pdf
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TariffService tariffService = AppContext.getInstance().getTariffService();
        Set<TariffServices> tariffs = new LinkedHashSet<>();

        String sortBy = request.getParameter("sortBy");
        String order = request.getParameter("order");
        String lang = (String) request.getSession().getAttribute("lang");

        String orderBy = " order by t.id asc ";

        if (sortBy != null && !sortBy.isBlank()
                && order != null && !order.isBlank()) {
            orderBy = " order by " + sortBy + " " + order + " ";
        }

        tariffs = tariffService.getAll(lang, orderBy);

        new TariffPDFBuilder().openPDF(request, response, tariffs);
    }
}
