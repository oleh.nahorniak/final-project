package com.nahorniak.finalproject.controller.servlets.common.tariff;

import com.nahorniak.finalproject.DAO.DBConnection;
import com.nahorniak.finalproject.DAO.TariffServicesDAO;
import com.nahorniak.finalproject.DAO.entity.TariffServices;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Set;

import static com.nahorniak.finalproject.util.constants.Path.*;


/**
 * TariffsServlet - servlet to get all available tariffs on jsp page .
 *
 * @author Oleh Nahorniak.
 */

@WebServlet(name = "tariffs", value = "/tariffs")
public class TariffsServlet extends HttpServlet {

    /**
     * doGet method - set all tariffs as request attribute and forward to
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String page = request.getParameter("page");
        if (page != null && !page.matches("[0-9]+")) {
            response.sendRedirect(TARIFFS + "?page=1");
        } else {
            setTariffs(request, response);
            request.getRequestDispatcher(TARIFFS_JSP).forward(request, response);
        }
    }

    /**
     * doPost method = doGet
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    /**
     * private method setTariffs to set all tariffs as request attribute
     *
     * @param request
     * @param response
     * @throws IOException
     * @throws ServletException
     */
    private void setTariffs(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        HttpSession session = request.getSession();
        String applicationLang = (String) session.getAttribute("lang");

        String paramPage = request.getParameter("page");
        String sortBy = request.getParameter("sortBy");
        String order = request.getParameter("order");

        String orderBy = " order by t.id asc limit ? offset ?";

        if (sortBy != null && order != null) {
            orderBy = " order by " + sortBy + " " + order + " limit ? offset ?";
        }

        int page = 1;
        int itemsPerPage = 4;

        DBConnection dbConnection = DBConnection.getInstance();
        TariffServicesDAO tariffServicesDAO = TariffServicesDAO.getInstance();

        try (Connection connection = dbConnection.getConnection()) {
            int allItems = tariffServicesDAO.getTariffsCount("", connection);
            int maxPages = getMaxPages(allItems, itemsPerPage);


            if (paramPage != null && paramPage.matches("[1-9]")) {
                page = Integer.parseInt(paramPage);
            }


            if (page == 1) {
                page -= 1;
            } else if (page > 1) {
                page = (page * itemsPerPage) - itemsPerPage;
            }


            Set<TariffServices> tariffServices = tariffServicesDAO.getAll(applicationLang, orderBy, itemsPerPage, page, connection);

            request.setAttribute("tariffServices", tariffServices);
            request.setAttribute("maxPages", maxPages);

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private int getMaxPages(int maxItems, int itemsPerPage) {
        return (int) Math.ceil(maxItems * 1.0 / itemsPerPage);
    }
}
