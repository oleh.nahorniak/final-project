package com.nahorniak.finalproject.controller.servlets.admin.tariff;

import com.nahorniak.finalproject.DAO.DBConnection;

import com.nahorniak.finalproject.DAO.TariffServicesDAO;
import com.nahorniak.finalproject.DAO.entity.Service;
import com.nahorniak.finalproject.validation.TariffValidation;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.nahorniak.finalproject.util.constants.Path.*;

/**
 * TariffInsertServlet - servlet to insert new tariff.
 *
 * @author Oleh Nahorniak.
 */
@WebServlet(name = "newTariff", value = "/newTariff")
public class TariffInsertServlet extends HttpServlet {
    private static final Logger log = Logger.getLogger(TariffEditServlet.class);

    /**
     * doGet method - set all services from database into request as attribute to show on a page
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        DBConnection dbConnection = DBConnection.getInstance();
        TariffServicesDAO tariffServicesDAO = TariffServicesDAO.getInstance();

        try (Connection connection = dbConnection.getConnection()) {
            String lang = (String) session.getAttribute("lang");
            Set<Service> allServices = tariffServicesDAO.getAllServices(lang, connection);
            request.setAttribute("allServices", allServices);
            request.getRequestDispatcher(NEW_TARIFF_JSP).forward(request, response);
            log.trace("forwarded to newTariff.jsp with services to insert new tariff");
        } catch (SQLException e) {
            e.printStackTrace();
            log.trace("something went wrong");
            response.sendError(404);
        }
    }

    /**
     * doPost method - insert new tariff into database
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();

        String titleEn = request.getParameter("titleEn");
        String titleUk = request.getParameter("titleUk");
        String code = request.getParameter("code");
        String[] services = request.getParameterValues("services");
        Double price = Double.parseDouble(request.getParameter("price"));
        log.trace("Tariff validation starts...");
        if (services != null && TariffValidation.isValid(titleEn, titleUk, code, price)) {
            log.trace("Tariff validation finished successfully");
            List<Integer> services_id = Stream.of(services).map(x -> Integer.parseInt(x)).collect(Collectors.toList());
            DBConnection dbConnection = DBConnection.getInstance();
            TariffServicesDAO tariffServicesDAO = TariffServicesDAO.getInstance();
            try (Connection connection = dbConnection.getConnection()) {
                tariffServicesDAO.insertTariff(price, code, titleEn, titleUk, services_id, connection);
                session.setAttribute("errorMessage", "TARIFF_SUCCESSFULLY_INSERTED");
                log.trace("Tariff was inserted successfully");
                response.sendRedirect(TARIFFS + "?page=1");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            log.error("Tariff validation error...");
            session.setAttribute("errorMessage", "VALIDATION_ERROR");
            response.sendRedirect(NEW_TARIFF);
        }
    }

}
