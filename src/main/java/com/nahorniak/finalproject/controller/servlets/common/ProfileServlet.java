package com.nahorniak.finalproject.controller.servlets.common;

import com.nahorniak.finalproject.DAO.DBConnection;
import com.nahorniak.finalproject.service.RequestService;
import com.nahorniak.finalproject.service.context.AppContext;
import com.nahorniak.finalproject.service.impl.RequestServiceImpl;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import static com.nahorniak.finalproject.util.constants.Path.PROFILE_JSP;

/**
 * ProfileServlet - servlet to show user profile .
 *
 * @author Oleh Nahorniak.
 */
@WebServlet(name = "profile", value = "/profile")
public class ProfileServlet extends HttpServlet {

    /**
     * doGet method - set user request and forwards to profile.jsp
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestService requestService = AppContext.getInstance().getRequestService();
        DBConnection dbConnection = DBConnection.getInstance();
        try (Connection connection = dbConnection.getConnection()) {
            requestService.setRequestForUser(request, connection);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        request.getRequestDispatcher(PROFILE_JSP).forward(request, response);
    }

    /**
     * doPost method = doGet
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

}
