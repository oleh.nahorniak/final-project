package com.nahorniak.finalproject.controller.servlets.customer.request;

import com.nahorniak.finalproject.DAO.entity.Status;
import com.nahorniak.finalproject.DAO.entity.User;
import com.nahorniak.finalproject.service.RequestService;
import com.nahorniak.finalproject.service.context.AppContext;
import com.nahorniak.finalproject.service.impl.RequestServiceImpl;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

import static com.nahorniak.finalproject.util.constants.Path.PROFILE;

/**
 * ChangeRequestStatusServlet - servlet to change customer request status .
 *
 * @author Oleh Nahorniak.
 */
@WebServlet(name = "changeRequestStatus", value = "/changeRequestStatus")
public class ChangeRequestStatusServlet extends HttpServlet {
    /**
     * doGet method - sendRedirect to error-page
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendError(404);
    }

    /**
     * doPost method - change user request status
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestService requestService = AppContext.getInstance().getRequestService();
        HttpSession session = request.getSession();

        String status = request.getParameter("status");
        User user = (User) session.getAttribute("user");

        if (Status.ACTIVE.equalsTo(status) && user.getIsBlocked() == true) {
            session.setAttribute("errorMessage", "IMPOSSIBLE_TO_ACTIVATE_REQUEST");
            response.sendRedirect(PROFILE);

        } else {
            requestService.changeRequest(request);
            response.sendRedirect(PROFILE);
        }

    }
}
