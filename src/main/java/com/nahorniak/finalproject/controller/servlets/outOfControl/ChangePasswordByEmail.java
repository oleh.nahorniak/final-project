package com.nahorniak.finalproject.controller.servlets.outOfControl;

import com.nahorniak.finalproject.DAO.DBConnection;
import com.nahorniak.finalproject.DAO.entity.User;
import com.nahorniak.finalproject.service.UserService;
import com.nahorniak.finalproject.service.context.AppContext;
import com.nahorniak.finalproject.util.Encryption;
import com.nahorniak.finalproject.validation.UserValidation;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import static com.nahorniak.finalproject.util.constants.Path.LOGIN;

/**
 * ChangePasswordByEmail - servlet that allows to change password by email
 *
 * @author Oleh Nahorniak.
 */
@WebServlet(name = "changePasswordByEmail", value = "/changePasswordByEmail")
public class ChangePasswordByEmail extends HttpServlet {

    /**
     * doGet method - sendRedirect to error-page
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendError(404);
    }

    /**
     * doPost method - change password by email
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        DBConnection dbConnection = DBConnection.getInstance();
        UserService userService = AppContext.getInstance().getUserService();

        String newPassword = request.getParameter("newPassword");
        String confirmPassword = request.getParameter("confirmPassword");

        HttpSession session = request.getSession();

        String email = (String) session.getAttribute("email");

        User user = null;
        try {
            user = userService.getUserByEmail(email);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (!newPassword.equals(confirmPassword)) {
            session.setAttribute("passwordError", "NEW_PASSWORD_NOT_CONFIRMED");
            response.sendRedirect(LOGIN);
        } else {

            user.setPassword(newPassword);

            if (UserValidation.isValidUser(user)) {
                String encryptedPassword = Encryption.md5(newPassword);
                user.setPassword(encryptedPassword);


                try (Connection connection = dbConnection.getConnection()) {
                    userService.update(user, connection);
                    session.removeAttribute("changePassword");
                    session.setAttribute("errorMessage", "PASSWORD_SUCCESSFULLY_CHANGED");
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            } else session.setAttribute("passwordError", "VALIDATION_ERROR");

            response.sendRedirect(LOGIN);

        }
    }
}
