package com.nahorniak.finalproject.controller.servlets.util;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

/**
 * LanguageServlet - servlet to change language
 *
 * @author Oleh Nahorniak.
 */
@WebServlet(name = "languageServlet", value = "/languageServlet")
public class LanguageServlet extends HttpServlet {
    /**
     * doGet method - sendRedirect to error-page
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendError(404);
    }

    /**
     * doPost method - change language
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String language = request.getParameter("lang");
        if (language != null) {
            Cookie lang = getLangCookie(request);
            lang.setValue(language);
            response.addCookie(lang);
            request.getSession().setAttribute("lang", language);
        }
    }

    /**
     * @param request
     * @return lang cookie
     */
    private Cookie getLangCookie(HttpServletRequest request) {
        if (request.getCookies() != null) {
            for (Cookie cookie : request.getCookies()) {
                if (cookie.getName().equals("lang")) {
                    return cookie;
                }
            }
        }

        return null;
    }
}
