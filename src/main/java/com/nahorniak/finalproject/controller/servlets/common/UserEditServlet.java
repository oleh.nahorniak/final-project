package com.nahorniak.finalproject.controller.servlets.common;

import com.nahorniak.finalproject.DAO.DBConnection;
import com.nahorniak.finalproject.DAO.UserDAO;
import com.nahorniak.finalproject.DAO.entity.User;
import com.nahorniak.finalproject.service.UserService;
import com.nahorniak.finalproject.service.context.AppContext;
import com.nahorniak.finalproject.service.impl.UserServiceImpl;
import com.nahorniak.finalproject.validation.UserValidation;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import static com.nahorniak.finalproject.util.constants.Path.USER_EDIT;
import static com.nahorniak.finalproject.util.constants.Path.USER_EDIT_JSP;


/**
 * UserEditServlet - servlet to edit user information .
 *
 * @author Oleh Nahorniak.
 */
@WebServlet(value = "/userEdit", name = "userEdit")
public class UserEditServlet extends HttpServlet {

    public static final String PLEASE_INPUT_FIELDS = "PLEASE_INPUT_FIELDS";
    public static final String SUCCESSFUL_UPDATE = "SUCCESSFUL_UPDATE";
    public static final String UPDATE_FAILURE = "UPDATE_FAILURE";


    /**
     * doGet method forwards to userEdit page
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher(USER_EDIT_JSP).forward(request, response);
    }


    /**
     * doPost method - allows user to edit his information
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        HttpSession session = request.getSession();

        String fName = request.getParameter("fName");
        String lName = request.getParameter("lName");
        String email = request.getParameter("email");
        String phone = request.getParameter("phone");
        String country = request.getParameter("country");
        String city = request.getParameter("city");
        String street = request.getParameter("street");

        User user = (User) session.getAttribute("user");

        user.setFirstName(fName);
        user.setLastName(lName);
        user.setEmail(email);
        user.setPhoneNumber(phone);
        user.setCountry(country);
        user.setCity(city);
        user.setStreet(street);


        if (UserValidation.isValidUser(user)) {


            DBConnection dbConnection = DBConnection.getInstance();
            UserService userService = AppContext.getInstance().getUserService();


            try (Connection connection = dbConnection.getConnection()) {
                userService.update(user, connection);
                session.setAttribute("errorMessage", SUCCESSFUL_UPDATE);
                session.setAttribute("user", user);
            } catch (SQLException e) {
                session.setAttribute("errorMessage", UPDATE_FAILURE);
            }
        } else {
            session.setAttribute("errorMessage", PLEASE_INPUT_FIELDS);
        }

        response.sendRedirect(USER_EDIT);
    }
}
