package com.nahorniak.finalproject.controller.servlets.customer.request;

import com.nahorniak.finalproject.DAO.DBConnection;
import com.nahorniak.finalproject.DAO.RequestDAO;
import com.nahorniak.finalproject.service.RequestService;
import com.nahorniak.finalproject.service.context.AppContext;
import com.nahorniak.finalproject.service.impl.RequestServiceImpl;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import static com.nahorniak.finalproject.util.constants.Path.TARIFFS;

/**
 * ConnectTariffServlet - servlet to connect tariff .
 *
 * @author Oleh Nahorniak.
 */
@WebServlet(name = "connect", value = "/connect")
public class ConnectTariffServlet extends HttpServlet {
    /**
     * doGet method - sendRedirect to error-page
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendError(404);
    }

    /**
     * doPost method connects tariff
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestService requestService = AppContext.getInstance().getRequestService();
        requestService.connect(request);
        response.sendRedirect(TARIFFS);
    }
}
