package com.nahorniak.finalproject.controller.servlets.common;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

/**
 * LogoutServlet - servlet to logout and invalidate session  .
 *
 * @author Oleh Nahorniak.
 */
@WebServlet(value = "/logout", name = "logout")
public class LogoutServlet extends HttpServlet {

    /**
     * doGet method = doPost
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    /**
     * doPost method - invalidate session
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        if (session != null) {
            request.getSession().invalidate();
        }
        response.sendRedirect("/application");
    }
}
