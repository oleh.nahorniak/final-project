package com.nahorniak.finalproject.controller.servlets.admin;

import com.nahorniak.finalproject.DAO.entity.User;
import com.nahorniak.finalproject.service.UserService;
import com.nahorniak.finalproject.service.context.AppContext;
import com.nahorniak.finalproject.util.PDF.CustomersPDFBuilder;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * saveCustomersInPDF - servlet to save all customers in pdf format .
 *
 * @author Oleh Nahorniak.
 */
@WebServlet(name = "saveCustomersInPDF", value = "/saveCustomersInPDF")
public class SaveCustomersInPDFServlet extends HttpServlet {

    /**
     * doGet method - sendRedirect to error-page
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendError(404);
    }

    /**
     * doPost method - allows to save all customers in one pdf
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        UserService userService = AppContext.getInstance().getUserService();
        List<User> users = new ArrayList<>();

        String sortBy = request.getParameter("sortBy");
        String order = request.getParameter("order");
        String lang = (String) request.getSession().getAttribute("lang");

        String orderBy = " order by id asc ";

        if (sortBy != null && !sortBy.isBlank()
                && order != null && !order.isBlank()) {
            orderBy = " order by " + sortBy + " " + order + " ";
        }

        users = userService.getAll(orderBy);

        new CustomersPDFBuilder().openPDF(request, response, users);
    }
}
