package com.nahorniak.finalproject.controller.servlets.admin;

import com.nahorniak.finalproject.DAO.entity.User;
import com.nahorniak.finalproject.service.UserService;
import com.nahorniak.finalproject.service.context.AppContext;
import com.nahorniak.finalproject.util.mail.MailSender;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Locale;
import java.util.ResourceBundle;

import static com.nahorniak.finalproject.util.constants.MailConstants.*;
import static com.nahorniak.finalproject.util.constants.Path.*;

/**
 * Customers Servlet.
 *
 * @author Oleh Nahorniak
 */

@WebServlet(name = "customers", value = "/customers")
public class CustomersServlet extends HttpServlet {

    private static final Logger log = Logger.getLogger(CustomersServlet.class);

    public static final String BLOCK = "Block";
    public static final String UNBLOCK = "Unblock";

    /**
     * doGet method - forward to customers.jsp with all customers
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String page = request.getParameter("page");
        if (page != null && !page.matches("[0-9]+")) {
            response.sendRedirect(CUSTOMERS + "?page=1");
        } else {
            UserService userService = AppContext.getInstance().getUserService();
            try {
                userService.setUserList(request);
                request.getRequestDispatcher(CUSTOMERS_JSP).forward(request, response);
                log.trace("forward to customers.jsp with all customers ");
            } catch (SQLException e) {
                e.printStackTrace();
                log.error("something went wrong");
                response.sendError(404);
            }
        }
    }

    /**
     * doPost method - allows admin to block/unblock customer
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();

        ResourceBundle resourceBundle = ResourceBundle.getBundle("text",
                new Locale((String) request.getSession().getAttribute("lang")));
        UserService userService = AppContext.getInstance().getUserService();

        String block = resourceBundle.getString(BLOCK);
        String unblock = resourceBundle.getString(UNBLOCK);


        String action = request.getParameter("action");
        if (action != null) {
            try {
                int id = Integer.parseInt(request.getParameter("id"));
                User user = userService.getUserById(id);
                if (action.equals(block)) {
                    userService.changeBlockStatus(true, id, request);
                    session.setAttribute("errorMessage", "USER_SUCCESSFULLY_BLOCKED");
                    log.trace("user successfully blocked");


                    new MailSender(user, MAIL_BAN_MESSAGE, MAIL_BAN_SUBJECT).start();
                    log.trace("appropriate mail was send to user");

                } else if (action.equals(unblock)) {
                    userService.changeBlockStatus(false, id, request);
                    session.setAttribute("errorMessage", "USER_SUCCESSFULLY_UNBLOCKED");
                    log.trace("user successfully unblocked");

                    new MailSender(user, MAIL_UNBAN_MESSAGE, MAIL_UNBAN_SUBJECT).start();
                    log.trace("appropriate mail was send to user");
                }
                response.sendRedirect(CUSTOMERS);
            } catch (SQLException e) {
                e.getMessage();
                response.sendError(404);
            }
        }
    }

}
