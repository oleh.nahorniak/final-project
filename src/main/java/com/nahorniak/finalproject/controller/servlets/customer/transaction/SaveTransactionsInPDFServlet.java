package com.nahorniak.finalproject.controller.servlets.customer.transaction;

import com.nahorniak.finalproject.DAO.DBConnection;
import com.nahorniak.finalproject.DAO.entity.Transaction;
import com.nahorniak.finalproject.service.TransactionService;
import com.nahorniak.finalproject.service.context.AppContext;
import com.nahorniak.finalproject.util.PDF.TransactionPDFBuilder;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.List;

/**
 * SaveTransactionsInPDFServlet - servlet to save all customer transactions in pdf format .
 *
 * @author Oleh Nahorniak.
 */
@WebServlet(name = "saveTransactionsInPDF", value = "/saveTransactionsInPDF")
public class SaveTransactionsInPDFServlet extends HttpServlet {

    /**
     * doGet method - sendRedirect to error-page
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendError(404);
    }

    /**
     * doPost method - allows to save all customer transactions in one pdf
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DBConnection dbConnection = DBConnection.getInstance();
        TransactionService transactionService = AppContext.getInstance().getTransactionService();
        HttpSession session = request.getSession();

        String lang = (String) session.getAttribute("lang");
        int userId = (int) session.getAttribute("userId");

        String sortBy = request.getParameter("sortBy");
        String order = request.getParameter("order");

        String orderBy = " order by id asc ";


        if (sortBy != null && !sortBy.isBlank()
                && order != null && !order.isBlank()) {
            orderBy = " order by " + sortBy + " " + order + " ";
        }

        List<Transaction> transactions = transactionService.getAllByUser(userId, orderBy);

        new TransactionPDFBuilder().openPDF(request, response, transactions);
    }
}
