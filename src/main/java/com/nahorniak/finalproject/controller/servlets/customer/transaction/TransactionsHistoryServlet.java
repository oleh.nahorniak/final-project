package com.nahorniak.finalproject.controller.servlets.customer.transaction;

import com.nahorniak.finalproject.service.TransactionService;
import com.nahorniak.finalproject.service.context.AppContext;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

import static com.nahorniak.finalproject.util.constants.Path.TRANSACTIONS;
import static com.nahorniak.finalproject.util.constants.Path.TRANSACTIONS_JSP;

/**
 * TransactionsHistoryServlet - get all customer transactions from database ,set as request attribute
 *
 * @author Oleh Nahorniak.
 */
@WebServlet(name = "transactionsHistory", value = "/transactionsHistory")
public class TransactionsHistoryServlet extends HttpServlet {

    /**
     * doGet method get all customer transactions from database, set as request attribute and forwards to transactionsHistory.jsp
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String page = request.getParameter("page");
        if (page != null && !page.matches("[0-9]+")) {
            response.sendRedirect(TRANSACTIONS + "?page=1");
        } else {
            TransactionService transactionService = AppContext.getInstance().getTransactionService();
            transactionService.getAll(request);
            request.getRequestDispatcher(TRANSACTIONS_JSP).forward(request, response);
        }
    }

    /**
     * doPost method do the same as doGet method
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

}
