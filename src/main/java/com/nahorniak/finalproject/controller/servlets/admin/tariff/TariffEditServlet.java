package com.nahorniak.finalproject.controller.servlets.admin.tariff;

import com.nahorniak.finalproject.DAO.DBConnection;
import com.nahorniak.finalproject.DAO.TariffServicesDAO;
import com.nahorniak.finalproject.DAO.entity.Service;
import com.nahorniak.finalproject.DAO.entity.TariffServices;
import com.nahorniak.finalproject.validation.TariffValidation;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.nahorniak.finalproject.util.constants.Path.*;

/**
 * TariffEditServlet - servlet to perform editing chosen tariff.
 *
 * @author Oleh Nahorniak.
 */

@WebServlet(name = "edit", value = "/tariffEdit")
public class TariffEditServlet extends HttpServlet {
    private static final Logger log = Logger.getLogger(TariffEditServlet.class);

    /**
     * doGet method set tariff fields to perform editing
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        DBConnection dbConnection = DBConnection.getInstance();
        TariffServicesDAO tariffServicesDAO = TariffServicesDAO.getInstance();
        String tariffIdParam = request.getParameter("id");
        if (tariffIdParam == null || !tariffIdParam.matches("^\\d+$")) {
            response.sendError(404);
            log.trace("user was redirected to error-page because request param that contained tariffId was incorrect");
        } else {
            try (Connection connection = dbConnection.getConnection()) {
                String lang = (String) session.getAttribute("lang");
                System.out.println(lang);
                int tariff_id = Integer.parseInt(tariffIdParam);
                TariffServices tariffServices = tariffServicesDAO.getTariffWithServices(lang, tariff_id, connection);
                Set<Service> allServices = tariffServicesDAO.getAllServices(lang, connection);
                String additionalTitle = tariffServicesDAO.getAdditionalTariffTitle(tariff_id, lang, connection);
                request.setAttribute("tariffServices", tariffServices);
                request.setAttribute("allServices", allServices);
                request.setAttribute("additionalTitle", additionalTitle);
                log.trace("[tariffServices,allServices,additionalTitle] attributes were set successfully into request");
            } catch (SQLException e) {
                e.printStackTrace();
                response.sendError(404);
            }
            request.getRequestDispatcher(TARIFF_EDIT_JSP).forward(request, response);
            log.trace("user was forwarded to tariffEdit jsp page ---> " + TARIFF_EDIT_JSP);
        }
    }

    /**
     * doPost method that allows to edit tariff, after editing sendRedirect to page with tariffs with appropriate message
     * <p>success - Tariff successfully edited</p>
     * <p>fail - Validation error</p>
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("id");

        HttpSession session = request.getSession();

        String titleEn = request.getParameter("titleEn");
        String titleUk = request.getParameter("titleUk");
        String code = request.getParameter("code");
        Double price = Double.parseDouble(request.getParameter("price"));
        String[] services = request.getParameterValues("services");

        log.trace("Tariff validation starts...");
        if (services != null && TariffValidation.isValid(titleEn, titleUk, code, price)) {
            log.trace("Tariff validation finished successfully");
            int tariff_id = Integer.parseInt(id);


            List<Integer> services_id = Stream.of(services).map(x -> Integer.parseInt(x)).collect(Collectors.toList());
            DBConnection dbConnection = DBConnection.getInstance();
            TariffServicesDAO tariffServicesDAO = TariffServicesDAO.getInstance();


            try (Connection connection = dbConnection.getConnection()) {
                tariffServicesDAO.updateTariff(tariff_id, code, price, titleEn, titleUk, services_id, connection);
                log.trace("Tariff was edited successfully");
                session.setAttribute("errorMessage", "TARIFF_SUCCESSFULLY_EDITED");

                log.trace("user sendRedirected to page with tariffs with appropriate message");
                response.sendRedirect(TARIFFS);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            log.error("Tariff validation error...");
            session.setAttribute("errorMessage", "VALIDATION_ERROR");
            response.sendRedirect(TARIFF_EDIT + "?id=" + id);
        }


    }

}
