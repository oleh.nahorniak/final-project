package com.nahorniak.finalproject.controller.servlets.common;

import com.nahorniak.finalproject.DAO.DBConnection;
import com.nahorniak.finalproject.DAO.entity.User;
import com.nahorniak.finalproject.service.UserService;
import com.nahorniak.finalproject.service.context.AppContext;
import com.nahorniak.finalproject.util.Encryption;
import com.nahorniak.finalproject.validation.UserValidation;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import static com.nahorniak.finalproject.util.constants.Path.PROFILE;

/**
 * ChangePasswordServlet - servlet to change current password .
 *
 * @author Oleh Nahorniak.
 */
@WebServlet(name = "changePassword", value = "/changePassword")
public class ChangePasswordServlet extends HttpServlet {

    /**
     * doGet method - redirect to error-page
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendError(404);
    }

    /**
     * doPost method - allows user to change his current password
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        DBConnection dbConnection = DBConnection.getInstance();
        UserService userService = AppContext.getInstance().getUserService();

        String currentPassword = request.getParameter("currentPassword");
        String newPassword = request.getParameter("newPassword");
        String confirmPassword = request.getParameter("confirmPassword");

        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");

        String password = user.getPassword();

        System.out.println(password + "   " + Encryption.md5(currentPassword));

        if (!password.equals(Encryption.md5(currentPassword))) {
            session.setAttribute("error", "PASSWORD_IS_NOT_CORRECT");
            response.sendRedirect(PROFILE);
        } else {

            if (!newPassword.equals(confirmPassword)) {
                session.setAttribute("error", "NEW_PASSWORD_NOT_CONFIRMED");
                response.sendRedirect(PROFILE);
            } else {
                user.setPassword(newPassword);
                if (UserValidation.isValidUser(user)) {
                    String encryptedPassword = Encryption.md5(newPassword);
                    user.setPassword(encryptedPassword);

                    try (Connection connection = dbConnection.getConnection()) {
                        userService.update(user, connection);
                        session.setAttribute("user", user);
                        session.setAttribute("errorMessage", "PASSWORD_SUCCESSFULLY_CHANGED");
                        response.sendRedirect(PROFILE);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                } else {
                    session.setAttribute("errorMessage", "VALIDATION_ERROR");
                    response.sendRedirect(PROFILE);
                }

            }
        }

    }
}
