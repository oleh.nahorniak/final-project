package com.nahorniak.finalproject.controller.servlets.admin;

import com.nahorniak.finalproject.DAO.DBConnection;
import com.nahorniak.finalproject.DAO.entity.User;
import com.nahorniak.finalproject.service.UserService;
import com.nahorniak.finalproject.service.context.AppContext;
import com.nahorniak.finalproject.util.Encryption;
import com.nahorniak.finalproject.validation.UserValidation;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import static com.nahorniak.finalproject.util.constants.Path.*;

/**
 * Register Servlet.
 *
 * @author Oleh Nahorniak
 */
@WebServlet(name = "register", value = "/register")
public class RegisterServlet extends HttpServlet {
    private static final Logger log = Logger.getLogger(RegisterServlet.class);

    public static final String USER_CREATED_SUCCESSFULLY = "USER_CREATED_SUCCESSFULLY";
    public static final String NOT_UNIQUE_EMAIL = "NOT_UNIQUE_EMAIL";
    public static final String NOT_UNIQUE_LOGIN = "NOT_UNIQUE_LOGIN";

    /**
     * doGet method - forward to register.jsp
     *
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        req.getRequestDispatcher(REGISTER_JSP).forward(req, resp);
        log.trace("admin send redirected to register page with appropriate message");
    }

    /**
     * doPost method - allows admin to register new customer
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();

        String login = request.getParameter("login");
        String password = request.getParameter("password");
        String firstName = request.getParameter("fName");
        String lastName = request.getParameter("lName");
        String role = request.getParameter("role");
        String email = request.getParameter("email");
        String phoneNumber = request.getParameter("phone");
        String country = request.getParameter("country");
        String city = request.getParameter("city");
        String street = request.getParameter("street");

        User.Builder builder = new User.Builder();


        User newUser = builder.withLogin(login)
                .withPassword(password)
                .withFirstName(firstName)
                .withLastName(lastName)
                .withRole(role)
                .withEmail(email)
                .withPhoneNumber(phoneNumber)
                .withCountry(country)
                .withCity(city)
                .withStreet(street)
                .build();

        if (UserValidation.isValidUser(newUser)) {

            String encryptedPassword = Encryption.md5(password);

            newUser.setPassword(encryptedPassword);

            DBConnection dbConnection = DBConnection.getInstance();
            UserService userService = AppContext.getInstance().getUserService();

            StringBuffer message = new StringBuffer("");

            try (Connection connection = dbConnection.getConnection()) {
                userService.create(newUser, connection);
                message.append(USER_CREATED_SUCCESSFULLY);
                List<User> users = userService.getAll(connection);
                request.getSession().setAttribute("customers", users);
            } catch (SQLException e) {
                String msg = e.getMessage();
                if (msg.contains("login"))
                    message.append(NOT_UNIQUE_LOGIN);
                else if (msg.contains("email"))
                    message.append(NOT_UNIQUE_EMAIL);
                else response.sendError(404);
            }

            session.setAttribute("errorMessage", message);
            log.trace("admin send redirected to register page with appropriate message");
            response.sendRedirect(REGISTER);

        } else {
            session.setAttribute("errorMessage", "VALIDATION_ERROR");
            log.trace("admin send redirected to register page with validation error message");
            response.sendRedirect(REGISTER);
        }
    }
}
