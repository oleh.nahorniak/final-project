package com.nahorniak.finalproject.controller.servlets.outOfControl;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

import static com.nahorniak.finalproject.util.constants.Path.LOGIN;

/**
 * DeleteAttributes - servlet to delete attributes from session
 *
 * @author Oleh Nahorniak.
 */
@WebServlet(name = "deleteAttributes", value = "/deleteAttributes")
public class DeleteAttributes extends HttpServlet {
    /**
     * doGet method - sendRedirect to error-page
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendError(404);
    }

    /**
     * doPost method to delete attributes from session
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();
        String action = request.getParameter("action");

        if (action.equals("changePass")) {
            session.removeAttribute("changePassword");
        } else if (action.equals("code")) {
            session.removeAttribute("code");
        }

        response.sendRedirect(LOGIN);
    }
}
