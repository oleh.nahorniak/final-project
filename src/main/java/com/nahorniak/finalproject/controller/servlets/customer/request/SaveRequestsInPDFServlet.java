package com.nahorniak.finalproject.controller.servlets.customer.request;

import com.nahorniak.finalproject.DAO.DBConnection;
import com.nahorniak.finalproject.DAO.entity.RequestTariff;
import com.nahorniak.finalproject.service.RequestService;
import com.nahorniak.finalproject.service.context.AppContext;
import com.nahorniak.finalproject.util.PDF.RequestPDFBuilder;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.List;

/**
 * SaveRequestsInPDFServlet - servlet to save all customer requests in pdf format .
 *
 * @author Oleh Nahorniak.
 */
@WebServlet(name = "saveRequestsInPDF", value = "/saveRequestsInPDF")
public class SaveRequestsInPDFServlet extends HttpServlet {

    /**
     * doGet method - sendRedirect to error-page
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendError(404);
    }

    /**
     * doPost method - allows to save all customer requests in one pdf
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DBConnection dbConnection = DBConnection.getInstance();
        RequestService requestService = AppContext.getInstance().getRequestService();
        HttpSession session = request.getSession();
        String lang = (String) session.getAttribute("lang");
        int userId = (int) session.getAttribute("userId");

        String sortBy = request.getParameter("sortBy");
        String order = request.getParameter("order");

        String orderBy = " order by id asc ";


        if (sortBy != null && !sortBy.isBlank()
                && order != null && !order.isBlank()) {
            orderBy = " order by " + sortBy + " " + order + " ";
        }

        List<RequestTariff> requests = requestService.getAll(lang, userId, orderBy);

        new RequestPDFBuilder().openPDF(request, response, requests);
    }
}
