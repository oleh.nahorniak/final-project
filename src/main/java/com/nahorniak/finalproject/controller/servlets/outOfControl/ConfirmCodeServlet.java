package com.nahorniak.finalproject.controller.servlets.outOfControl;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

import static com.nahorniak.finalproject.util.constants.Path.LOGIN;

/**
 * ConfirmCodeServlet - servlet to confirm code from email to change password
 *
 * @author Oleh Nahorniak.
 */
@WebServlet(name = "confirmCode", value = "/confirmCode")
public class ConfirmCodeServlet extends HttpServlet {
    /**
     * doGet method - sendRedirect to error-page
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendError(404);
    }

    /**
     * doPost method to confirm code from email to change password
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();

        String inputCode = request.getParameter("code");
        String code = (String) session.getAttribute("code");

        if (inputCode.equals(code)) {
            session.setAttribute("changePassword", true);
            session.removeAttribute("code");
        } else {
            session.setAttribute("codeError", "CODE_IS_NOT_CORRECT");
        }
        response.sendRedirect(LOGIN);
    }
}
