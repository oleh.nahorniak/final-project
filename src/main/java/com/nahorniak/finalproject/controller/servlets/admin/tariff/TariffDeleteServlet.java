package com.nahorniak.finalproject.controller.servlets.admin.tariff;

import com.nahorniak.finalproject.DAO.DBConnection;
import com.nahorniak.finalproject.DAO.TariffServicesDAO;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import static com.nahorniak.finalproject.util.constants.Path.*;

/**
 * TariffDeleteServlet - servlet to perform deleting chosen tariff.
 *
 * @author Oleh Nahorniak.
 */

@WebServlet(name = "tariffDelete", value = "/tariffDelete")
public class TariffDeleteServlet extends HttpServlet {
    private static final Logger log = Logger.getLogger(TariffDeleteServlet.class);

    /**
     * doGet method , send user to page with tariffs
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendRedirect(TARIFFS + "?page=1");
        log.trace("user was sendRedirected to tariffs page when trying to get tariffDelete page");
    }


    /**
     * doPost method - delete chosen tariff with appropriate message
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String paramId = request.getParameter("id");
        HttpSession session = request.getSession();
        DBConnection dbConnection = DBConnection.getInstance();
        TariffServicesDAO tariffServicesDAO = TariffServicesDAO.getInstance();
        if (paramId != null) {
            int id = Integer.parseInt(paramId);
            try (Connection connection = dbConnection.getConnection()) {
                tariffServicesDAO.deleteTariff(id, connection);
                session.setAttribute("errorMessage", "TARIFF_SUCCESSFULLY_DELETED");
                log.trace("tariff deleted successfully");
                response.sendRedirect(TARIFFS + "?page=1");
            } catch (SQLException e) {
                e.printStackTrace();
                log.trace("something went wrong -->" + e.getMessage());
                response.sendError(404);
            }
        }
    }
}
