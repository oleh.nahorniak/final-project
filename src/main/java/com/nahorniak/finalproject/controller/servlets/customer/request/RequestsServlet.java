package com.nahorniak.finalproject.controller.servlets.customer.request;

import com.nahorniak.finalproject.service.RequestService;
import com.nahorniak.finalproject.service.context.AppContext;
import com.nahorniak.finalproject.service.impl.RequestServiceImpl;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

import static com.nahorniak.finalproject.util.constants.Path.REQUESTS;
import static com.nahorniak.finalproject.util.constants.Path.REQUESTS_JSP;

/**
 * RequestsServlet - servlet get all customer requests from database ,set as request attribute and forwards to requests.jsp.
 *
 * @author Oleh Nahorniak.
 */

@WebServlet(name = "requests", value = "/requests")
public class RequestsServlet extends HttpServlet {

    /**
     * doGet method get all customer requests from database, set as request attribute and forwards to requests.jsp
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String page = request.getParameter("page");
        if (page != null && !page.matches("[0-9]+")) {
            response.sendRedirect(REQUESTS + "?page=1");
        } else {
            RequestService requestService = AppContext.getInstance().getRequestService();
            requestService.getAllByUser(request);
            request.getRequestDispatcher(REQUESTS_JSP).forward(request, response);
        }
    }

    /**
     * do the same as doGet
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
