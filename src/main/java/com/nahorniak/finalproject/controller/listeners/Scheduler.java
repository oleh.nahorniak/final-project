package com.nahorniak.finalproject.controller.listeners;

import com.nahorniak.finalproject.service.TransactionService;
import com.nahorniak.finalproject.service.context.AppContext;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


/**
 * Scheduler for daily payment.
 *
 * @author Oleh Nahorniak.
 */

@WebListener
public class Scheduler implements ServletContextListener {

    private static final Logger log = Logger.getLogger(Scheduler.class);

    private ScheduledExecutorService scheduler;
    private TransactionService transactionService = AppContext.getInstance().getTransactionService();

    public Scheduler() {
    }

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        log.debug("Scheduler initialization starts");
        scheduler = Executors.newSingleThreadScheduledExecutor();
        scheduler.scheduleAtFixedRate(new DailyTask(), 0, 1, TimeUnit.DAYS);
        log.debug("Scheduler initialization finished");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        log.debug("Scheduler destruction starts");
        scheduler.shutdownNow();
        log.debug("Scheduler destruction finished");
    }

    /**
     * Runnable class DailyTask to perform payment.
     */

    private class DailyTask implements Runnable {
        @Override
        public void run() {
            transactionService.dailyPaymentAndBlock();
            log.trace("Payment for services");
        }

    }
}
