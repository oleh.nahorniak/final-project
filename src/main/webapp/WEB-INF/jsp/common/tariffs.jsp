<%@ page import="java.time.LocalDate" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tags" %>
<%@ taglib prefix="dt" uri="myTags" %>
<fmt:setLocale value="${lang}" scope="session"/>
<fmt:setBundle basename="text"/>
<html>
<head>
    <title>Internet Service Provider</title>
    <style>
        <%@include file="../../../style/tariffs.css" %>
    </style>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

</head>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
      integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<body>
<header>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="/application"><fmt:message key="ISP"/></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarNav" style="justify-content: right">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="/application"><fmt:message key="Home"/></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="about"><fmt:message key="About"/></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="contacts" style="margin-right: 15px">
                            <fmt:message key="Contacts"/>
                        </a>
                    </li>
                    <c:if test="${role!=null}">
                        <c:if test="${role.equalsTo('CUSTOMER')}">
                            <li class="nav-item">
                                <a class="button-link" href="topUpAccount">
                                    <button type="button" class="btn btn-outline-warning"><fmt:message
                                            key="Balance"/>: &nbsp<dt:numberTag format="${lang}"
                                                                                number="${user.balance}"/>&nbsp$
                                    </button>
                                </a>
                            </li>
                        </c:if>
                        <li class="nav-item">
                            <a class="button-link" href="logout">
                                <button type="button" class="btn btn-outline-primary"
                                        style="width: 80px; margin-right: 15px; margin-left: 15px">
                                    <fmt:message key="Logout"/>
                                </button>
                            </a>
                        </li>
                    </c:if>
                    <c:if test="${role==null}">
                        <li class="nav-item">
                            <a class="button-link" href="login">
                                <button type="button" class="btn btn-outline-primary"
                                        style="width: 80px; margin-right: 15px;">
                                    <fmt:message key="Login"/>
                                </button>
                            </a>
                        </li>
                    </c:if>
                    <li>
                        <select id="lang" onchange="setLanguage()">
                            <c:if test="${lang == 'en'}">
                                <option>en</option>
                                <option>uk</option>
                            </c:if>
                            <c:if test="${lang == 'uk'}">
                                <option>uk</option>
                                <option>en</option>
                            </c:if>
                        </select>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>

<jsp:include page="../message.jsp"></jsp:include>
<section class="middle-container">
    <div class="background-image">
        <div style="display: flex ; justify-content: flex-start; align-items: flex-start">
            <div class="links-container">
                <a class=" button-link
            " href="profile">
                    <button type="button" class="input-button-links" style="width: 12vh">
                        <fmt:message key="Profile"/>
                    </button>
                </a>
                <a class="button-link" href="tariffs">
                    <button type="button" disabled class="input-button-disabled" style="width: 12vh">
                        <fmt:message key="Tariffs"/>
                    </button>
                </a>
                <c:if test="${role.equalsTo('CUSTOMER')}">
                    <a class="button-link" href="topUpAccount">
                        <button type="button" class="input-button-links" style="width: 25vh">
                            <fmt:message key="FILL_UP_BALANCE"/>
                        </button>
                    </a>
                </c:if>
                <c:if test="${role.equalsTo('ADMIN')}">
                    <a class="button-link" href="newTariff">
                        <button type="button" class="input-button-links" style="width: 25vh">
                            <fmt:message key="ADD_NEW_TARIFF"/>
                        </button>
                    </a>
                </c:if>
            </div>

            <div class="sorting">
                <form action="tariffs" method="get">
                    <input type="hidden" name="page" value="1">
                    <div class="tariff-selection">
                        <select name="sortBy" class="form-select" aria-label="Default select example"
                                style="width: 20vh">
                            <c:choose>
                                <c:when test="${param.sortBy == 'title'}">
                                    <option selected value="title"><fmt:message key="SORT_BY_TITLE"/></option>
                                    <option value="price_per_day"><fmt:message key="SORT_BY_PRICE"/></option>
                                </c:when>
                                <c:when test="${param.sortBy == 'price_per_day'}">
                                    <option selected value="price_per_day"><fmt:message key="SORT_BY_PRICE"/></option>
                                    <option value="title"><fmt:message key="SORT_BY_TITLE"/></option>
                                </c:when>
                                <c:otherwise>
                                    <option value="t.id" selected><fmt:message key="SORT_BY"/></option>
                                    <option value="title"><fmt:message key="SORT_BY_TITLE"/></option>
                                    <option value="price_per_day"><fmt:message key="SORT_BY_PRICE"/></option>
                                </c:otherwise>
                            </c:choose>
                        </select>
                    </div>
                    <div class="tariff-selection">
                        <select name="order" class="form-select" aria-label="Default select example"
                                style="width: 20vh">
                            <c:choose>
                                <c:when test="${param.order == 'asc'}">
                                    <option selected value="asc"><fmt:message key="ASC"/></option>
                                    <option value="desc"><fmt:message key="DESC"/></option>
                                </c:when>
                                <c:when test="${param.order == 'desc'}">
                                    <option selected value="desc">DESC</option>
                                    <option value="asc"><fmt:message key="ASC"/></option>
                                </c:when>
                                <c:otherwise>
                                    <option value="asc" selected><fmt:message key="Order"/></option>
                                    <option value="desc"><fmt:message key="DESC"/></option>
                                    <option value="asc"><fmt:message key="ASC"/></option>
                                </c:otherwise>
                            </c:choose>
                        </select>
                    </div>

                    <div class="tariff-selection">
                        <input class="input-button" style="width: 15vh" type="submit" value="<fmt:message key="SORT"/>">
                    </div>
                </form>
                <form action="saveTariffsInPDF" method="post">
                    <input type="hidden" name="sortBy" value="${param.sortBy}">
                    <input type="hidden" name="order" value="${param.order}">
                    <input class="input-button" type="submit" style="margin-left: 1vh; width: 19vh"
                           value="<fmt:message key="SAVE_IN_PDF"/>">
                </form>
            </div>


        </div>


        <div class="tariffs-container container-xxl">
            <c:if test="${tariffServices!=null}">
                <c:forEach var="tariffServices" items="${tariffServices}">


                    <c:set var="tariff" scope="request" value="${tariffServices.tariff}"/>
                    <c:set var="services" scope="request" value="${tariffServices.services}"/>
                    <div class="card-t">
                        <h3 class="title"><c:out value="${tariff.title}"/>
                        </h3>
                        <h3 class="title" style="margin-top: 8vh; font-size: 1rem"><c:out value="${tariff.code}"/>
                        </h3>
                        <div class="bar">
                            <div class="emptybar"></div>
                            <div class="filledbar"></div>
                        </div>
                        <div class="card-text">
                            <h3 class="card-t-text"><fmt:message key="Service_List"/>
                            </h3>
                            <div class="services">
                                <c:forEach var="service" items="${services}">
                                    <li class="card-text">
                                        <c:out value="${service.title}"/>
                                    </li>
                                </c:forEach>
                            </div>
                            <h3 class="card-t-price">
                                <dt:numberTag format="${lang}"
                                              number="${tariff.price}"/>
                                $ &nbsp&nbsp
                                    <fmt:message key="PER_DAY"/></p>
                            </h3>
                            <c:if test="${role.equalsTo('CUSTOMER')}">
                                <div style="height: 5vh ;width: 20vh ;position: absolute; display: flex ; justify-content: center; align-items: center">
                                    <c:if test="${user.isBlocked == false}">

                                        <input type="submit" class="tariff-button-links button-link ss"
                                               value=" <fmt:message key="Connect"/>"
                                               style="background-color: white;"
                                               data-bs-toggle="modal"
                                               data-bs-target="#staticBackdrop<c:out value="${tariff.id}"/>">

                                        <!-- Modal -->


                                    </c:if>
                                    <c:if test="${user.isBlocked == true}">
                                        <input type="submit" class="input-button-disabled button-link"
                                               disabled="disabled"
                                               name="action"
                                               value=" <fmt:message key="Connect"/>"
                                               style="background-color: white">
                                    </c:if>
                                </div>

                            </c:if>
                            <c:if test="${role.equalsTo('ADMIN')}">
                                <form class="tariff-form" action="tariffEdit" method="get">
                                    <input type="submit" class="tariff-button-links button-link"
                                           value="<fmt:message key="Edit"/>"
                                           style="background-color: white ; width: 15vh; margin-left: -3.3vh">
                                    <input type="hidden" name="id" value="${tariff.id}">
                                </form>
                                <form class="tariff-form" action="tariffDelete" method="post">
                                    <input type="hidden" name="id" value="${tariff.id}">
                                    <input type="submit" class="tariff-button-links button-link"
                                           value="<fmt:message key="Delete"/>"
                                           style="background-color: white ;width: 13vh;">
                                </form>

                            </c:if>
                        </div>
                    </div>

                    <c:if test="${role.equalsTo('CUSTOMER')}">
                        <div class="modal fade" id="staticBackdrop<c:out value="${tariff.id}"/>"
                             data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel"
                             aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="staticBackdropLabel">
                                            <c:out value="${tariff.title}"/>
                                        </h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                aria-label="Close"></button>
                                    </div>
                                    <form action="connect" method="POST">
                                        <div class="modal-body">
                                            <div class="icon-container">
                                                <img src="images/network.png" width="100px" height="100px"/>
                                                <p><fmt:message key="Service_List"/></p>
                                            </div>
                                            <div class="text-container">
                                                <div class="text">
                                                    <div class="service-list">
                                                        <c:forEach var="service" items="${services}">
                                                            <li class="card-text">
                                                                <c:out value="${service.title}"/>
                                                            </li>
                                                        </c:forEach>
                                                    </div>
                                                    <div>
                                                        <label class="price-label" for="price"><fmt:message
                                                                key="PRICE_PER_DAY"/>:</label>
                                                        <span id="price"><dt:numberTag format="${lang}"
                                                                                       number="${tariff.price}"/>$</span>
                                                    </div>
                                                    <div>
                                                        <label id="startDate-label" for="startDate">
                                                            <fmt:message key="START_DATE"/>
                                                        </label>

                                                        <input id="startDate" type="date" style="width: 20vh"
                                                               value="<%= LocalDate.now()%>"
                                                               class="form-control" disabled>
                                                    </div>
                                                    <div class="end-date">
                                                        <label for="date">
                                                            <fmt:message key="END_DATE"/>
                                                        </label>
                                                        <input name="endDate" id="date" type="date" style="width: 20vh"
                                                               class="form-control"
                                                               min="<%= LocalDate.now()%>"
                                                               required
                                                        >
                                                        <input type="hidden" name="tariffId"
                                                               value="${tariff.id}">
                                                    </div>
                                                    <div>
                                                        <div class="g-recaptcha"
                                                             data-sitekey="6LdK3CAfAAAAADxB5G2f7_6mCqwjJn16qCKJf-T9"></div>
                                                        <br/>
                                                        <c:if test="${captchaError != null}">
                                                            <div>
                                                            <span>
                                                                <p class="inp-error">
                                                                    <fmt:message key="${captchaError}"/>
                                                                </p>
                                                            </span>
                                                            </div>
                                                        </c:if>
                                                    </div>
                                                    <input type="submit" class="btn btn-primary" style="width: 40vh"
                                                           value="<fmt:message key="Connect"/>">
                                                </div>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </c:if>


                </c:forEach>
            </c:if>

        </div>
        <nav aria-label=" Page navigation example">
            <ul class="pagination justify-content-center">

                <c:choose>
                    <c:when test="${param.page == 1}">
                        <li class="page-item disabled">
                            <a class="page-link gr" href="tariffs?page=1"><fmt:message key="PREVIOUS"/></a>
                        </li>
                    </c:when>
                    <c:otherwise>
                        <c:choose>
                            <c:when test="${param.sortBy != null && param.order != null}">
                                <li class="page-item">
                                    <a class="page-link dark"
                                       href="tariffs?page=${param.page-1}&sortBy=${param.sortBy}&order=${param.order}"><fmt:message
                                            key="PREVIOUS"/></a>
                                </li>
                            </c:when>
                            <c:otherwise>
                                <li class="page-item">
                                    <a class="page-link dark" href="tariffs?page=${param.page-1}"><fmt:message
                                            key="PREVIOUS"/></a>
                                </li>
                            </c:otherwise>
                        </c:choose>

                    </c:otherwise>
                </c:choose>


                <c:forEach begin="1" var="i" end="${maxPages}">
                    <c:choose>
                        <c:when test="${param.page == i}">
                            <c:choose>
                                <c:when test="${param.sortBy != null && param.order != null}">
                                    <li class="page-item active"><a class="page-link item white"
                                                                    href="tariffs?page=${i}&sortBy=${param.sortBy}&order=${param.order}">${i}</a>
                                    </li>
                                </c:when>
                                <c:otherwise>
                                    <li class="page-item active"><a class="page-link item white"
                                                                    href="tariffs?page=${i}">${i}</a>
                                    </li>
                                </c:otherwise>
                            </c:choose>
                        </c:when>
                        <c:otherwise>
                            <c:choose>
                                <c:when test="${param.sortBy != null && param.order != null}">
                                    <li class="page-item"><a class="page-link item dark"
                                                             href="tariffs?page=${i}&sortBy=${param.sortBy}&order=${param.order}">${i}</a>
                                    </li>
                                </c:when>
                                <c:otherwise>
                                    <li class="page-item"><a class="page-link item dark"
                                                             href="tariffs?page=${i}">${i}</a>
                                    </li>
                                </c:otherwise>
                            </c:choose>
                        </c:otherwise>
                    </c:choose>

                </c:forEach>

                <c:choose>
                    <c:when test="${param.page == maxPages}">
                        <li class="page-item disabled">
                            <a class="page-link gr" href="tariffs?page=${param.page + 1}"><fmt:message key="NEXT"/></a>
                        </li>

                    </c:when>
                    <c:otherwise>

                        <c:choose>
                            <c:when test="${param.sortBy != null && param.order != null}">

                                <li class="page-item">
                                    <a class="page-link dark"
                                       href="tariffs?page=${param.page + 1}&sortBy=${param.sortBy}&order=${param.order}"><fmt:message
                                            key="NEXT"/></a>
                                </li>

                            </c:when>
                            <c:otherwise>
                                <li class="page-item">
                                    <a class="page-link dark" href="tariffs?page=${param.page + 1}"><fmt:message
                                            key="NEXT"/></a>
                                </li>
                            </c:otherwise>

                        </c:choose>

                    </c:otherwise>
                </c:choose>


            </ul>
        </nav>
    </div>
</section>
<c:if test="${captchaError != null && tariffId != null}">
    <script src="//ajax.aspnetcdn.com/ajax/jQuery/jquery-2.1.1.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(window).load(function () {
            $('#staticBackdrop${tariffId}').modal('show');
        });
    </script>
    <% session.removeAttribute("captchaError");%>
    <% session.removeAttribute("tariffId");%>

</c:if>
<jsp:include page="../templates/footer.jsp"/>
<script type="text/javascript">
    <%@include file="../../../js/languageUtil.js" %>
</script>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous"></script>

</body>
</html>
