<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="dt" uri="myTags" %>
<fmt:setLocale value="${lang}" scope="session"/>
<fmt:setBundle basename="text"/>
<html>
<head>
    <title>Internet Service Provider</title>
    <style>
        <%@include file="../../../style/userEdit.css" %>
    </style>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

</head>
<body>

<header>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="/application"><fmt:message key="ISP"/></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav" style="justify-content: right">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="/application"><fmt:message key="Home"/></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/application/about"><fmt:message key="About"/></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/application/contacts" style="margin-right: 15px">
                            <fmt:message key="Contacts"/></a>
                    </li>
                    <c:if test="${role!=null}">
                        <c:if test="${role.equalsTo('CUSTOMER')}">
                            <li class="nav-item">
                                <a class="button-link" href="topUpAccount">
                                    <button type="button" class="btn btn-outline-warning"><fmt:message
                                            key="Balance"/>: &nbsp<dt:numberTag format="${lang}"
                                                                                number="${user.balance}"/>&nbsp$
                                    </button>
                                </a>
                            </li>
                        </c:if>
                        <li class="nav-item">
                            <a class="button-link" href="logout">
                                <button type="button" class="btn btn-outline-primary"
                                        style="width: 80px; margin-right: 15px; margin-left: 15px">
                                    <fmt:message key="Logout"/>
                                </button>
                            </a>
                        </li>
                    </c:if>
                    <li>
                        <select id="lang" onchange="setLanguage()">
                            <c:if test="${lang == 'en'}">
                                <option>en</option>
                                <option>uk</option>
                            </c:if>
                            <c:if test="${lang == 'uk'}">
                                <option>uk</option>
                                <option>en</option>
                            </c:if>
                        </select>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>
<jsp:include page="../message.jsp"></jsp:include>

<section class="middle-container">
    <div class="background-image">
        <div class="links-container">
            <a class="button-link" href="profile">
                <button type="button" class="input-button-links">
                    <fmt:message key="Profile"/>
                </button>
            </a>

            <a class="button-link" href="userEdit">
                <button type="button" disabled class="input-button-disabled">
                    <fmt:message key="PROFILE_EDIT"/>
                </button>
            </a>

        </div>
        <div class="person-container">
            <div class="mdl">
                <form action="userEdit" method="post">
                    <div class="t-container">
                        <p class="t-title">
                            <fmt:message key="PROFILE_EDIT"/>
                        </p>
                    </div>
                    <div class="form-container">
                        <div class="form">
                            <div class="center">
                                <input class="inp" type="text" name="fName" value="${user.getFirstName()}"
                                       placeholder="<fmt:message key="FIRST_NAME"/>*"
                                       pattern="^[A-ZА-ЯІЄЇ]{1}([a-zа-яіїє]{2,15})"
                                       oninvalid="this.setCustomValidity(`<fmt:message key="INVALID_FIRST_NAME"/>`)"
                                       oninput="this.setCustomValidity('')"
                                       required/>
                            </div>
                            <div class="center">
                                <input class="inp" type="text" name="lName" value="${user.getLastName()}"
                                       placeholder="<fmt:message key="LAST_NAME"/>*"
                                       pattern="^[A-ZА-ЯІЄЇ]{1}([a-zа-яіїє]{2,15})"
                                       oninvalid="this.setCustomValidity(`<fmt:message key="INVALID_LAST_NAME"/>`)"
                                       oninput="this.setCustomValidity('')"
                                       required>
                            </div>
                            <div class="center">
                                <input class="inp" type="email" name="email" value="${user.getEmail()}" required
                                       placeholder="<fmt:message key="Email"/>*">
                            </div>
                            <div class="center">
                                <input class="inp" type="tel" name="phone" value="${user.getPhoneNumber()}"
                                       pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}"
                                       oninvalid="this.setCustomValidity(`<fmt:message key="INVALID_PHONE_NUMBER"/>`)"
                                       oninput="this.setCustomValidity('')"
                                       placeholder="<fmt:message key="PHONE_NUMBER"/>   111-111-1111"
                                       required>
                            </div>
                            <c:if test="${role.equalsTo('CUSTOMER')}">
                                <div class="center">
                                    <input class="inp" type="text" name="country" value="${user.getCountry()}"
                                           pattern="([A-ZА-ЯІЇЄ]([a-zа-яіїє]{1,15}) ?)+"
                                           oninvalid="this.setCustomValidity(`<fmt:message key="INVALID_COUNTRY"/>`)"
                                           oninput="this.setCustomValidity('')"
                                           placeholder="<fmt:message key="Country"/>">
                                </div>
                                <div class="center">
                                    <input class="inp" type="text" name="city" value="${user.getCity()}"
                                           pattern="([A-ZА-ЯІЇЄ]([a-zа-яіїє]{1,15})( |-)?)+"
                                           oninvalid="this.setCustomValidity(`<fmt:message key="INVALID_CITY"/>`)"
                                           oninput="this.setCustomValidity('')"
                                           placeholder="<fmt:message key="City"/>">
                                </div>
                                <div class="center">
                                    <input class="inp" type="text" name="street" value="${user.getStreet()}"
                                           pattern="(\w+\s*(road|street|square|rd|st|sq), \w{1,5})"
                                           oninvalid="this.setCustomValidity(`<fmt:message key="INVALID_STREET"/>`)"
                                           oninput="this.setCustomValidity('')"
                                           placeholder="<fmt:message key="Street"/>">
                                </div>
                            </c:if>
                            <div class="center"
                                 style="display: flex; justify-content: center;align-items: center; margin-top: 2vh">
                                <input class="button-35" type="submit" value="<fmt:message key="Edit"/>">
                            </div>
                        </div>

                    </div>
                </form>
            </div>

        </div>
        <p class="error-message">${param.message}</p>
    </div>

</section>

<jsp:include page="../templates/footer.jsp"/>
<script type="text/javascript">
    <%@include file="../../../js/languageUtil.js" %>
</script>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous"></script>
</body>
</html>
