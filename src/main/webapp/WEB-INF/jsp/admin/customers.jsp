<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="dt" uri="myTags" %>
<fmt:setLocale value="${lang}" scope="session"/>
<fmt:setBundle basename="text"/>
<html>
<head>
    <title>Internet Service Provider</title>
    <style>
        <%@include file="../../../style/customers.css" %>
    </style>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<body>
<header>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="/application"><fmt:message key="ISP"/></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarNav" style="justify-content: right">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="/application"><fmt:message key="Home"/></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="about"><fmt:message key="About"/></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="contacts" style="margin-right: 15px">
                            <fmt:message key="Contacts"/>
                        </a>
                    </li>
                    <c:if test="${role!=null}">

                        <li class="nav-item">
                            <a class="button-link" href="logout">
                                <button type="button" class="btn btn-outline-primary"
                                        style="width: 80px; margin-right: 15px;">
                                    <fmt:message key="Logout"/>
                                </button>
                            </a>
                        </li>
                    </c:if>

                    <li>
                        <select id="lang" onchange="setLanguage()">
                            <c:if test="${lang == 'en'}">
                                <option>en</option>
                                <option>uk</option>
                            </c:if>
                            <c:if test="${lang == 'uk'}">
                                <option>uk</option>
                                <option>en</option>
                            </c:if>
                        </select>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>

<jsp:include page="../message.jsp"></jsp:include>

<section class="middle-container">
    <div class="background-image">
        <div style="display: flex ; justify-content: flex-start; align-items: flex-start">
            <div class="links-container" style="width: 80.5vh">
                <a class="button-link" href="profile">
                    <button type="button" class="input-button-links">
                        <fmt:message key="Profile"/>
                    </button>
                </a>
                <a class="button-link" href="customers">
                    <button type="button" disabled class="input-button-disabled">
                        <fmt:message key="Customers"/>
                    </button>
                </a>

                <a class="button-link" href="register">
                    <button type="button" class="input-button-links">
                        <fmt:message key="ADD_NEW_USER"/>
                    </button>
                </a>
            </div>
            <div class="sorting">
                <form action="customers" method="get">
                    <input type="hidden" name="page" value="1">
                    <div class="tariff-selection">
                        <select name="sortBy" class="form-select" aria-label="Default select example">
                            <c:choose>
                                <c:when test="${param.sortBy == 'first_name'}">
                                    <option selected value="first_name"><fmt:message key="SORT_BY_FIRST_NAME"/></option>
                                    <option value="last_name"><fmt:message key="SORT_BY_LAST_NAME"/></option>
                                    <option value="balance"><fmt:message key="SORT_BY_BALANCE"/></option>
                                </c:when>
                                <c:when test="${param.sortBy == 'last_name'}">
                                    <option selected value="last_name"><fmt:message key="SORT_BY_LAST_NAME"/></option>
                                    <option value="first_name"><fmt:message key="SORT_BY_FIRST_NAME"/></option>
                                    <option value="balance"><fmt:message key="SORT_BY_BALANCE"/></option>
                                </c:when>
                                <c:when test="${param.sortBy == 'balance'}">
                                    <option selected value="balance"><fmt:message key="SORT_BY_BALANCE"/></option>
                                    <option value="first_name"><fmt:message key="SORT_BY_FIRST_NAME"/></option>
                                    <option value="last_name"><fmt:message key="SORT_BY_LAST_NAME"/></option>
                                </c:when>
                                <c:otherwise>
                                    <option value="id" selected><fmt:message key="SORT_BY"/></option>
                                    <option value="first_name"><fmt:message key="SORT_BY_FIRST_NAME"/></option>
                                    <option value="last_name"><fmt:message key="SORT_BY_LAST_NAME"/></option>
                                    <option value="balance"><fmt:message key="SORT_BY_BALANCE"/></option>
                                </c:otherwise>
                            </c:choose>
                        </select>
                    </div>
                    <div class="tariff-selection">
                        <select name="order" class="form-select" aria-label="Default select example">
                            <c:choose>
                                <c:when test="${param.order == 'asc'}">
                                    <option selected value="asc"><fmt:message key="ASC"/></option>
                                    <option value="desc"><fmt:message key="DESC"/></option>
                                </c:when>
                                <c:when test="${param.order == 'desc'}">
                                    <option selected value="desc"><fmt:message key="DESC"/></option>
                                    <option value="asc"><fmt:message key="ASC"/></option>
                                </c:when>
                                <c:otherwise>
                                    <option value="asc" selected><fmt:message key="Order"/></option>
                                    <option value="desc"><fmt:message key="DESC"/></option>
                                    <option value="asc"><fmt:message key="ASC"/></option>
                                </c:otherwise>
                            </c:choose>
                        </select>
                    </div>

                    <div class="tariff-selection">
                        <input class="input-button" type="submit" value="<fmt:message key="SORT"/>">
                    </div>
                </form>
                <form action="saveCustomersInPDF" method="post">
                    <input type="hidden" name="sortBy" value="${param.sortBy}">
                    <input type="hidden" name="order" value="${param.order}">
                    <input class="input-button" type="submit" style="margin-left: 1vh"
                           value="<fmt:message key="SAVE_IN_PDF"/>">
                </form>
            </div>

        </div>


        <section class="table-container">
            <table class="table">
                <thead style="border-bottom: solid black;">
                <th>#</th>
                <th><fmt:message key="USER_LOGIN"/></th>
                <th><fmt:message key="FIRST_NAME"/></th>
                <th><fmt:message key="LAST_NAME"/></th>
                <th><fmt:message key="Balance"/></th>
                <th><fmt:message key="Email"/></th>
                <th><fmt:message key="PHONE_NUMBER"/></th>
                <th><fmt:message key="Country"/></th>
                <th><fmt:message key="City"/></th>
                <th><fmt:message key="Street"/></th>
                </thead>
                <tbody>
                <c:if test="${customers != null}">

                    <c:forEach var="customer" varStatus="i" items="${customers}">
                        <form action="customers" method="post">
                            <tr style="border-bottom: solid black;">
                                <th>
                                    <c:out value="${i.index+1}"/>
                                </th>
                                <td>
                                    <c:out value="${customer.login}"/>
                                </td>
                                <td>
                                    <c:out value="${customer.firstName}"/>
                                </td>
                                <td>
                                    <c:out value="${customer.lastName}"/>
                                </td>
                                <td style="text-align: right">
                                    <dt:numberTag format="${lang}" number="${customer.balance}"/>
                                    &nbsp;$
                                </td>
                                <td>
                                    <c:out value="${customer.email}"/>
                                </td>
                                <td>
                                    <c:out value="${customer.phoneNumber}"/>
                                </td>
                                <td>
                                    <c:out value="${customer.country}"/>
                                </td>
                                <td>
                                    <c:out value="${customer.city}"/>
                                </td>
                                <td>
                                    <c:out value="${customer.street}"/>
                                </td>
                                <td>
                                    <input type="hidden" name="id" value="${customer.id}">
                                    <c:if test="${customer.isBlocked == false}">
                                        <input class="input-button" name="action" style="margin-left: 20%" type="submit"
                                               value="<fmt:message key="Block"/>">
                                    </c:if>
                                    <c:if test="${customer.isBlocked == true}">
                                        <input class="input-button" name="action" style="margin-left: 20%" type="submit"
                                               value="<fmt:message key="Unblock"/>">
                                    </c:if>

                                </td>
                            </tr>

                        </form>
                    </c:forEach>
                </c:if>

                </tbody>
            </table>
        </section>


    </div>
    <nav aria-label="Page navigation example">
        <ul class="pagination justify-content-center">

            <c:choose>
                <c:when test="${param.page == 1}">
                    <li class="page-item disabled">
                        <a class="page-link gr" href="customers?page=1"><fmt:message key="PREVIOUS"/></a>
                    </li>
                </c:when>
                <c:otherwise>
                    <c:choose>
                        <c:when test="${param.sortBy != null && param.order != null}">
                            <li class="page-item">
                                <a class="page-link dark"
                                   href="customers?page=${param.page-1}&sortBy=${param.sortBy}&order=${param.order}"><fmt:message
                                        key="PREVIOUS"/></a>
                            </li>
                        </c:when>
                        <c:otherwise>
                            <li class="page-item">
                                <a class="page-link dark" href="customers?page=${param.page-1}"><fmt:message
                                        key="PREVIOUS"/></a>
                            </li>
                        </c:otherwise>
                    </c:choose>

                </c:otherwise>
            </c:choose>


            <c:forEach begin="1" var="i" end="${maxPages}">
                <c:choose>
                    <c:when test="${param.page == i}">
                        <c:choose>
                            <c:when test="${param.sortBy != null && param.order != null}">
                                <li class="page-item active"><a class="page-link item white"
                                                                href="customers?page=${i}&sortBy=${param.sortBy}&order=${param.order}">${i}</a>
                                </li>
                            </c:when>
                            <c:otherwise>
                                <li class="page-item active"><a class="page-link item white"
                                                                href="customers?page=${i}">${i}</a>
                                </li>
                            </c:otherwise>
                        </c:choose>
                    </c:when>
                    <c:otherwise>
                        <c:choose>
                            <c:when test="${param.sortBy != null && param.order != null}">
                                <li class="page-item"><a class="page-link item dark"
                                                         href="customers?page=${i}&sortBy=${param.sortBy}&order=${param.order}">${i}</a>
                                </li>
                            </c:when>
                            <c:otherwise>
                                <li class="page-item"><a class="page-link item dark" href="customers?page=${i}">${i}</a>
                                </li>
                            </c:otherwise>
                        </c:choose>
                    </c:otherwise>
                </c:choose>

            </c:forEach>

            <c:choose>
                <c:when test="${param.page == maxPages}">
                    <li class="page-item disabled">
                        <a class="page-link gr" href="customers?page=${param.page + 1}"><fmt:message key="NEXT"/></a>
                    </li>

                </c:when>
                <c:otherwise>

                    <c:choose>
                        <c:when test="${param.sortBy != null && param.order != null}">

                            <li class="page-item">
                                <a class="page-link dark"
                                   href="customers?page=${param.page + 1}&sortBy=${param.sortBy}&order=${param.order}"><fmt:message
                                        key="NEXT"/></a>
                            </li>

                        </c:when>
                        <c:otherwise>
                            <li class="page-item">
                                <a class="page-link dark" href="customers?page=${param.page + 1}"><fmt:message
                                        key="NEXT"/></a>
                            </li>
                        </c:otherwise>

                    </c:choose>

                </c:otherwise>
            </c:choose>


        </ul>
    </nav>
    </div>
</section>

<jsp:include page="../templates/footer.jsp"/>
<script type="text/javascript">
    <%@include file="../../../js/languageUtil.js"%>
</script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous"></script>
</body>
</html>
