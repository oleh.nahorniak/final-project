<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${lang}" scope="session"/>
<fmt:setBundle basename="text"/>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <style>
        <%@include file="/style/login.css" %>
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <title>Internet Service Provider</title>
</head>
<body>
<header>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="/application"><fmt:message key="ISP"/></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav" style="justify-content: right">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="/application"><fmt:message key="Home"/></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="about"><fmt:message key="About"/></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="contacts" style="margin-right: 15px"><fmt:message key="Contacts"/></a>
                    </li>
                    <li>
                        <select id="lang" onchange="setLanguage()">
                            <c:if test="${lang == 'en'}">
                                <option>en</option>
                                <option>uk</option>
                            </c:if>
                            <c:if test="${lang == 'uk'}">
                                <option>uk</option>
                                <option>en</option>
                            </c:if>
                        </select>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>

<jsp:include page="WEB-INF/jsp/message.jsp"></jsp:include>

<div class="container-fluid form-container">


    <div class="container py-5 h-100">
        <div class="row d-flex align-items-center justify-content-center h-100">
            <div class="col-md-8 col-lg-7 col-xl-6">
                <img src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-login-form/draw2.svg"
                     class="img-fluid" alt="Phone image">
            </div>
            <div class="col-md-7 col-lg-5 col-xl-5 offset-xl-1" style="">
                <form action="login" method="post" style="width: 70%">
                    <!-- Email input -->
                    <div class="form-outline mb-4">
                        <label class="form-label" for="form1Example13"><fmt:message key="USER_LOGIN"/></label>
                        <input id="form1Example13" class="form-control form-control-lg" type="text" name="login"
                               value=""
                               placeholder="<fmt:message key="USER_LOGIN"/>"/>

                    </div>

                    <!-- Password input -->
                    <div class="form-outline mb-4">
                        <label class="form-label" for="form1Example23"><fmt:message key="Password"/></label>
                        <input type="password" id="form1Example23" class="form-control form-control-lg" value=""
                               name="password"
                               placeholder="<fmt:message key="Password"/>"/>

                    </div>

                    <div class="d-flex justify-content-around align-items-center mb-4">
                        <a href="#staticBackdrop" data-bs-toggle="modal"
                           data-bs-target="#staticBackdrop"><fmt:message key="FORGOT_PASSWORD"/></a>
                    </div>
                    <!-- Submit button -->
                    <div style="align-items: center ; justify-content: center ; text-align: center">
                        <input type="submit" class="btn btn-primary btn-lg btn-block"
                               value="<fmt:message key="Login"/>"/>
                    </div>

                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="staticBackdrop" style="top:18vh; "
         data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">

                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                            aria-label="Close"></button>
                </div>

                <form action="forgotPassword" method="POST">
                    <div class="modal-body">
                        <div class="icon-container">
                            <img src="images/lock.png" width="100px" height="100px"/>
                        </div>
                        <div class="text-con">
                            <p>
                                <fmt:message key="TROUBLE_LOGGING_IN"/>
                            </p>
                            <p><fmt:message key="RECOVERY_TIP"/></p>
                        </div>
                        <div class="inp-con">
                            <input class="inp" id="email" name="email" maxlength="30" type="email"
                                   placeholder="<fmt:message key="Email"/>"
                                   required>
                            <br>
                            <c:if test="${error != null}">
                                <p class="inp-error"><fmt:message key="THERE_IS_NO_USER_WITH_THIS_EMAIL"/></p>
                            </c:if>
                        </div>
                        <div class="btn-con">
                            <input type="submit" class="btn btn-primary" style="width: 38vh"
                                   value="<fmt:message key="SEND"/>">
                        </div>

                    </div>

                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="staticBackdrop1" style="top:20vh"
         data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdrop1Label"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <form action="deleteAttributes" method="post" style="width:100%;">
                        <button style="float: right;" type="submit" class="btn-close" data-bs-dismiss="modal"
                                name="action"
                                value="code"
                                aria-label="Close"></button>
                    </form>
                </div>

                <form action="confirmCode" method="POST">
                    <div class="modal-body">
                        <div class="icon-container">
                            <img src="images/secret-code.png" width="100px" height="100px"/>
                        </div>
                        <div class="text-con">
                            <p><fmt:message key="PLEASE_TYPE_CODE"/></p>
                        </div>
                        <div class="inp-con">
                            <input class="inp" id="code" name="code" maxlength="6" type="text"
                                   placeholder="<fmt:message key="CODE"/>"
                                   required>
                            <br>
                            <c:if test="${codeError != null}">
                                <p class="inp-error">
                                    <fmt:message key="CODE_IS_NOT_CORRECT"/>
                                </p>
                            </c:if>
                        </div>
                        <div class="btn-con">
                            <input type="submit" class="btn btn-primary" style="width: 38vh"
                                   value="<fmt:message key="CONFIRM"/>">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="staticBackdrop2" style="top:20vh"
         data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdrop2Label"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <form action="deleteAttributes" method="post" style="width:100%;">
                        <button style="float: right;" type="submit" class="btn-close" data-bs-dismiss="modal"
                                name="action"
                                value="changePass"
                                aria-label="Close"></button>
                    </form>

                </div>

                <form action="changePasswordByEmail" method="POST">
                    <div class="modal-body">
                        <div class="icon-container">
                            <img src="images/unlocked.png" width="100px" height="100px"/>
                        </div>
                        <div class="text-con">
                            <p><fmt:message key="PLEASE_INPUT_CURRENT_PASSWORD"/></p>
                        </div>
                        <div class="inp-con">
                            <input class="inp" id="newPassword" name="newPassword" style="margin-bottom: 1vh"
                                   maxlength="20"
                                   type="password"
                                   placeholder="<fmt:message key="NEW_PASSWORD"/>"
                                   pattern="(^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$)|([a-fA-F0-9]{32})"
                                   oninvalid="this.setCustomValidity('Invalid password !\n Password length must be greater than 8 characters.' +
                                    '\nPassword must contain at least one Capital letter')"
                                   oninput="this.setCustomValidity('')"
                                   required>
                            <input class="inp" id="confirmPassword" name="confirmPassword" maxlength="20"
                                   type="password"
                                   placeholder="<fmt:message key="CONFIRM_PASSWORD"/>"
                                   pattern="(^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$)|([a-fA-F0-9]{32})"
                                   oninvalid="this.setCustomValidity('Invalid password !\n Password length must be greater than 8 characters.' +
                                    '\nPassword must contain at least one Capital letter')"
                                   oninput="this.setCustomValidity('')"
                                   required>
                            <br>
                            <div class="error-container">
                                <c:if test="${passwordError != null}">
                                    <p class="inp-error">
                                        <fmt:message key="${passwordError}"/>
                                    </p>
                                </c:if>
                            </div>
                        </div>
                        <div class="btn-con">
                            <input type="submit" class="btn btn-primary" style="width: 38vh ; margin-top: 2vh"
                                   value="<fmt:message key="CONFIRM"/>">
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>

</div>
</div>

<script src="//ajax.aspnetcdn.com/ajax/jQuery/jquery-2.1.1.js" type="text/javascript"></script>
<c:if test="${error != null}">

    <script type="text/javascript">
        $(window).load(function () {
            $('#staticBackdrop').modal('show');
        });
    </script>
    <% session.removeAttribute("error");%>
</c:if>

<c:if test="${code != null}">

    <script type="text/javascript">
        $(window).load(function () {
            $('#staticBackdrop1').modal('show');
        });
    </script>
    <% session.removeAttribute("codeError");%>
</c:if>

<c:if test="${changePassword == true}">

    <script type="text/javascript">
        $(window).load(function () {
            $('#staticBackdrop2').modal('show');
        });
    </script>
</c:if>

<script type="text/javascript">
    function deleteChangePassword() {
        console.log(<%= session.getAttribute("changePassword")
    %>)

    }
</script>
<jsp:include page="WEB-INF/jsp/templates/footer.jsp"/>
<script type="text/javascript">
    <%@include file="js/languageUtil.js"%>
</script>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous"></script>
</body>
</html>
