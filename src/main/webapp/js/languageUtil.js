function setLanguage() {
    let str = document.getElementById("lang").value;
    if (str != null) {
        let xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function () {
            location.reload();
        };
        xmlhttp.open("POST", "languageServlet?lang=" + str, true);
        xmlhttp.send();
    }

}